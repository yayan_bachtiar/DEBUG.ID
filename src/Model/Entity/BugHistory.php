<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * BugHistory Entity.
 *
 * @property int $id
 * @property string $bugs_name
 * @property string $skenario
 * @property string $ekspektasi
 * @property string $result
 * @property string $saran
 * @property string $link
 * @property int $modul_id
 * @property \App\Model\Entity\Modul $modul
 * @property \Cake\I18n\Time $created
 * @property int $created_by
 * @property int $status
 * @property int $priority
 * @property int $is_deleted
 * @property string $version
 * @property \Cake\I18n\Time $start_date
 * @property \Cake\I18n\Time $due_date
 * @property string $bug_id
 * @property \App\Model\Entity\Bug $bug
 */
class BugHistory extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        '*' => true,
        'id' => false,
    ];
}
