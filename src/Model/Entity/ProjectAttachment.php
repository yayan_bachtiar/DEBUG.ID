<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * ProjectAttachment Entity.
 */
class ProjectAttachment extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * @var array
     */
    protected $_accessible = [
        'project_id' => true,
        'path' => true,
        'ext' => true,
        'filename' => true,
        'upload_date' => true,
        'upload_by' => true,
//        'module' => true,
    ];
}
