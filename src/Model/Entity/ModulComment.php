<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * ModulComment Entity.
 */
class ModulComment extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * @var array
     */
    protected $_accessible = [
        'id' => true,
        'comment' => true,
        'module_id' => true,
        'created_by' => true,
        'modified_by' => true,
        'module' => true,
    ];
}
