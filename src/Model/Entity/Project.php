<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * Project Entity.
 */
class Project extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * @var array
     */
    protected $_accessible = [
        'project_name' => true,
//        'project_start' => true,
//        'project_end' => true,
        'project_owner' => true,
        'project_image' => true,
        'created_by' => true,
        'project_desc' => true,
        'updated_by' => true,
//        'module' => true,
    ];
}
