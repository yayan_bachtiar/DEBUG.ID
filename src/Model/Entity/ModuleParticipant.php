<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * ModuleParticipant Entity.
 */
class ModuleParticipant extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * @var array
     */
    protected $_accessible = [
        'id' => true,
        'modul_id' => true,
        'user_id' => true,
        'modul' => true,
        'user' => true,
    ];
}
