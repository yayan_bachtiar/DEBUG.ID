<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * ProjectParticipant Entity.
 */
class ProjectParticipant extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * @var array
     */
    protected $_accessible = [
        'project_id' => true,
        'user_id' => true,
        'project' => true,
        'user' => true,
    ];
}
