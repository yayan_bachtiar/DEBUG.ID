<?php
namespace App\Model\Table;

use App\Model\Entity\User;
use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * Users Model
 *
 * @property \Cake\ORM\Association\HasMany $BugsParticipant
 * @property \Cake\ORM\Association\HasMany $ModuleParticipant
 * @property \Cake\ORM\Association\HasMany $ProjectParticipant
 */
class UsersTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config) {
        parent::initialize($config);

        $this->table('users');
        $this->displayField(['first_name']);
        $this->primaryKey('id');

        $this->hasMany('BugsParticipant', ['foreignKey' => 'user_id']);
        $this->hasMany('ModuleParticipant', ['foreignKey' => 'user_id']);
        $this->hasMany('ProjectParticipant', ['foreignKey' => 'user_id']);
        $this->belongsTo('Role', ['foreignKey' => 'role']);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator) {
        $validator->add('id', 'valid', ['rule' => 'numeric'])->allowEmpty('id', 'create');

        $validator->add('email', 'valid', ['rule' => 'email'])->requirePresence('email', 'create')->notEmpty('email')->add('email', 'unique', ['rule' => 'validateUnique', 'provider' => 'table']);

        $validator->requirePresence('username', 'create')->notEmpty('username')->add('username', 'unique', ['rule' => 'validateUnique', 'provider' => 'table']);

        $validator->requirePresence('password', 'create')->notEmpty('password');

        $validator->add('register_date', 'valid', ['rule' => 'date'])->requirePresence('register_date', 'create')->notEmpty('register_date');

        // $validator->add('created_by', 'valid', ['rule' => 'numeric'])->requirePresence('created_by', 'create')->notEmpty('created_by');

        // $validator->add('role', 'valid', ['rule' => 'numeric'])->requirePresence('role', 'create')->notEmpty('role');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules) {
        $rules->add($rules->isUnique(['email']));
        $rules->add($rules->isUnique(['username']));
        return $rules;
    }
}
