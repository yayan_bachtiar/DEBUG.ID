<?php
namespace App\Model\Table;

use App\Model\Entity\BugsAttachment;
use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * BugsAttachment Model
 *
 * @property \Cake\ORM\Association\BelongsTo $Bugs
 */
class BugsAttachmentTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->table('bugs_attachment');
        $this->displayField('id');
        $this->primaryKey('id');
        $this->belongsTo('Bugs', [
            'foreignKey' => 'bugs_id',
            'joinType' => 'INNER'
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->add('id', 'valid', ['rule' => 'numeric'])
            ->allowEmpty('id', 'create');

        $validator
            ->requirePresence('path', 'create')
            ->notEmpty('path');

        $validator
            ->requirePresence('ext', 'create')
            ->notEmpty('ext');

        $validator
            ->requirePresence('filename', 'create')
            ->notEmpty('filename');

        $validator
            ->add('upload_date', 'valid', ['rule' => 'datetime'])
            ->requirePresence('upload_date', 'create')
            ->notEmpty('upload_date');

        $validator
            ->add('upload_by', 'valid', ['rule' => 'numeric'])
            ->requirePresence('upload_by', 'create')
            ->notEmpty('upload_by');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules)
    {
        $rules->add($rules->existsIn(['bugs_id'], 'Bugs'));
        return $rules;
    }
}
