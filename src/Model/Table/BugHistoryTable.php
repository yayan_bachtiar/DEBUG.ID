<?php
namespace App\Model\Table;

use App\Model\Entity\BugHistory;
use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * BugHistory Model
 *
 * @property \Cake\ORM\Association\BelongsTo $Moduls
 * @property \Cake\ORM\Association\BelongsTo $Bugs
 */
class BugHistoryTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->table('bug_history');
        $this->displayField('id');
        $this->primaryKey('id');

        $this->addBehavior('Timestamp');

        $this->belongsTo('Bugs', [
            'foreignKey' => 'bug_id',
            'joinType' => 'INNER'
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->add('id', 'valid', ['rule' => 'numeric'])
            ->allowEmpty('id', 'create');

        $validator
            ->requirePresence('bugs_name', 'create')
            ->notEmpty('bugs_name');

        $validator
            ->requirePresence('skenario', 'create')
            ->notEmpty('skenario');

        $validator
            ->requirePresence('ekspektasi', 'create')
            ->notEmpty('ekspektasi');

        $validator
            ->requirePresence('result', 'create')
            ->notEmpty('result');

        $validator
            ->requirePresence('saran', 'create')
            ->notEmpty('saran');

        $validator
            ->requirePresence('link', 'create')
            ->notEmpty('link');

        $validator
            ->add('created_by', 'valid', ['rule' => 'numeric'])
            ->requirePresence('created_by', 'create')
            ->notEmpty('created_by');

        $validator
            ->add('status', 'valid', ['rule' => 'numeric'])
            ->requirePresence('status', 'create')
            ->notEmpty('status');

        $validator
            ->add('priority', 'valid', ['rule' => 'numeric'])
            ->requirePresence('priority', 'create')
            ->notEmpty('priority');

        $validator
            ->add('is_deleted', 'valid', ['rule' => 'numeric'])
            ->requirePresence('is_deleted', 'create')
            ->notEmpty('is_deleted');

        $validator
            ->requirePresence('version', 'create')
            ->notEmpty('version');

        $validator
            ->add('start_date', 'valid', ['rule' => 'datetime'])
            ->requirePresence('start_date', 'create')
            ->notEmpty('start_date');

        $validator
            ->add('due_date', 'valid', ['rule' => 'datetime'])
            ->requirePresence('due_date', 'create')
            ->notEmpty('due_date');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules)
    {
        $rules->add($rules->existsIn(['modul_id'], 'Moduls'));
        $rules->add($rules->existsIn(['bug_id'], 'Bugs'));
        return $rules;
    }
}
