<?php
namespace App\Model\Table;

use App\Model\Entity\ModulComment;
use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * ModulComment Model
 */
class ModulCommentTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config) {
        $this->primaryKey('id');
        
        $this->table('modul_comment');
        $this->addBehavior('Timestamp');
        $this->belongsTo('Module', ['foreignKey' => 'module_id', 'joinType' => 'INNER']);
        $this->belongsTo('Users', ['foreignKey' => 'created_by', 'joinType' => 'INNER']);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator) {
        $validator->requirePresence('comment', 'create')->notEmpty('comment');
        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules) {
        // $rules->add($rules->existsIn(['module_id'], 'Module'));
        return $rules;
    }
}
