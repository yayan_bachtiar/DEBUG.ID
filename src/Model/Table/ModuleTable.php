<?php
namespace App\Model\Table;

use App\Model\Entity\Module;
use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * Module Model
 */
class ModuleTable extends Table
{
    
    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config) {
        $this->table('module');
        $this->displayField('id');
        $this->primaryKey('id');
        $this->addBehavior('Timestamp');
        $this->belongsTo('Project', ['foreignKey' => 'project_id', 'joinType' => 'INNER']);
        $this->hasMany('ModulComment', ['foreignKey' => 'module_id']);
        $this->hasMany('Task', ['foreignKey' => 'modul_id']);
        $this->hasMany('ModuleParticipant', ['foreignKey' => 'modul_id']);
        $this->hasMany('ModuleAttachment', ['foreignKey' => 'modul_id']);
        $this->hasMany('Bugs', ['foreignKey' => 'modul_id']);
        $this->belongsTo('Users', ['foreignKey' => 'module_excecutor']);
        $this->belongsTo('Status', ['foreignKey' => 'status']);
    }
    
    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator) {
        $validator->add('id', 'valid', ['rule' => 'numeric'])->allowEmpty('id', 'create');
        $validator->requirePresence('module_name', 'create')->notEmpty('module_name');
        $validator->requirePresence('module_desc', 'create')->notEmpty('module_desc');
        return $validator;
    }
    
}
