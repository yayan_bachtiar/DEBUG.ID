<?php
namespace App\Model\Table;

use App\Model\Entity\Menu;
use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * Menu Model
 *
 * @property \Cake\ORM\Association\BelongsToMany $Role
 */
class MenuTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->table('menu');
        $this->displayField('id');
        $this->primaryKey('id');
        $this->belongsToMany('Role', [
            'foreignKey' => 'menu_id',
            'targetForeignKey' => 'role_id',
            'joinTable' => 'role_menu'
        ]);
        $this->hasMany('MenuModul', [
            'foreignKey'=>'menu_id',
            'targetForeignKey'=>'id',
            ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->add('id', 'valid', ['rule' => 'numeric'])
            ->allowEmpty('id', 'create');

        $validator
            ->requirePresence('menu', 'create')
            ->notEmpty('menu');

        $validator
            ->requirePresence('link', 'create')
            ->notEmpty('link');

        $validator
            ->add('urutan', 'valid', ['rule' => 'numeric'])
            ->requirePresence('urutan', 'create')
            ->notEmpty('urutan');

        $validator
            ->add('parent', 'valid', ['rule' => 'numeric'])
            ->allowEmpty('parent');

        return $validator;
    }
}
