<?php
namespace App\Model\Table;

use App\Model\Entity\Bug;
use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * Bugs Model
 *
 * @property \Cake\ORM\Association\BelongsTo $Moduls
 */
class BugsTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config) {
        parent::initialize($config);

        $this->table('bugs');
        $this->displayField('id');
        $this->primaryKey('id');
        $this->addBehavior('Timestamp');
        $this->belongsTo('Module', ['foreignKey' => 'modul_id', 'joinType' => 'INNER']);
        $this->belongsTo('Users', ['foreignKey' => 'created_by', 'joinType' => 'LEFT']);
        $this->belongsTo('Status', ['foreignKey' => 'status', 'joinType' => 'LEFT']);
        $this->belongsTo('Priority', ['foreignKey' => 'priority', 'joinType' => 'LEFT']);
        $this->hasMany('BugsParticipant', ['foreignKey' => 'bug_id']);

        // $this->belongsTo('Users', ['foreignKey' => 'modified_by', 'joinType' =>=> 'LEFT']);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator) {
        $validator->add('id', 'valid', ['rule' => 'numeric'])->allowEmpty('id', 'create');

        $validator->requirePresence('bugs_name', 'create')->notEmpty('bugs_name');

        $validator->allowEmpty('bugs_detail');

        $validator->add('created_by', 'valid', ['rule' => 'numeric'])->requirePresence('created_by', 'create')->notEmpty('created_by');

        $validator->add('modified_by', 'valid', ['rule' => 'numeric'])->allowEmpty('modified_by');

        $validator->add('status', 'valid', ['rule' => 'numeric'])->requirePresence('status', 'create')->notEmpty('status');

        $validator->add('priority', 'valid', ['rule' => 'numeric'])->requirePresence('priority', 'create')->notEmpty('priority');

        $validator->requirePresence('link', 'create')->notEmpty('link');

        $validator->allowEmpty('method');

        $validator->requirePresence('skenario', 'create')->notEmpty('skenario');

        $validator->requirePresence('result', 'create')->notEmpty('result');

        $validator->requirePresence('expekasi', 'create')->notEmpty('expekasi');

        $validator->allowEmpty('saran');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules) {
        $rules->add($rules->existsIn(['modul_id'], 'Module'));
        return $rules;
    }
}
