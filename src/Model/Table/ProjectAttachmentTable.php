<?php
namespace App\Model\Table;

use App\Model\Entity\ProjectAttachment;
use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * ProjectAttachment Model
 */
class ProjectAttachmentTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        $this->table('project_attachment');
        $this->displayField('id');
        $this->primaryKey('id');
        $this->belongsTo('Project', [
            'foreignKey' => 'project_id',
            'joinType' => 'INNER'
        ]);
        $this->belongsTo('Users', [
            'foreignKey' => 'upload_by',
            'joinType' => 'LEFT'
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
//    public function validationDefault(Validator $validator)
//    {
//        $validator
//            ->add('id', 'valid', ['rule' => 'numeric'])
//            ->allowEmpty('id', 'create');
//
//        $validator
//            ->requirePresence('path', 'create')
//            ->notEmpty('path');
//
//        $validator
//            ->requirePresence('ext', 'create')
//            ->notEmpty('ext');
//
//        $validator
//            ->requirePresence('filename', 'create')
//            ->notEmpty('filename');
//
//        $validator
//            ->add('upload_date', 'valid', ['rule' => 'datetime'])
//            ->requirePresence('upload_date', 'create')
//            ->notEmpty('upload_date');
//
//        $validator
//            ->add('upload_by', 'valid', ['rule' => 'numeric'])
//            ->requirePresence('upload_by', 'create')
//            ->notEmpty('upload_by');
//
//        return $validator;
//    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules)
    {
        $rules->add($rules->existsIn(['project_id'], 'Project'));
        $rules->add($rules->existsIn(['upload_by'], 'Users'));
        return $rules;
    }
}
