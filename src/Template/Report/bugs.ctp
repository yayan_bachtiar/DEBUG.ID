<div class="" id="p_content">
    <div class="page-title">
        <div class="title_left">
            <h3>Report <small>XS Technology</small></h3>
        </div>
        <div class="title_right">
            <div class="col-md-5 col-sm-5 col-xs-12 form-group pull-right top_search">
                <div class="input-group">
                    <input type="text" class="form-control" id="jetssearch" placeholder="Search for...">
                    <span class="input-group-btn">
                    <button class="btn btn-default" type="button">Go!</button>
                    </span>
                </div>
            </div>
        </div>
    </div>
    <div class="clearfix"></div>
    <div class="row" >
        <div class="col-md-12">
            <div class="x_panel">
                <div class="x_title">
                    <h2>Report</h2>
                    <ul class="nav navbar-right panel_toolbox">
                        <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                        </li>
                        <li class="dropdown">
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i class="fa fa-wrench"></i></a>
                            <ul class="dropdown-menu" role="menu">
                                <li><a href="#">Settings 1</a>
                                </li>
                                <li><a href="#">Settings 2</a>
                                </li>
                            </ul>
                        </li>
                        <li><a class="close-link"><i class="fa fa-close"></i></a>
                        </li>
                    </ul>
                    <div class="clearfix"></div>
                </div>
                <div class="x_content">
                    <p>Project handle by PT. Xs Technologies</p>
                    <!-- start project list -->
                    <table  class="table responsive-utilities ajax">
                        <thead>
                            <tr>
                                <th style="width: 1%"><?= $this->Paginator->sort('id', '#') ?></th>
                                <th style="width: 20%"><?= $this->Paginator->sort('bugs_name', 'Bug Name') ?></th>
                                <th><?= $this->Paginator->sort('username', 'Executor', ['model'=>'user']) ?></th>
                                <th style="width: 20%">Project</th>
                                <th><?= $this->Paginator->sort('project_name', 'Project') ?></th>
                                <th><?= $this->Paginator->sort('status', 'Status') ?></th>
                            </tr>
                        </thead>
                        <tbody id="jetssearch_source">
                            <?php foreach ($bug as $list) {?>
                            <tr>
                                <td><?= $this->Html->link("#".$list->id, ['controller'=>'bugs','action'=>'view', $list->id])?></td>
                                <td>
                                    <?= $this->Html->link($list->bugs_name, ['controller'=>'bugs','action'=>'view', $list->id])?>
                                    <br />
                                    <small>Due Date <?= $list->due_date?></small>
                                </td>
                                <td>
                                    <ul class="list-inline">
                                        <?php foreach ($list->bugs_participant as $users) {?>
                                        <li>
                                            <img src="<?= $this->General->get_avatar($users->user->email)?>" class="avatar" alt="Avatar" title="<?= $users->user->username;?>">
                                        </li>
                                        <?php }?>
                                    </ul>
                                </td>
                                <td>
                                    <?=  $this->Html->link($list->module->module_name, ['controller'=>'module', 'action'=>'view', $list->module->id],['class'=>'', 'escape'=>false]);?>
                                </td>
                                <td>
                                <?=  $this->Html->link($list->module->project->project_name, ['controller'=>'project', 'action'=>'view', $list->module->project->id],['class'=>'', 'escape'=>false]);?>
                                </td>
                                <td>
                                <?php $btn="danger";
                                if($list->status->id ==1){
                                    $btn ="success";
                                }else if($list->status->id==2){
                                    $btn ="default";
                                }else if($list->status->id==4){
                                    $btn ="info";
                                }?>
                                    <button type="button" class="btn btn-<?= $btn;?> btn-xs"><?= $list->status->name?></button>
                                </td>
                                <!-- <td> -->
                                    <?php //$this->Html->link('<i class="fa fa-search"></i> See Detail ', ['controller'=>'bugs','action'=>'view', $list->id], ['class'=>'btn btn-primary btn-xs', 'escape'=>false])?>
                                <!-- </td> -->
                            </tr>
                            <?php } ?>
                        </tbody>
                    </table>
                    <!-- end project list -->
                    <!-- paginator -->
                    <div class="pull-right">
                        <ul class="pagination pagination" id="passed">
                            <?= $this->Paginator->prev('«', $options = ['model'=>'bpass']) ?>
                            <?= $this->Paginator->numbers() ?>
                            <?= $this->Paginator->next('»', $options = ['model'=>'bpass']) ?>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <?= $this->element('collapse-box');?>
    <script type="text/javascript">
        $("ul#passed.pagination li a").bind("click", function (event) {
            var href = $(this).attr('href');
            $.ajax({
            success:function (response) {
                $("#p_content").html(response);
            },
            url: href,
            error:(function(response){
                location.reload();
                }),
            });
            return false;
        });
        $("div#p_content a").bind("click", function (event) {
            var href = $(this).attr('href');
            $.ajax({
            success:function (response) {
                $("#p_content").html(response);
            },
            url: href,
            error:(function(response){
                location.reload();
                }),
            });
            return false;
        });
    </script>
    <?= $this->element('progress')?>
</div>