<div class="actions columns large-2 medium-3">
    <h3><?= __('Actions') ?></h3>
    <ul class="side-nav">
        <li><?= $this->Html->link(__('List Role'), ['action' => 'index']) ?></li>
    </ul>
</div>
<div class="role form large-10 medium-9 columns">
    <?= $this->Form->create($role); ?>
    <fieldset>
        <legend><?= __('Add Role') ?></legend>
        <?php
            echo $this->Form->input('role_name');
            echo $this->Form->input('created_by');
            echo $this->Form->input('role_desc');
        ?>
    </fieldset>
    <?= $this->Form->button(__('Submit')) ?>
    <?= $this->Form->end() ?>
</div>
