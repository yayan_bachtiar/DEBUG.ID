<div class="x_content">
    <table  class="table responsive-utilities ">
        <thead>
            <tr>
                <th style="width: 1%">#</th>
                <th style="width: 20%">Bug Name</th>
                <th>Executor</th>
                <th>Module</th>
                <th style="width: 20%">#Action</th>
            </tr>
        </thead>
        <tbody id="jetssearch_source">
            <?php if($ball->count()==null){?>
            <a href="#" class="ticket_open_comment">No Bugs Yet</a>
            <?php }?>
            <?php foreach ($ball as $bug) {?>
            <tr>
                <td>#<?= $bug->id;?></td>
                <td>
                    <a><?= $bug->bugs_name?></a>
                    <br/><small>DUE DATE <?= $bug->due_date?></small>
                </td>
                <td>
                    <ul class="list-inline">
                        <?php foreach ($bug->bugs_participant as $users) {?>
                        <li>
                            <img src="<?= $this->General->get_avatar($users->user->email)?>" class="avatar" alt="Avatar" title="<?= $users->user->username;?>">
                        </li>
                        <?php }?>
                    </ul>
                </td>
                <td>
                    <?=  $this->Html->link($bug->module->module_name, ['controller'=>'module', 'action'=>'view', $bug->module->id],['class'=>'', 'escape'=>false]);?>
                </td>
                <td>
                    <?=$this->Html->link('<i class="fa fa-folder"></i> View ', ['action'=>'view', $bug->id], ['class'=>'btn btn-primary btn-xs ', 'escape'=>false]);

                        if($bug->status==1){
                        echo $this->Html->link("<i class='fa fa-key'></i> It's Ready" , ['action'=>'is_ready', $bug->id], ['name'=>$bug->bugs_name, 'class'=>'btn btn-success btn-xs confirm', 'escape'=>false]);
                        }else{
                        echo $this->Html->link('<i class="fa fa-lock"></i> Close ', ['action'=>'close', $bug->id], ['name'=>$bug->bugs_name, 'class'=>'btn btn-warning btn-xs confirm', 'escape'=>false]);
                        }
                    if($this->request->session()->read('Auth.User.role')=='QA'){
                        if($bug->status==1){
                            echo $this->Html->link('<i class="fa fa-bug"></i> open bug ', ['action'=>'open', $bug->id], ['name'=>$bug->bugs_name, 'escape'=>false, 'class'=>'btn btn-warning btn-xs confirm']);
                        }
                    }
                    ?>
                </td>
            </tr>
            <?php } ?>
        </tbody>
    </table>
    <!-- end project list -->
    <!-- paginator -->
    <div class="pull-right">
        <ul class="pagination pagination" id="all">
            <?= $this->Paginator->prev('«', $options = ['model'=>'all']) ?>
            <?= $this->Paginator->numbers() ?>
            <?= $this->Paginator->next('»', $options = ['model'=>'all']) ?>
        </ul>
    </div>
</div>

<?= $this->element('collapse-box');?>
<?= $this->element('progress');?>
<script type="text/javascript">
$("a.confirm").bind("click", function (event) {
    var href = $(this).attr('href');
    var name = $(this).attr('name');
    $.confirm({
        title: 'Confirm!',
        content: 'Are you sure to this action for '+name,
        confirm: function(){
            $.ajax({
                url: href,
                type: 'GET',
            })
            .done(function(response) {
                toastr.success("Bugs has been open");
            })
            .fail(function() {
                setTimeout(toastr.error("boo, error!!"), 3000);
                location.reload(); 
                // console.log("error");
            })
            .always(function() {
            });
            
        },
        cancel: function(){
            alert('Canceled!')
        }
    });
    return false;
});
$("ul#all.pagination li a").bind("click", function (event) {
    var href = $(this).attr('href');
    $.ajax({
    success:function (response) {
        $("#b_content").html(response);
    },
    url: href,
    error:(function(response){
        location.reload();
        }),
    });
    return false;
});
</script>
<?= $this->element('progress')?>
</div>