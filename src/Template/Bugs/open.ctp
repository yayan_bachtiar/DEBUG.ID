<div class="container clar_both padding_fix" id="content_panel">
    <div class="row">
        <div class="col-md-6" id="project">
        </div>
        <div class="col-md-6" id="add_task">

        </div>
    </div>
</div>
<script type="text/javascript">
    $.ajax({
        url: "<?= $this->Url->build(['controller'=>'project', 'action'=>'get_project_list']); ?>",
        type: 'GET',
        dataType: 'html',
    })
    .done(function(response) {
        $('#project').html(response)
    })
    .fail(function() {
        console.log("error");
    })
    .always(function() {
        console.log("complete");
    });

</script>
