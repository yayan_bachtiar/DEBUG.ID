<div class="clar_both padding_fix">
    <div class="row">
        <div class="col-sm-12">
            <div class="x_panel">
                <div class="x_title">
                    <h2><i class="fa fa-bars"></i> My Bug <small></small></h2>
                    <ul class="nav navbar-right panel_toolbox">
                        <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                        </li>
                        <li class="dropdown">
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i class="fa fa-wrench"></i></a>
                            <ul class="dropdown-menu" role="menu">
                                <li><a href="#">Settings 1</a>
                                </li>
                                <li><a href="#">Settings 2</a>
                                </li>
                            </ul>
                        </li>
                        <li><a class="close-link"><i class="fa fa-close"></i></a>
                        </li>
                    </ul>
                    <div class="clearfix"></div>
                </div>
                <div class="x_content">
                    <div class="" role="tabpanel" data-example-id="togglable-tabs">
                        <ul id="myTab" class="nav nav-tabs bar_tabs" role="tablist">
                            <li role="presentation" class="active"><a href="#tab_content1" id="home-tab" role="tab" data-toggle="tab" aria-expanded="true" onclick="get_open();"><span class="badge open"></span>Open Bugs</a>
                            </li>
                            <li role="presentation" class=""><a href="#tab_content4" role="tab" id="profile-tab" data-toggle="tab"  aria-expanded="false" onclick="get_ready();"><span class="badge ready"></span>Ready Bugs</a>
                            </li>
                            <li role="presentation" class=""><a href="#tab_content2" role="tab" id="profile-tab" data-toggle="tab"  aria-expanded="false" onclick="get_close()"><span class="badge close"></span>Closed Bugs</a>
                            </li>
                            <li role="presentation" class=""><a href="#tab_content3" role="tab" id="profile-tab2" data-toggle="tab" aria-expanded="false" onclick="get_all();"><span class="badge all"></span>All Bugs</a>
                            </li>
                        </ul>
                        <div id="myTabContent" class="tab-content">
                            <div role="tabpanel" class="tab-pane fade active in" id="tab_content1" aria-labelledby="home-tab">
                                <div id="open_content">
                                </div>
                            </div>
                            <div role="tabpanel" class="tab-pane fade" id="tab_content4" aria-labelledby="profile-tab">
                                <div id="ready_content">
                                </div>
                            </div>
                            <div role="tabpanel" class="tab-pane fade" id="tab_content2" aria-labelledby="profile-tab">
                                <div id="close_content">
                                </div>
                            </div>
                            <div role="tabpanel" class="tab-pane fade" id="tab_content3" aria-labelledby="profile-tab">
                                <div id="all_content">
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript">
  jQuery(document).ready(function($) {
    get_open();
    // get_close();
    // get_ready();
    // get_all();
  });
</script>
<script type="text/javascript">


  function get_open(){
    $.ajax({
      url: '<?= $this->Url->build(["controller"=>"bugs", "action"=>"get_unresolved_bugs"]);?>',
      type: 'GET',
    })
    .done(function(response) {
      $("#open_content").html(response)
    })
    .fail(function() {
        console.log('nganu');
    });
  }
  function get_close(){
    $.ajax({
      url: '<?= $this->Url->build(["controller"=>"bugs", "action"=>"get_bugs_passed"]);?>',
      type: 'GET',
    })
    .done(function(response) {
      $("#close_content").html(response)
    })
    .fail(function() {
        console.log('nganu');
    });
  }
  function get_all(){
    $.ajax({
      url: '<?= $this->Url->build(["controller"=>"bugs", "action"=>"get_all"]);?>',
      type: 'GET',
    })
    .done(function(response) {
      $("#all_content").html(response)
    })
    .fail(function() {
        console.log('nganu');
    });
  }
  function get_ready(){
   $.ajax({
      url: '<?= $this->Url->build(["controller"=>"bugs", "action"=>"get_ready"]);?>',
      type: 'GET',
    })
    .done(function(response) {
      $("#ready_content").html(response)
    })
    .fail(function() {
        console.log('nganu');
    }); 
  }
</script>