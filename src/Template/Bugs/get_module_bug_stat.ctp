<div class="container">
    <div class="panel">
        <div class="block-web">
            <div class="header">
                <div class="actions">
                    <a href="#" class="minimize"><i class="fa fa-chevron-down"></i></a>
                    <a href="#" class="refresh" onclick="get_module_bug_stat()"><i class="fa fa-repeat"></i></a>
                    <a href="#" class="close-down" ><i class="fa fa-times"></i></a>
                </div>
                <h3>Bug Statistic</h3>
            </div>
            <div class="porlets-content">
                <div class="ticket_open_grid text-center "><b class=""> <?=$percent;?>% Done</b><span class="ticket_open_grid_progress" style="width:<?= $percent;?>%; margin-top:-19px; background-color:#EA8D14"></span>
                </div>
                <div class="row">
                    <div class="col-md-6">
                        <p>Total bug : <?=$this->Html->link($bug_sum,  ['controller'=>'bugs', 'action'=>'get_module_bug?module_id='. $modul_id."&type=all"])?></p>
                        <p>Total done : <?=$this->Html->link($bug_sum_pass,  ['controller'=>'bugs', 'action'=>'get_module_bug?module_id='. $modul_id."&type=pass"])?></p>
                        <p>Total pending : <?=$this->Html->link($bug_sum_pending,  ['controller'=>'bugs', 'action'=>'get_module_bug?module_id='. $modul_id."&type=pending"])?></p>
                        <p>Total Failed : <?=$this->Html->link($bug_sum_failed,  ['controller'=>'bugs', 'action'=>'get_module_bug?module_id='. $modul_id."&type=failed"])?></p>
                        <p>Total Ready To Test : <?=$this->Html->link($bug_sum_ready,  ['controller'=>'bugs', 'action'=>'get_module_bug?module_id='. $modul_id."&type=ready"])?></p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>