<div class="x_content">
    <table  class="table responsive-utilities ">
        <thead>
            <tr>
                <th style="width: 1%">#</th>
                <th style="width: 20%">Bug Name</th>
                <th>Skenario</th>
                <th style="width: 20%">#Action</th>
            </tr>
        </thead>
        <tbody id="jetssearch_source">
            <?php if($ready->count()==null){?>
            <a href="#" class="ticket_open_comment">No Bugs Yet</a>
            <?php }?>
            <?php foreach ($ready as $bug) {?>
            <tr>
                <td>#<?= $bug->bug->id;?></td>
                <td>
                    <a><?= $bug->bug->bugs_name?></a>
                    <br/><small><?= $bug->bug->created?></small>
                </td>
                <td>
                    <p><?php echo $bug->bug->skenario ?></p>
                </td>
                <td>
                <?=$this->Html->link('<i class="fa fa-folder"></i> View ', ['action'=>'view', $bug->bug->id], ['class'=>'btn btn-primary btn-xs', 'escape'=>false]);
                    
                    if($this->request->session()->read('Auth.User.role')=='WD'){
                        if($bug->bug->status==1){
                        echo $this->Html->link("<i class='fa fa-key'></i> It's Ready" , ['action'=>'is_ready', $bug->bug->id], ['name'=> $bug->bug->bugs_name,'class'=>'btn btn-success btn-xs confirm', 'escape'=>false]);
                        }
                    }
                    if($this->request->session()->read('Auth.User.role')=='QA'){
                        if($bug->bug->status==1){
                            echo $this->Html->link('<i class="fa fa-bug"></i> open bug ', ['action'=>'open', $bug->bug->id], ['name'=> $bug->bug->bugs_name, 'escape'=>false, 'class'=>'btn btn-warning btn-xs confirm']);
                        }
                        else if($bug->bug->status==4){
                        echo $this->Html->link('<i class="fa fa-lock"></i> Close ', ['action'=>'close', $bug->bug->id], ['name'=>$bug->bug->bugs_name, 'class'=>'btn btn-warning btn-xs confirm', 'escape'=>false]);
                        }
                    }
                    ?>
                </td>
            </tr>
            <?php } ?>
        </tbody>
    </table>
    <!-- end project list -->
    <!-- paginator -->
    <div class="pull-right">
        <ul class="pagination pagination" id="unresolved">
            <?= $this->Paginator->prev('«', $options = ['model'=>'bugs']) ?>
            <?= $this->Paginator->numbers() ?>
            <?= $this->Paginator->next('»', $options = ['model'=>'bugs']) ?>
        </ul>
    </div>
</div>

<?= $this->element('progress');?>
<script type="text/javascript">
$("a.confirm").bind("click", function (event) {
    var href = $(this).attr('href');
    $.confirm({
        title: 'Confirm!',
        content: 'Simple confirm!',
        confirm: function(){
            $.ajax({
                url: href,
                type: 'GET',
            })
            .done(function(response) {
                toastr.success("Bugs has been open");
            })
            .fail(function() {
                setTimeout(toastr.error("boo, error!!"), 3000);
                location.reload(); 
                // console.log("error");
            })
            .always(function() {
            });
            
        },
        cancel: function(){
            alert('Canceled!')
        }
    });
    return false;
});
$("ul#unresolved.pagination li a").bind("click", function (event) {
    var href = $(this).attr('href');
    $.ajax({
    success:function (response) {
        $("#open_content").html(response);
    },
    url: href,
    error:(function(response){
        location.reload();
        }),
    });
    return false;
});

</script>
<?= $this->element('progress')?>
</div>