<div class="todo_body animated fadeInRight" >
    <h5 class="orange_bg"> <i class="fa fa-warning"></i>  Bugs List (
    <small><?= $number?></small>
    )
    </h5>
    <?php
        if($number==0){
            ?>
        <li class="list-group-item">
            Bugs No Longer Available
        </li>
        <?php }
        $i=0;
        foreach ($bugs as $bugs) {?>
    <ul class="group_sortable1">
        <li>
            <span class=""><input name="" value="" type="checkbox"></span>
            <p><strong>Tikets  #<?=$bugs->id?></strong>
            - <?= $bugs->bugs_name;?> . [
            <?= $this->Html->link('More Details', ['controller'=>'bugs', 'action'=>'view', $bugs->id], ['class'=>'font-xs']);?>]
            <b><?= substr($bugs->bugs_detail, 0, 100)?></b>
            Reported @ <b class="blue-text" data-livestamp="<?= strtotime($bugs->created)?>"></b>
            </p>
        </li>
    </ul>
    <?php $i++;}?>
</div>
<div class="pull-right">
    <ul class="pagination pagination" id="passed">
        <?= $this->Paginator->prev('«', $options = ['model'=>'bpass']) ?>
        <?= $this->Paginator->numbers() ?>
        <?= $this->Paginator->next('»', $options = ['model'=>'bpass']) ?>
    </ul>
</div>
<script type="text/javascript">
    $("ul#passed.pagination li a").bind("click", function (event) {
        var href = $(this).attr('href');
        $.ajax({
        success:function (response) {
            $("#contentpanel").html(response);
        },
        url: href,
        error:(function(response){
            location.reload();
            }),
        });
        return false;
    });
</script>