<?= $this->Html->css("summernote/summernote.css");?>
<div class="">
    <div class="page-title">
        <div class="title_left">
            <h3>View Bug :: BUGID#<?php echo $bug->id; ?> </h3>
        </div>
        <div class="title_right">
            <div class="col-md-5 col-sm-5 col-xs-12 form-group pull-right top_search">
                <div class="input-group">
                    <input type="text" class="form-control" placeholder="Search for...">
                    <span class="input-group-btn">
                    <button class="btn btn-default" type="button">Go!</button>
                    </span>
                </div>
            </div>
        </div>
    </div>
    <div class="clearfix"></div>
    <div class="row">
        <div class="col-md-12 col-sm-12 col-xs-12">
            <div class="x_panel">
                <div class="x_title">
                    <h2>#<?php echo $bug->id; ?> <?= $bug->bugs_name;?></h2>
                    <ul class="nav navbar-right panel_toolbox">
                        <li><a href="#" class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                        </li>
                        <li class="dropdown">
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i class="fa fa-wrench"></i></a>
                            <ul class="dropdown-menu" role="menu">
                                <li><a href="#">Settings 1</a>
                                </li>
                                <li><a href="#">Settings 2</a>
                                </li>
                            </ul>
                        </li>
                        <li><a href="#"><i class="fa fa-close"></i></a>
                        </li>
                    </ul>
                    <div class="clearfix"></div>
                </div>
                <div class="x_content">
                    <div class="col-md-4 col-sm-4 col-xs-12">
                    </div>
                    <div class="col-md-8 col-sm-8 col-xs-12" style="border:0px solid #e5e5e5;">
                        <h3 class="prod_title"><?= $bug->bugs_name;?></h3>
                        <p class="user_owner"><span style="font-style:italic" data-livestamp="<?= strtotime($bug->created)?>">&nbs3 days &nbsp;&nbsp;&nbsp;</span> by <?=  $this->Html->link($bug->user->username, ['controller'=>'user', 'action'=>'view', $bug->user->id],['class'=>'', 'escape'=>false]);?> on <?=  $this->Html->link($bug->module->module_name, ['controller'=>'module', 'action'=>'view', $bug->module->id],['class'=>'', 'escape'=>false]);?> </p>
                        <p><?= substr($bug->skenario, 0, 200);?> ... </p>
                        <br />
                        <div class="row">
                            <div class="col-sm-6">
                                <h2>Result Status</h2>
                            </div>
                            <div class="col-sm-6">
                                <span class="btn <?php echo $color; ?> pull-right"><i class="<?php echo $fa; ?>"></i> <?php echo $bug->status->name; ?></span>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-6">
                                <h2>Result Priority</h2>
                            </div>
                            <div class="col-sm-6">
                                <span class="btn <?php echo $color2; ?> pull-right"><i class="<?php echo $fa2; ?>"></i> <?php echo $bug->priority->name; ?>
                                </span>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-6">
                                <h2>Reported @</h2>
                            </div>
                            <div class="col-sm-6">
                                <span class="btn btn-primary pull-right" ><i class="fa fa-clock-o"></i>&nbsp;<span data-livestamp="<?= strtotime($bug->created)?>">&nbsp; 3 days &nbsp;&nbsp;&nbsp;</span></span>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-6">
                                <h2>Due Date in </h2>
                            </div>
                            <div class="col-sm-6">
                                    <span class="btn btn-warning pull-right" ><i class="fa fa-clock-o"></i>&nbsp;<span id="timmer">&nbsp; 3 days &nbsp;&nbsp;&nbsp;</span></span>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-6">
                                <h2>PIC Developer</h2>
                            </div>
                            <div class="col-sm-6">
                                    <?php foreach ($bug->bugs_participant as $user ): ?>
                                    <span class="pull-right" >
                                        <img src="<?php echo $this->General->get_avatar($user->user->email) ?>" class="avatar" alt="Avatar" title="yayan">
                                    </span>
                                    <?php endforeach ?>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-12 text-center">
                            <?php
                                if($this->request->session()->read('Auth.User.role')=='WD'){
                                    if($bug->status->id==3){
                                        echo $this->Html->link("<i class='fa fa-key'></i> It's Ready" , ['action'=>'is_ready', $bug->id], ['name'=>$bug->bugs_name, 'class'=>'btn btn-success confirm', 'escape'=>false]);
                                    }
                                }
                                if($this->request->session()->read('Auth.User.role')=='QA'){
                                    if($bug->status->id==4||$bug->status->id==1){
                                        echo $this->Html->link('<i class="fa fa-bug"></i> Open Bug ', ['action'=>'open', $bug->id], ['name'=>$bug->bugs_name, 'escape'=>false, 'class'=>'btn btn-success confirm']);
                                        echo $this->Html->link('<i class="fa fa-lock"></i> Close Bug ', ['action'=>'open', $bug->id], ['name'=>$bug->bugs_name, 'escape'=>false, 'class'=>'btn btn-warning confirm']);
                                    }
                                }
                            ?>
                        </div>
                    </div>
                    <div class="col-md-12">
                        <div class="" role="tabpanel" data-example-id="togglable-tabs">
                            <ul id="myTab" class="nav nav-tabs bar_tabs" role="tablist">
                                <li role="presentation" class="active"><a href="#tab_content1" id="home-tab" role="tab" data-toggle="tab" aria-expanded="true">Scenario</a>
                                </li>
                                <li role="presentation" class=""><a href="#tab_content2" role="tab" id="profile-tab" data-toggle="tab" aria-expanded="false">expectasi</a>
                                </li>
                                <li role="presentation" class=""><a href="#tab_content3" role="tab" id="profile-tab2" data-toggle="tab"  aria-expanded="false">Result</a>
                                </li>
                                <li role="presentation" class=""><a href="#tab_content4" role="tab" id="profile-tab2" data-toggle="tab"  aria-expanded="false"><span class="pull-left badge" id="badge_cmt">0</span>&nbsp;Comments</a>
                                </li>
                                <li role="presentation" class=""><a href="#tab_content5" role="tab" id="profile-tab2" data-toggle="tab"  aria-expanded="false">Recent Activity</a>
                                </li>
                            </ul>
                            <div id="myTabContent" class="tab-content">
                                <div role="tabpanel" class="tab-pane fade active in" id="tab_content1" aria-labelledby="home-tab">
                                    <?= $bug->skenario?>
                                </div>
                                <div role="tabpanel" class="tab-pane fade" id="tab_content2" aria-labelledby="profile-tab">
                                    <?php echo $bug->ekspektasi ?>
                                </div>
                                <div role="tabpanel" class="tab-pane fade" id="tab_content3" aria-labelledby="profile-tab">
                                    <?php echo $bug->result;?>
                                </div>
                                <div role="tabpanel" class="tab-pane fade" id="tab_content4" aria-labelledby="comments"></div>
                                <div role="tabpanel" class="tab-pane fade" id="tab_content5" aria-labelledby="Activity">
                                    <div class="row">
                                        <div class="container col-md-12" id="log">
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<?=$this->element('collapse-box')?>
<?= $this->Html->script('countdown/jquery.countdown.min.js');?>
<?= $this->Html->script("summernote/summernote.min.js")?>
<script type="text/javascript">
$("a.confirm").bind("click", function (event) {
    var href = $(this).attr('href');
    $.confirm({
        title: 'Confirm!',
        content: 'Simple confirm!',
        confirm: function(){
            $.ajax({
                url: href,
                type: 'POST',
            })
            .done(function(response) {
                toastr.success(response);
            })
            .fail(function() {
                setTimeout(toastr.error("boo, error!!"), 3000);
                location.reload();
                // console.log("error");
            })
            .always(function() {
            });
            
        },
        cancel: function(){
        }
    });
    return false;
});
$('#timmer').countdown('<?php echo date("Y-m-d", strtotime($bug->due_date));?>', function(event) {
$(this).html(event.strftime('%d days %H:%M:%S'));
});
</script>
<script type="text/javascript">
    jQuery(document).ready(function($) {
        get_file();
        get_log();
        get_comment();
        get_comment_count();
    });
/**
    * [get_file description]
    * @return {[type]} [description]
    */
function get_file(){
        $.ajax({
url: '<?= $this->Url->build(["controller"=>"bugsAttachment", "action"=>"get_bug_files"]);?>',
type: 'GET',
dataType: 'html',
data: {id: <?= $bug->id;?>},
})
.done(function(response) {
$("#file_attach").html(response)
// console.log("success");
})
.fail(function() {
alert("error");
});
}
/**
* [get_comment description]
* @return {[type]} [description]
*/
function get_comment(){
$.ajax({
url: '<?= $this->Url->build(["controller"=>"bugsComment", "action"=>"get_bug_comment"]);?>',
type: 'GET',
dataType: 'html',
data: {id: <?= $bug->id;?>},
})
.done(function(response) {
$("#tab_content4").html(response)
// console.log("success");
})
.fail(function() {
alert("error");
});
}
function get_comment_count(){
$.ajax({
url: '<?= $this->Url->build(["controller"=>"bugsComment", "action"=>"cmt_count"]);?>',
type: 'GET',
dataType: 'html',
data: {id: <?= $bug->id;?>},
})
.done(function(response) {
$("#badge_cmt").html(response)
// console.log("success");
})
.fail(function() {
alert("error");
});
}
/**
* get log
* @return {[type]} [description]
*/
function get_log(){
$.ajax({
url: "<?= $this->Url->build(['controller'=>'ProjectLog', 'action'=>'get_log']);?>",
type: 'GET',
data: {bug_id: "<?= $bug->id;?>"},
})
.done(function(response) {
$("#log").html(response);
console.log(response);
})
.fail(function() {
console.log("error");
})
.always(function() {
console.log("complete");
});
}
</script>