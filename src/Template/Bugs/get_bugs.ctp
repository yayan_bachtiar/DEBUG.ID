<div class="x_content">
    <!-- start project list -->
    <table  class="table responsive-utilities ">
        <thead>
            <tr>
                <th style="width: 1%">#</th>
                <th style="width: 50%">Bugs</th>
                <th>Status</th>
                <th style="width: 30%">#Action</th>
            </tr>
        </thead>
        <tbody id="jetssearch_source">
        <?= $this->Paginator->counter(
    'Page {{page}} of {{pages}}, showing {{current}} records out of
     {{count}} total, starting on record {{start}}, ending on {{end}}'
);?>
            <?php foreach ($bugs as $list) {?>
            <tr>
                <td>#<?= $this->Html->link($list->id, ['controller'=>'bugs', 'action'=>'view', $list->modul_id])?></td>
                <td>
                    <?= $this->Html->link($list->bugs_name, ['controller'=>'bugs', 'action'=>'view', $list->modul_id])?>
                    <br />
                    <small><?= $list->created?></small>
                </td>
                <td>
                    <?php
                    $color = 'btn-danger';
                    if($list->status->name=='PASS'){
                        $color = 'btn-success';
                    }
                    if($list->status->name=='PENDING'){
                        $color = 'btn-info';   
                    }
                    ?>
                    <button type="button" class="btn <?= $color?> btn-xs"><?= $list->status->name?></button>
                </td>
                <td>
                    <?=$this->Html->link('<i class="fa fa-folder"></i> View ', ['action'=>'view', $list->id], ['class'=>'btn btn-primary btn-xs', 'escape'=>false])?>
                    <a href="#" class="btn btn-danger btn-xs"><i class="fa fa-trash-o"></i> Close </a>
                    <a href="#" class="btn btn-danger btn-xs"><i class="fa fa-trash-o"></i> Is Resolved </a>
                </td>
            </tr>
            <?php } ?>
        </tbody>
    </table>
    <!-- end project list -->
    <!-- paginator -->
    <div class="pull-right">
        <ul class="pagination pagination" id="closed">
            <?= $this->Paginator->prev('«', $options = ['model'=>'bpass']) ?>
            <?= $this->Paginator->numbers() ?>
            <?= $this->Paginator->next('»', $options = ['model'=>'bpass']) ?>
        </ul>
    </div>
</div>
<?= $this->element('collapse-box');?>
<script type="text/javascript">
    $("ul#closed.pagination li a").bind("click", function (event) {
        var href = $(this).attr('href');
        $.ajax({
        success:function (response) {
            $("#b_content").html(response);
        },
        url: href,
        error:(function(response){
            location.reload();
            }),
        });
        return false;
    });
</script>
<?= $this->element('progress')?>