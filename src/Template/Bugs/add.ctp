<?= $this->Html->css("summernote/summernote.css");?>
<?php echo $this->Html->css('icheck/flat/green.css'); ?>
<div class="page-title">
    <div class="title_left">
        <h3>Add New Bug</h3>
    </div>

    <div class="title_right">
        <div class="col-md-5 col-sm-5 col-xs-12 form-group pull-right top_search">
            <div class="input-group">
                <input type="text" class="form-control" placeholder="Search for...">
                <span class="input-group-btn">
        <button class="btn btn-default" type="button">Go!</button>
    </span>
            </div>
        </div>
    </div>
</div>
<div class="clearfix"></div>
<div class="row">
    <div class="col-md-12">
        <div class="x_panel">
            <div class="x_title">
                <h2>Add New  <small>Scenario Testing</small></h2>
                <ul class="nav navbar-right panel_toolbox">
                    <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                    </li>
                    <li class="dropdown">
                        <a href="#" class="collapse-link" data-toggle="dropdown" role="button" aria-expanded="false"><i class="fa fa-wrench"></i></a>
                        <ul class="dropdown-menu" role="menu">
                            <li><a href="#">Settings 1</a>
                            </li>
                            <li><a href="#">Settings 2</a>
                            </li>
                        </ul>
                    </li>
                    <li><a class="close-link"><i class="fa fa-close"></i></a>
                    </li>
                </ul>
                <div class="clearfix"></div>
            </div>
            <div class="x_content">
                <form  class="form-horizontal form-label-left bug" method="POST" action="">
                    <div class="form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">Test Name <span class="required">*</span>
                        </label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                            <input type="text" id="testname" name="bugs_name" required="required" class="form-control col-md-7 col-xs-12">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12">Version</label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                            <div id="version" class="btn-group" data-toggle="buttons">
                                <label class="btn btn-info active" data-toggle-class="btn-primary" data-toggle-passive-class="btn-default">
                                    <input type="radio" name="version" value="Development"> &nbsp; Development &nbsp;
                                </label>
                                <label class="btn btn-info " data-toggle-class="btn-primary" data-toggle-passive-class="btn-default">
                                    <input type="radio" name="version" value="Staging Server"> Staging Server
                                </label>
                                <label class="btn btn-info " data-toggle-class="btn-primary" data-toggle-passive-class="btn-default">
                                    <input type="radio" name="version" value="Production">  Production
                                </label>
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="url">Link <span class="required">*</span>
                        </label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                            <input type="url" id="url" name="link" required="required" class="form-control col-md-7 col-xs-12">
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="scenario" class="control-label col-md-3 col-sm-3 col-xs-12">Scenario Testing</label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                            <textarea id="scenario" class="summernote" type="text" name="skenario" ></textarea>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="ekspektasi" class="control-label col-md-3 col-sm-3 col-xs-12">expectations</label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                            <textarea id="ekspektasi" class="summernote" type="text" name="ekspektasi" ></textarea>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="result" class="control-label col-md-3 col-sm-3 col-xs-12">Result</label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                            <textarea id="result" class="summernote" type="text" name="result" ></textarea>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12">Result Status<span class="required">*</span>
                        </label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                            <p>
                            PASS
                            <input type="radio" class="flat radio_stat" name="status" id="genderM" value="1" checked="" required />
                            Pending:
                            <input type="radio" class="flat radio_stat" name="status" id="genderF" value="2" />
                            Failed:
                            <input type="radio" class="flat radio_stat" name="status" id="genderF" value="3" />
                            </p>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12">Result Priority<span class="required">*</span>
                        </label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                            <p>
                            <?php foreach ($priority as $priority) {
                                echo $priority->name.'<input type="radio" class="flat radio_stat" name="priority" id="radio" value="'.$priority->id.'" required />';
                            }?>
                            </p>
                        </div>
                    </div>
                     <div class="item form-group">
                        <div class="controls">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="textarea">Bugs Due Date <span class="required">*</span>
                            </label>
                            <div class="col-md-6 col-sm-6 col-xs-12">
                                <div class="input-prepend input-group">
                                    <span class="add-on input-group-addon"><i class="glyphicon glyphicon-calendar fa fa-calendar"></i></span>
                                    <input type="text" style="width: 200px" name="reservation" id="reservation" class="form-control" value="" required="required"/>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="result" class="control-label col-md-3 col-sm-3 col-xs-12">Suggestion</label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                            <textarea id="result" class="summernote" type="text" name="saran" ></textarea>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12"> </label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                            <a href="<?= isset($_SERVER['HTTP_REFERER']) ? $_SERVER['HTTP_REFERER'] : null?>" class="btn btn-default"> cancel</a>
                            <button id="send" type="submit" class="btn btn-success">Submit</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<?=$this->element('collapse-box')?>
<?= $this->Html->script("validator/validator.js")?>
<?= $this->Html->script("summernote/summernote.min.js")?>
<?=  $this->Html->script('icheck/icheck.min.js');?>
<?= $this->Html->script("moment.min2.js")?>
<?= $this->Html->script("datepicker/daterangepicker.js")?>
<script type="text/javascript">
    $(document).ready(function(){
    $(".summernote").summernote();
    });
    $(document).ready(function () {
        $('#reservation').daterangepicker({
            format: 'YYYY-MM-DD',
            }, function (start, end, label) {
                console.log(start.toISOString(), end.toISOString(), label);
        });
    });
</script>