<div class="actions columns large-2 medium-3">
    <h3><?= __('Actions') ?></h3>
    <ul class="side-nav">
        <li><?= $this->Html->link(__('List Priority'), ['action' => 'index']) ?></li>
    </ul>
</div>
<div class="priority form large-10 medium-9 columns">
    <?= $this->Form->create($priority) ?>
    <fieldset>
        <legend><?= __('Add Priority') ?></legend>
        <?php
            echo $this->Form->input('name');
            echo $this->Form->input('created_by');
        ?>
    </fieldset>
    <?= $this->Form->button(__('Submit')) ?>
    <?= $this->Form->end() ?>
</div>
