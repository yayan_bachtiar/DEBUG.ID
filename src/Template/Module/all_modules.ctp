<div class="" id="p_content">
    <div class="page-title">
        <div class="title_left">
            <h3><?= $project->project_name?></h3>
        </div>
        <div class="title_right">
            <div class="col-md-5 col-sm-5 col-xs-12 form-group pull-right top_search">
                <div class="input-group">
                    <input type="text" class="form-control" id="jetssearch" placeholder="Search for...">
                    <span class="input-group-btn">
                    <button class="btn btn-default" type="button">Go!</button>
                    </span>
                </div>
            </div>
        </div>
    </div>
    <div class="clearfix"></div>
    <div class="row" >
        <div class="col-md-12">
            <div class="x_panel">
                <div class="x_title">
                    <h2>Modules</h2>
                    <ul class="nav navbar-right panel_toolbox">
                        <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                        </li>
                        <li class="dropdown">
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i class="fa fa-wrench"></i></a>
                            <ul class="dropdown-menu" role="menu">
                                <li><a href="#">Settings 1</a>
                                </li>
                                <li><a href="#">Settings 2</a>
                                </li>
                            </ul>
                        </li>
                        <li><a class="close-link"><i class="fa fa-close"></i></a>
                        </li>
                    </ul>
                    <div class="clearfix"></div>
                </div>
                <div class="x_content">
                    <p>All Modules by <small><?= $project->project_name?></small></p>
                    <!-- start project list -->
                    <div class="pull-right">
                        <div class="button-group">
                            <?=  $this->Html->link('<i class="fa fa-plus"></i> add module', ['controller'=>'Module', 'action'=>'add', $project->id],['class'=>'btn btn-success btn-xs', 'escape'=>false]);?>
                        </div>
                    </div>
                    <table  class="table responsive-utilities ">
                        <thead>
                            <tr>
                                <th style="width: 1%">#</th>
                                <th style="width: 20%">Module Name</th>
                                <th>Excecutor</th>
                                <th>Module Progress</th>
                                <th>Status</th>
                                <th style="width: 20%">#Edit</th>
                            </tr>
                        </thead>
                        <tbody id="jetssearch_source">
                            <?php foreach ($module as $list) {?>
                            <tr>
                                <td>#</td>
                                <td>
                                    <a><?= $list->module_name?></a>
                                    <br />
                                    <small><?= $list->created?></small>
                                </td>
                                <td>
                                    <ul class="list-inline">
                                        <li>
                                            <img src="<?= $this->General->get_avatar($list->user->email)?>" class="avatar" alt="Avatar" title="<?= $list->user->username;?>">
                                        </li>
                                    </ul>
                                </td>
                                <script type="text/javascript">
                                $.ajax({
                                    url: "<?= $this->Url->build(['controller'=>'module', 'action'=>'get_statistic'])?>",
                                    type: 'GET',
                                    data: {id: '<?php echo $list->id;?>'},
                                    })
                                .done(function(data) {
                                    var json = jQuery.parseJSON(data);
                                    $("#stat<?php echo $list->id ?>").attr('style', "width:"+json.percent*100+"%");
                                    $("#num<?php echo $list->id ?>").html(json.percent*100+"%  Complete ("+json.finished+" finished of "+json.total+" total tasks) and (0 resolved of 100 bugs");
                                    // console.log("response");
                                })
                                .fail(function(response) {
                                    console.log(response);
                                })
                                .always(function(response) {
                                    console.log(response);
                                });

                                </script>
                                <td class="project_progress">
                                    <div class="progress">
                                        <div class="progress-bar bg-green" id="stat<?php echo $list->id;  ?>" role="progressbar" aria-valuenow="20" aria-valuemin="0" aria-valuemax="100" style="width:50%">
                                        </div>
                                    </div>
                                    <small id="num<?php echo $list->id;?>">50% Complete 0 finished of 0 total task</small>
                                </td>
                                <td>
                                    <button type="button" class="btn btn-success btn-xs"><?= $list->status->name?></button>
                                </td>
                                <td>
                                <?=$this->Html->link('<i class="fa fa-folder"></i> View ', ['action'=>'view', $list->id], ['class'=>'btn btn-primary btn-xs', 'escape'=>false])?>
                                <?=$this->Html->link('<i class="fa fa-pencil"></i> Edit ', ['action'=>'edit', $list->id], ['class'=>'btn btn-info btn-xs', 'escape'=>false])?>
                                    <a href="#" class="btn btn-danger btn-xs"><i class="fa fa-trash-o"></i> Delete </a>
                                </td>
                            </tr>
                            <?php } ?>
                        </tbody>
                    </table>
                    <!-- end project list -->
                    <!-- paginator -->
                    <div class="pull-right">
                        <ul class="pagination pagination" id="passed">
                            <?= $this->Paginator->prev('«', $options = ['model'=>'bpass']) ?>
                            <?= $this->Paginator->numbers() ?>
                            <?= $this->Paginator->next('»', $options = ['model'=>'bpass']) ?>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <?= $this->element('collapse-box');?>
    <?= $this->element('progress');?>
    <script type="text/javascript">
        $("ul#passed.pagination li a").bind("click", function (event) {
            var href = $(this).attr('href');
            $.ajax({
            success:function (response) {
                $("#p_content").html(response);
            },
            url: href,
            error:(function(response){
                location.reload();
                }),
            });
            return false;
        });
    </script>
        <?= $this->element('progress')?>
</div>