<div class="actions columns large-2 medium-3">
    <h3><?= __('Actions') ?></h3>
    <ul class="side-nav">
        <li><?= $this->Html->link(__('New Module'), ['action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Modul Comment'), ['controller' => 'ModulComment', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Modul Comment'), ['controller' => 'ModulComment', 'action' => 'add']) ?> </li>
    </ul>
</div>
<div class="module index large-10 medium-9 columns">
    <table cellpadding="0" cellspacing="0">
    <thead>
        <tr>
            <th><?= $this->Paginator->sort('id') ?></th>
            <th><?= $this->Paginator->sort('module_name') ?></th>
            <th><?= $this->Paginator->sort('project_id') ?></th>
            <th><?= $this->Paginator->sort('created') ?></th>
            <th><?= $this->Paginator->sort('modified') ?></th>
            <th><?= $this->Paginator->sort('created_by') ?></th>
            <th><?= $this->Paginator->sort('modified_by') ?></th>
            <th class="actions"><?= __('Actions') ?></th>
        </tr>
    </thead>
    <tbody>
    <?php foreach ($module as $module): ?>
        <tr>
            <td><?= $this->Number->format($module->id) ?></td>
            <td><?= h($module->module_name) ?></td>
            <td><?= $this->Number->format($module->project_id) ?></td>
            <td><?= $this->Number->format($module->created) ?></td>
            <td><?= $this->Number->format($module->modified) ?></td>
            <td><?= $this->Number->format($module->created_by) ?></td>
            <td><?= $this->Number->format($module->modified_by) ?></td>
            <td class="actions">
                <?= $this->Html->link(__('View'), ['action' => 'view', $module->id]) ?>
                <?= $this->Html->link(__('Edit'), ['action' => 'edit', $module->id]) ?>
                <?= $this->Form->postLink(__('Delete'), ['action' => 'delete', $module->id], ['confirm' => __('Are you sure you want to delete # {0}?', $module->id)]) ?>
            </td>
        </tr>

    <?php endforeach; ?>
    </tbody>
    </table>
    <div class="paginator">
        <ul class="pagination">
            <?= $this->Paginator->prev('< ' . __('previous')) ?>
            <?= $this->Paginator->numbers() ?>
            <?= $this->Paginator->next(__('next') . ' >') ?>
        </ul>
        <p><?= $this->Paginator->counter() ?></p>
    </div>
</div>
