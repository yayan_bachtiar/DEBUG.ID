<section class="panel default blue_border vertical_border h1 animated fadeInLeft">
    <div class="task-header blue_task">Module List
        <span class="input-group">
        <input type="text" placeholder="search" class="form-control"></span>
    </div>
    <div class="row task_inner inner_padding">
        <?php foreach ($module as $key ) {?>
        <div id="<?=$key->id?>">
            <div class="col-sm-8">
                <p><?= $key->module_name;?></p>
            </div>
            <div class="col-sm-4">
                <div class="pull-right">
                    <ul class="footer-icons-group">
                        <li><?= $this->Html->link('<i class="fa fa-eye"></i>', ['controller'=>'module', 'action'=>'view', $key->id], ['escape'=>false]);?></li>
                        <li><?= $this->Html->link('<i class="fa fa-bug"></i>', ['controller'=>'bugs', 'action'=>'add', $key->id], ['escape'=>false]);?></li>
                        <li><a href="javascript:hapus_module(<?=$key->id?>)"  onclick="if (confirm('Are you sure you want to delete # <?= $key->module_name;?>')) { return true; } return false;"><i class="fa fa-times"></i></a></li>
                    </ul>
                </div>
            </div>
        </div>
        <?php }
        if(empty($module)){?>
        <div class="col-sm-8">
            <p>No Module Available</p>
        </div>
        <div class="col-sm-4"></div>
        <?php }?>
    </div>
    <div class="task-footer">
        <label class="pull-left">
            <div class="progress ">
                <div style="width: <?=$percent?>%;" aria-valuemax="100" aria-valuemin="0" aria-valuenow="60" role="progressbar" class="progress-bar progress-bar-primary"> <span class="sr-only"><?=$percent?>% Complete</span> </div>
            </div>
        </label>
        <span class="label btn-primary"><?=$percent?>%</span>
        <div class="pull-right">
            <ul class="footer-icons-group">
                <li><?= $this->Html->link('<i class="fa fa-plus"></i>', ['controller'=>'module', 'action'=>'add', $project_id], ['escape'=>false])?></li>
                <li><a href="#"><i class="fa fa-trash-o"></i></a></li>
                <li><a href="#" class="refresh" onclick="get_module()"><i class="fa fa-repeat"></i></a></li>
                <li class="dropup"><a data-toggle="dropdown" class="dropdown-toggle" href="#"><i class="fa fa-wrench"></i></a></li>
            </ul>
        </div>
    </div>
</section>
<script type="text/javascript">
    function hapus_module(id_module) {
        $.ajax({
url: "<?= $this->Url->build(['controller'=>'module', 'action'=>'delete']);?>",
data: {id: id_module},
type: 'GET',
// dataType: 'default: Intelligent Guess (Other values: xml, json, script, or html)',
success:function() {
console.log("success");
$("#"+id_module).hide();
toastr.success("Data has ben deleted");
}
});
};
</script>