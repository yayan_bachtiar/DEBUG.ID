<div class="container">
    <div class="row">
        <div class="col s12 l9">
            <div class="project form large-10 medium-9 columns">
                <br>
                <?php
                $this->loadHelper('Form', [
                    'templates' => 'app_form',
                ]);?>
                <div class="">
                    <?php echo  $this->Form->create($module); 
                    echo $this->Form->input('module_name', ['length'=>40]);
                    echo $this->Form->input('module_desc', ['class'=>'materialize-textarea', 'length'=>255]);
                    ?>
                    <div class="input-field col s6">
                        <input id="project_start" name="start_date" type="date" class="datepicker" autocomplete='off' value="<?=$module->start_date;?>">
                        <label for="project_start" class="active">Start Date</label>
                    </div>
                    <div class="input-field col s6">
                        <input id="project_end" name="end_date" type="date" class="datepicker" autocomplete='off' value="<?=$module->end_date?>">
                        <label for="project_end" class="active">End Date</label>
                    </div>
                    <a class="btn waves-effect waves-light orange" href="<?= $this->Url->build(['controller'=>'module', 'action'=>'view', $module->id]);?>">Cancel
                    <i class="material-icons left">replay</i>
                    </a>
                    <button class="btn waves-effect waves-light" type="submit" name="action">Save
                    <i class="material-icons right">system_update_alt</i>
                    </button>
                    <?= $this->Form->end() ?>
                </div>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript">
$(document).ready (function(){
    $('.datepicker').pickadate({
    selectMonths: true, // Creates a dropdown to control month
    selectYears: 15 // Creates a dropdown of 15 years to control year
  });  
});
</script>