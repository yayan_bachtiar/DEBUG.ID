<div class="clar_both padding_fix">
    <div class="row">
        <div class="col-sm-12">
            <a type="button" class="btn btn-primary add_task" href="<?= $this->Url->build(['controller'=>'task', 'action'=>'add', $module->id])?>" onclick="get_data(this)" id='Task'><i class="fa fa-plus-square"></i> Add Task</a>
            <a type="button" class="btn btn-primary " href="<?= $this->Url->build(['controller'=>'bugs', 'action'=>'add', $module->id])?>" onclick="get_data(this)" id='Bugs'><i class="fa fa-bug"></i> Add Bug</a>
            <span class="btn btn-warning pull-right" >Due Date : <i class="fa fa-clock-o"></i>&nbsp;<span data-livestamp="<?= strtotime($module->end_date)?>">&nbsp; 3 days &nbsp;&nbsp;&nbsp;</span></span>
        </div>
        <div class="col-sm-9 col-xs-12">
            <div class="x_panel">
                <div class="x_title">
                    <h2><i class="fa fa-card"></i> User Story Detail </h2>
                    <ul class="nav navbar-right panel_toolbox">
                        <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                        </li>
                        <li class="dropdown">
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i class="fa fa-wrench"></i></a>
                            <ul class="dropdown-menu" role="menu">
                                <li><a href="#">Settings 1</a>
                                </li>
                                <li><a href="#">Settings 2</a>
                                </li>
                            </ul>
                        </li>
                        <li><a class="close-link"><i class="fa fa-close"></i></a>
                        </li>
                    </ul>
                    <div class="clearfix"></div>
                </div>
                <div class="x_content">
                    <h3 class="title"><?= $module->module_name?></h3>
                    <blockquote>
                        <?php echo $module->module_desc; ?>
                        <footer><?= $module->user->role?> <cite title="Source Title"><?= $module->user->first_name.' '.$module->user->last_name?></cite>
                        </footer>
                    </blockquote>
                    <div class="" role="tabpanel" data-example-id="togglable-tabs">
                        <ul id="myTab" class="nav nav-tabs bar_tabs" role="tablist">
                            <li role="presentation" class="active"><a href="#tab_content1" id="home-tab" role="tab" data-toggle="tab" aria-expanded="true" onclick="get_task()">Task</a>
                            </li>
                            <li role="presentation" class=""><a href="#tab_content2" role="tab" id="profile-tab" data-toggle="tab"  aria-expanded="false" onclick="get_bugs()">Bugs</a>
                            </li>
                            <li role="presentation" class=""><a href="#tab_content3" role="tab" id="profile-tab2" data-toggle="tab" aria-expanded="false" onclick="get_log();">Logs</a>
                            </li>
                            <li role="presentation" class=""><a href="#tab_content4" role="tab" id="attachment-tab2" data-toggle="tab" aria-expanded="false" onclick="get_attachment()">File Attachment</a>
                            </li>
                        </ul>
                        <div id="myTabContent" class="tab-content">
                            <div role="tabpanel" class="tab-pane fade active in" id="tab_content1" aria-labelledby="home-tab">
                                <div id="t_content">
                                </div>
                            </div>
                            <div role="tabpanel" class="tab-pane fade" id="tab_content2" aria-labelledby="profile-tab">
                                <div id="b_content">
                                </div>
                            </div>
                            <div role="tabpanel" class="tab-pane fade" id="tab_content3" aria-labelledby="profile-tab">
                                <div id="l_content">

                                </div>
                            </div>
                            <div role="tabpanel" class="tab-pane fade" id="tab_content4" aria-labelledby="profile-tab">
                                <div id="a_content">

                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-3 col-sm-3 col-xs-12">
            <div class="x_panel">
            <h2 class="text-center bold">Excecutor</h2>
                <div class="c_content profile_right">

                </div>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript">
    user_badge();
    get_task();
    $("a.add_task").bind("click", function (event) {
        var href = $(this).attr('href');
        $.confirm({
            title: 'Add Tasks!',
            content: 'url:'+href,
            confirm: function(){
                // var name = $("#task_name").val();
                // console.log(name);
                $.ajax({
                    type: 'post',
                    url: href,
                    data: $('form#add_task').serialize(),
                    success: function (response) {
                        get_task();
                        toastr.success("Tasks has been added");
                    }
                });
            },
            cancel: function(){
                console.log('far');
            }
        });
        return false;
    });

    function get_task(){
    $("#t_content").html('<?=$this->Html->image('default.svg')?>');
    $.ajax({
            url: "<?= $this->Url->build(['controller'=>'task', 'action'=>'get_task'])?>",
            type: 'GET',
            data: {id: "<?php echo $module->id;?>"},
        })
        .done(function(response) {
        $("#t_content").html(response);
            console.log("success");
        })
        .fail(function() {
            console.log("error");
        })
        .always(function() {
            console.log("complete");
        });
    }
    function get_attachment(){
    $("#a_content").html('<?=$this->Html->image('default.svg')?>');
    $.ajax({
            url: "<?= $this->Url->build(['controller'=>'moduleAttachment', 'action'=>'get_attachment'])?>",
            type: 'GET',
            data: {id: "<?php echo $module->id;?>"},
        })
        .done(function(response) {
        $("#a_content").html(response);
            console.log("success");
        })
        .fail(function() {
            console.log("error");
        })
        .always(function() {
            console.log("complete");
        });
    }

    function get_bugs(){
    $("#b_content").html('<?=$this->Html->image('default.svg')?>');

        $.ajax({
            url: "<?= $this->Url->build(['controller'=>'bugs', 'action'=>'get_bugs'])?>",
            type: 'GET',
            data: {id: "<?php echo $module->id;?>"},
        })
        .done(function(response) {
        $("#b_content").html(response);
            console.log("success");
        })
        .fail(function() {
            consoleLog(msg).log("error");
        })
        .always(function() {
            console.log("complete");
        });
    }

    function get_log(){
    $("#l_content").html('<?=$this->Html->image('default.svg')?>');

        $.ajax({
            url: "<?= $this->Url->build(['controller'=>'ProjectLog', 'action'=>'get_log']);?>",
            type: 'GET',
            data: {mod_id: "<?= $module->id;?>"},
        })
        .done(function(response) {
        $("#l_content").html(response);
            console.log("success");
        })
        .fail(function() {
            console.log("error");
        })
        .always(function() {
            console.log("complete");
        });
    }


    function user_badge(){
    $(".profile_right").html('<?=$this->Html->image('default.svg')?>');

        $.ajax({
            url: "<?= $this->Url->build(['controller'=>'user', 'action'=>'user_badge'])?>",
            type: 'GET',
            data: {id: '<?php echo $module->module_excecutor; ?>'},
        })
        .done(function(response) {
        $(".profile_right").html(response);

            console.log("success");
        })
        .fail(function() {
            console.log("error");
        })
        .always(function() {
            console.log("complete");
        });

    }
</script>
