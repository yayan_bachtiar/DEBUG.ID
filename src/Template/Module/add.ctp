<?= $this->Html->css("summernote/summernote.css");?>
<?= $this->Html->css("select/select2.min.css");?>
<div class="clearfix"></div>
<div class="row">
    <div class="col-md-12 col-sm-12 col-xs-12">
        <div class="x_panel">
            <div class="x_title">
                <h2>User Story <small>add new User Sory</small></h2>
                <ul class="nav navbar-right panel_toolbox">
                    <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                    </li>
                    <li><a class="close-link"><i class="fa fa-close"></i></a>
                    </li>
                </ul>
                <div class="clearfix"></div>
            </div>
            <div class="x_content">
                <div id="wizard" class="form_wizard wizard_horizontal">
                    <ul class="wizard_steps">
                        <li>
                            <a href="#step-1">
                            <span class="step_no">1</span>
                            <span class="step_descr">
                            Step 1<br />
                            <small>User Story Detail</small>
                            </span>
                            </a>
                        </li>
                        <li>
                            <a href="#step-2">
                            <span class="step_no">2</span>
                            <span class="step_descr">
                            Step 2<br />
                            <small>User Story Scenario</small>
                            </span>
                            </a>
                        </li>
                    </ul>
                    <?php $this->Form->templates($form_templates['shortForm']); ?>
                    <?php echo $this->Form->create($module, ["class"=>"form-horizontal form-label-left module",  "novalidate", "method"=>"POST", "enctype"=>"multipart/form-data"]);?>
                        <div id="step-1">
                        <?php echo $this->Form->input('module_name') ?>
                        <?php echo $this->Form->input('module_excecutor', ['type'=>'select', 'options'=>$ProjectParticipant]) ?>
                        <?php echo $this->Form->input('module_desc', ['class'=>'summernote', 'style'=>'min-height:200px"']) ?>
                            <div class="item form-group">
                                <label class="control-label col-md-3 col-sm-3 col-xs-12" for="name">User Story Name <span class="required">*</span>
                                </label>
                                <div class="col-md-6 col-sm-6 col-xs-12">
                                    <input id="name" class="form-control col-md-7 col-xs-12" data-validate-length-range="6" name="module_name" required="required" type="text">
                                </div>
                            </div>
                            <div class="item form-group">
                                <label class="control-label col-md-3 col-sm-3 col-xs-12" for="textarea">User Storiy Description<span class="required">*</span>
                                </label>
                                <div class="col-md-6 col-sm-6 col-xs-12">
                                    <textarea id="summernote" required="required" name="module_desc" class="form-control col-md-7 col-xs-12" style="min-height:200px" ></textarea >
                                </div>
                            </div>
                            <div class="item form-group">
                                <div class="controls">
                                    <label class="control-label col-md-3 col-sm-3 col-xs-12" for="textarea">User Story Due Date <span class="required">*</span>
                                    </label>
                                    <div class="col-md-6 col-sm-6 col-xs-12">
                                        <div class="input-prepend input-group">
                                            <span class="add-on input-group-addon"><i class="glyphicon glyphicon-calendar fa fa-calendar"></i></span>
                                            <input type="text" style="width: 200px" name="reservation" id="reservation" class="form-control" value="" required="required"/>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="item form-group">
                                <label class="control-label col-md-3 col-sm-3 col-xs-12" for="number">Owner <span class="required">*</span>
                                </label>
                                <div class="col-md-6 col-sm-6 col-xs-12">
                                    <select id="heard" class="select2_single form-control" required="required" name="executor">
                                        <option value="" >Choose..</option>
                                        <?php
                                            $selected = '';
                                            foreach ($ProjectParticipant as $key) {
                                                $user = $key->user;
                                                echo "<option value='$user->id'>";
                                                echo $user->username."</option>";
                                        }?>
                                    </select>
                                </div>
                            </div>
                            <div class="item form-group">
                                <label class="control-label col-md-3 col-sm-3 col-xs-12" for="textarea">Plan Estimate<span class="required">*</span>
                                </label>
                                <div class="col-md-6 col-sm-6 col-xs-12" >
                                    <input required="required" name="point" type="number" class="form-control col-md-7 col-xs-12" ><span class="form-control-feedback right" aria-hidden="true" style="right:40px!important"> Point</span>
                                </div>
                            </div>
                        </div>
                        <div id="step-2">
                            <h2 class="StepTitle">Step 2 Content</h2>
                            <div class="item form-group">
                                <a href="#" class="btn btn-primary btn-xs" onclick="add_attachment()"><i class="fa fa-plus"></i> Add Attachment</a>
                            </div>
                            <div id="div1">
                                <div class="item form-group">
                                    <label class="control-label col-md-3 col-sm-3 col-xs-12">Attachment
                                    </label>
                                    <div class="col-md-6 col-sm-6 col-xs-12" >
                                        <input name="gambar[]" type="file" class="form-control col-md-7 col-xs-12" >
                                    </div>
                                </div>
                                <div class="item form-group">
                                    <label class="control-label col-md-3 col-sm-3 col-xs-12">Description
                                    </label>
                                    <div class="col-md-6 col-sm-6 col-xs-12" >
                                        <input name="description[]" class="form-control col-md-7 col-xs-12" >
                                    </div>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
<?=$this->element('collapse-box')?>
<?= $this->Html->script("validator/validator.js")?>
<?= $this->Html->script("moment.min2.js")?>
<?= $this->Html->script("datepicker/daterangepicker.js")?>
<?= $this->Html->script("select/select2.full.js")?>
<?= $this->Html->script("summernote/summernote.min.js")?>
<?= $this->Html->script("wizard/jquery.smartWizard.js");?>
<script type="text/javascript">
</script>
<script>
    function add_attachment(){
        var child = $('#div1').clone();
        $("#step-2").append(child);
    }
    $(document).ready(function () {
        jQuery.fn.outerHTML = function() {
                return jQuery('<div />').append(this.eq(0).clone()).html();
                };
                    // Smart Wizard
                $('#wizard').smartWizard({
                    // onLeaveStep:leaveAStepCallback,
                    onFinish:onFinishCallback
                });
                // function leaveAStepCallback(obj, context){
                        //     // alert("Leaving step " + context.fromStep + " to go to step " + context.toStep);
                        //     // return validateSteps(context.fromStep); // return false to stay on step and true to continue navigation
                // }
                function onFinishCallback() {
                    // $('#wizard').smartWizard('showMessage', 'Finish Clicked');
                    var endors = $('form').serialize();
                    console.log(endors);
                }
        });
        $(document).ready(function () {
            $(".select2_single").select2({
                placeholder: "Select a user",
                allowClear: true
            });
            $(".summernote").summernote({
                height: 150,
                focus:true
            });
        });
    </script>
    <script type="text/javascript">
    $(document).ready(function () {
        $('#reservation').daterangepicker({
            timePicker: true,
            timePickerIncrement: 1,
            format: 'MM/DD/YYYY h:mm A',
            }, function (start, end, label) {
                console.log(start.toISOString(), end.toISOString(), label);
            });
    });
    </script>
    <script>
            // initialize the validator function
            validator.message['date'] = 'not a real date';
            // validate a field on "blur" event, a 'select' on 'change' event & a '.reuired' classed multifield on 'keyup':
            $('form')
                .on('blur', 'input[required], input.optional, select.required', validator.checkField)
                .on('change', 'select.required', validator.checkField)
                .on('keypress', 'input[required][pattern]', validator.keypress);
            $('.multi.required')
                .on('keyup blur', 'input', function () {
                    validator.checkField.apply($(this).siblings().last()[0]);
                });
            // bind the validation to the form submit event
            //$('#send').click('submit');//.prop('disabled', true);
            $('form').submit(function (e) {
                e.preventDefault();
                var submit = true;
                // evaluate the form using generic validaing
                if (!validator.checkAll($(this))) {
                    submit = false;
                }
                if (submit)
                    this.submit();
                return false;
            });
            /* FOR DEMO ONLY */
            $('#vfields').change(function () {
                $('form').toggleClass('mode2');
            }).prop('checked', false);
            $('#alerts').change(function () {
                validator.defaults.alerts = (this.checked) ? false : true;
                if (this.checked)
                    $('form .alert').remove();
            }).prop('checked', false);
    </script>