<?= $this->Html->css('dropzone.css');?>
<br>
<div class="container">
    <div class="row">
        <div class="col l12 center">
            <?php echo $this->Form->create(null, ['class'=>'dropzone']);?>
            <?php echo $this->Form->end();?>
        </div>
    </div>
    <a class="btn waves-effect waves-light" href="<?= $this->Url->build(['controller'=>'project', 'action'=>'view',$id]);?>">Back
    <i class="material-icons left">replay</i>
    </a>
</div>
<?= $this->Html->script('dropzone.js');?>