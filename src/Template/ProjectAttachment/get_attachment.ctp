<section class="panel default blue_title h2 animated fadeInRight">
    <div class="panel-heading">
        File Attachment
        <a href="#"><span class="pull-right badge"><?= $number ?> Files</span></a>
    </div>
    <div class="panel-body">
        <ul class="list-group">
            <?php $i=0;
            foreach ($projectAttachment as $key ) {?>
            <li class="list-group-item" id='<?=$key->id;?>'>
            <div class="pull-right">
                <a href="<?= $this->request->webroot.'webroot/'. $key->path.'/'.$key->filename; ?>" class="" target="_blank">
                <i class="fa fa-download"></i>
                </a>
                <span>&nbsp; </span>
                <a href="javascript:hapus(<?=$key->id?>)"  onclick="if (confirm('Are you sure you want to delete # <?= $key->filename;?>')) { return true; } return false;"><i class="fa fa-times"></i></a>
                <span>&nbsp; </span>
            </div>
                <i class="fa fa-file-pdf-o"></i>
                <?= $key->filename;?>
            </li>
            <?php $i++;
                                if($i==5){
                                    break;
                                    }
            }?>
        </ul>
        <p class="text-center">
        <?= $this->Html->link('<i class="fa fa-cloud-upload"></i>', ['controller'=>'projectAttachment', 'action'=>'add?pid='.$this->Encryptor->encryptor('encrypt', $project_id)], ['class'=>'btn', 'escape'=>false]);?>
        </p>
    </div>
</section>
<script type="text/javascript">
    function hapus(id_attachment) {
        $.ajax({
            url: "<?= $this->Url->build(['controller'=>'projectAttachment', 'action'=>'delete']);?>",
            data: {id: id_attachment},
            type: 'GET',
            // dataType: 'default: Intelligent Guess (Other values: xml, json, script, or html)',
            success:function() {
                console.log("success");
                $("#"+id_attachment).hide();
                toastr.success("Data has ben deleted");
            }
        });
    };
</script>
