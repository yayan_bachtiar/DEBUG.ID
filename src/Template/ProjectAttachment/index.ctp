<div class="actions columns large-2 medium-3">
    <h3><?= __('Actions') ?></h3>
    <ul class="side-nav">
        <li><?= $this->Html->link(__('New Project Attachment'), ['action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Module'), ['controller' => 'Module', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Module'), ['controller' => 'Module', 'action' => 'add']) ?> </li>
    </ul>
</div>
<div class="projectAttachment index large-10 medium-9 columns">
    <table cellpadding="0" cellspacing="0">
    <thead>
        <tr>
            <th><?= $this->Paginator->sort('id') ?></th>
            <th><?= $this->Paginator->sort('project_id') ?></th>
            <th><?= $this->Paginator->sort('path') ?></th>
            <th><?= $this->Paginator->sort('ext') ?></th>
            <th><?= $this->Paginator->sort('filename') ?></th>
            <th><?= $this->Paginator->sort('upload_date') ?></th>
            <th><?= $this->Paginator->sort('upload_by') ?></th>
            <th class="actions"><?= __('Actions') ?></th>
        </tr>
    </thead>
    <tbody>
    <?php foreach ($projectAttachment as $projectAttachment): ?>
        <tr>
            <td><?= $this->Number->format($projectAttachment->id) ?></td>
            <td>
                <?= $projectAttachment->has('module') ? $this->Html->link($projectAttachment->module->id, ['controller' => 'Module', 'action' => 'view', $projectAttachment->module->id]) : '' ?>
            </td>
            <td><?= h($projectAttachment->path) ?></td>
            <td><?= h($projectAttachment->ext) ?></td>
            <td><?= h($projectAttachment->filename) ?></td>
            <td><?= h($projectAttachment->upload_date) ?></td>
            <td><?= $this->Number->format($projectAttachment->upload_by) ?></td>
            <td class="actions">
                <?= $this->Html->link(__('View'), ['action' => 'view', $projectAttachment->id]) ?>
                <?= $this->Html->link(__('Edit'), ['action' => 'edit', $projectAttachment->id]) ?>
                <?= $this->Form->postLink(__('Delete'), ['action' => 'delete', $projectAttachment->id], ['confirm' => __('Are you sure you want to delete # {0}?', $projectAttachment->id)]) ?>
            </td>
        </tr>

    <?php endforeach; ?>
    </tbody>
    </table>
    <div class="paginator">
        <ul class="pagination">
            <?= $this->Paginator->prev('< ' . __('previous')) ?>
            <?= $this->Paginator->numbers() ?>
            <?= $this->Paginator->next(__('next') . ' >') ?>
        </ul>
        <p><?= $this->Paginator->counter() ?></p>
    </div>
</div>
