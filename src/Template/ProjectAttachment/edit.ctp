<div class="actions columns large-2 medium-3">
    <h3><?= __('Actions') ?></h3>
    <ul class="side-nav">
        <li><?= $this->Form->postLink(
                __('Delete'),
                ['action' => 'delete', $projectAttachment->id],
                ['confirm' => __('Are you sure you want to delete # {0}?', $projectAttachment->id)]
            )
        ?></li>
        <li><?= $this->Html->link(__('List Project Attachment'), ['action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('List Module'), ['controller' => 'Module', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Module'), ['controller' => 'Module', 'action' => 'add']) ?> </li>
    </ul>
</div>
<div class="projectAttachment form large-10 medium-9 columns">
    <?= $this->Form->create($projectAttachment); ?>
    <fieldset>
        <legend><?= __('Edit Project Attachment') ?></legend>
        <?php
            echo $this->Form->input('project_id', ['options' => $module]);
            echo $this->Form->input('path');
            echo $this->Form->input('ext');
            echo $this->Form->input('filename');
            echo $this->Form->input('upload_date');
            echo $this->Form->input('upload_by');
        ?>
    </fieldset>
    <?= $this->Form->button(__('Submit')) ?>
    <?= $this->Form->end() ?>
</div>
