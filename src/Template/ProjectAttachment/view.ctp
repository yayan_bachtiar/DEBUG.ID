<div class="actions columns large-2 medium-3">
    <h3><?= __('Actions') ?></h3>
    <ul class="side-nav">
        <li><?= $this->Html->link(__('Edit Project Attachment'), ['action' => 'edit', $projectAttachment->id]) ?> </li>
        <li><?= $this->Form->postLink(__('Delete Project Attachment'), ['action' => 'delete', $projectAttachment->id], ['confirm' => __('Are you sure you want to delete # {0}?', $projectAttachment->id)]) ?> </li>
        <li><?= $this->Html->link(__('List Project Attachment'), ['action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Project Attachment'), ['action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Module'), ['controller' => 'Module', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Module'), ['controller' => 'Module', 'action' => 'add']) ?> </li>
    </ul>
</div>
<div class="projectAttachment view large-10 medium-9 columns">
    <h2><?= h($projectAttachment->id) ?></h2>
    <div class="row">
        <div class="large-5 columns strings">
            <h6 class="subheader"><?= __('Module') ?></h6>
            <p><?= $projectAttachment->has('module') ? $this->Html->link($projectAttachment->module->id, ['controller' => 'Module', 'action' => 'view', $projectAttachment->module->id]) : '' ?></p>
            <h6 class="subheader"><?= __('Path') ?></h6>
            <p><?= h($projectAttachment->path) ?></p>
            <h6 class="subheader"><?= __('Ext') ?></h6>
            <p><?= h($projectAttachment->ext) ?></p>
            <h6 class="subheader"><?= __('Filename') ?></h6>
            <p><?= h($projectAttachment->filename) ?></p>
        </div>
        <div class="large-2 columns numbers end">
            <h6 class="subheader"><?= __('Id') ?></h6>
            <p><?= $this->Number->format($projectAttachment->id) ?></p>
            <h6 class="subheader"><?= __('Upload By') ?></h6>
            <p><?= $this->Number->format($projectAttachment->upload_by) ?></p>
        </div>
        <div class="large-2 columns dates end">
            <h6 class="subheader"><?= __('Upload Date') ?></h6>
            <p><?= h($projectAttachment->upload_date) ?></p>
        </div>
    </div>
</div>
