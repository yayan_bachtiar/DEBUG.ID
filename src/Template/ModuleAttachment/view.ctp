<div class="actions columns large-2 medium-3">
    <h3><?= __('Actions') ?></h3>
    <ul class="side-nav">
        <li><?= $this->Html->link(__('Edit Module Attachment'), ['action' => 'edit', $moduleAttachment->id]) ?> </li>
        <li><?= $this->Form->postLink(__('Delete Module Attachment'), ['action' => 'delete', $moduleAttachment->id], ['confirm' => __('Are you sure you want to delete # {0}?', $moduleAttachment->id)]) ?> </li>
        <li><?= $this->Html->link(__('List Module Attachment'), ['action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Module Attachment'), ['action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Module'), ['controller' => 'Module', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Module'), ['controller' => 'Module', 'action' => 'add']) ?> </li>
    </ul>
</div>
<div class="moduleAttachment view large-10 medium-9 columns">
    <h2><?= h($moduleAttachment->id) ?></h2>
    <div class="row">
        <div class="large-5 columns strings">
            <h6 class="subheader"><?= __('Module') ?></h6>
            <p><?= $moduleAttachment->has('module') ? $this->Html->link($moduleAttachment->module->id, ['controller' => 'Module', 'action' => 'view', $moduleAttachment->module->id]) : '' ?></p>
            <h6 class="subheader"><?= __('Path') ?></h6>
            <p><?= h($moduleAttachment->path) ?></p>
            <h6 class="subheader"><?= __('Ext') ?></h6>
            <p><?= h($moduleAttachment->ext) ?></p>
            <h6 class="subheader"><?= __('Filename') ?></h6>
            <p><?= h($moduleAttachment->filename) ?></p>
        </div>
        <div class="large-2 columns numbers end">
            <h6 class="subheader"><?= __('Id') ?></h6>
            <p><?= $this->Number->format($moduleAttachment->id) ?></p>
            <h6 class="subheader"><?= __('Upload By') ?></h6>
            <p><?= $this->Number->format($moduleAttachment->upload_by) ?></p>
        </div>
        <div class="large-2 columns dates end">
            <h6 class="subheader"><?= __('Upload Date') ?></h6>
            <p><?= h($moduleAttachment->upload_date) ?></p>
        </div>
    </div>
</div>
