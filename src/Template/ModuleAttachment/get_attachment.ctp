<div class="row">
    <?php foreach ($module_attach as $key ) {?>
    <div class="col-md-55">
        <div class="">
            <div class="image view view-first">
            <?php if($key->ext=='jpg'){ ?>
                <?php echo $this->Html->image('../'.$key->path.'/'.$key->filename, ['fullBase' => true,'style'=>'width: 100%; display: block;']); ?>
                <?php }else{?>
                    <i class="fa fa-file-pdf-o" style="font-size:12em"></i>
                    <?php } ?>
                <div class="mask">
                    <p><?=$key->filename?></p>
                    <div class="tools tools-bottom">
                        <a href="#"><i class="fa fa-link"></i></a>
                        <a href="#"><i class="fa fa-pencil"></i></a>
                        <a href="#"><i class="fa fa-times"></i></a>
                    </div>
                </div>
            </div>
            <!-- <div class="caption"> -->
                <!-- <p>Snow and Ice Incoming for the South</p> -->
            <!-- </div> -->
        </div>
    </div>
    <?php }?>
</div>