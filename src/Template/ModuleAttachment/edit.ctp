<div class="actions columns large-2 medium-3">
    <h3><?= __('Actions') ?></h3>
    <ul class="side-nav">
        <li><?= $this->Form->postLink(
                __('Delete'),
                ['action' => 'delete', $moduleAttachment->id],
                ['confirm' => __('Are you sure you want to delete # {0}?', $moduleAttachment->id)]
            )
        ?></li>
        <li><?= $this->Html->link(__('List Module Attachment'), ['action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('List Module'), ['controller' => 'Module', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Module'), ['controller' => 'Module', 'action' => 'add']) ?> </li>
    </ul>
</div>
<div class="moduleAttachment form large-10 medium-9 columns">
    <?= $this->Form->create($moduleAttachment); ?>
    <fieldset>
        <legend><?= __('Edit Module Attachment') ?></legend>
        <?php
            echo $this->Form->input('modul_id', ['options' => $module]);
            echo $this->Form->input('path');
            echo $this->Form->input('ext');
            echo $this->Form->input('filename');
            echo $this->Form->input('upload_date');
            echo $this->Form->input('upload_by');
        ?>
    </fieldset>
    <?= $this->Form->button(__('Submit')) ?>
    <?= $this->Form->end() ?>
</div>
