<div class="actions columns large-2 medium-3">
    <h3><?= __('Actions') ?></h3>
    <ul class="side-nav">
        <li><?= $this->Html->link(__('New Module Attachment'), ['action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Module'), ['controller' => 'Module', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Module'), ['controller' => 'Module', 'action' => 'add']) ?> </li>
    </ul>
</div>
<div class="moduleAttachment index large-10 medium-9 columns">
    <table cellpadding="0" cellspacing="0">
    <thead>
        <tr>
            <th><?= $this->Paginator->sort('id') ?></th>
            <th><?= $this->Paginator->sort('modul_id') ?></th>
            <th><?= $this->Paginator->sort('path') ?></th>
            <th><?= $this->Paginator->sort('ext') ?></th>
            <th><?= $this->Paginator->sort('filename') ?></th>
            <th><?= $this->Paginator->sort('upload_date') ?></th>
            <th><?= $this->Paginator->sort('upload_by') ?></th>
            <th class="actions"><?= __('Actions') ?></th>
        </tr>
    </thead>
    <tbody>
    <?php foreach ($moduleAttachment as $moduleAttachment): ?>
        <tr>
            <td><?= $this->Number->format($moduleAttachment->id) ?></td>
            <td>
                <?= $moduleAttachment->has('module') ? $this->Html->link($moduleAttachment->module->id, ['controller' => 'Module', 'action' => 'view', $moduleAttachment->module->id]) : '' ?>
            </td>
            <td><?= h($moduleAttachment->path) ?></td>
            <td><?= h($moduleAttachment->ext) ?></td>
            <td><?= h($moduleAttachment->filename) ?></td>
            <td><?= h($moduleAttachment->upload_date) ?></td>
            <td><?= $this->Number->format($moduleAttachment->upload_by) ?></td>
            <td class="actions">
                <?= $this->Html->link(__('View'), ['action' => 'view', $moduleAttachment->id]) ?>
                <?= $this->Html->link(__('Edit'), ['action' => 'edit', $moduleAttachment->id]) ?>
                <?= $this->Form->postLink(__('Delete'), ['action' => 'delete', $moduleAttachment->id], ['confirm' => __('Are you sure you want to delete # {0}?', $moduleAttachment->id)]) ?>
            </td>
        </tr>

    <?php endforeach; ?>
    </tbody>
    </table>
    <div class="paginator">
        <ul class="pagination">
            <?= $this->Paginator->prev('< ' . __('previous')) ?>
            <?= $this->Paginator->numbers() ?>
            <?= $this->Paginator->next(__('next') . ' >') ?>
        </ul>
        <p><?= $this->Paginator->counter() ?></p>
    </div>
</div>
