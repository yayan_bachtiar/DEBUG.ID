<div class="actions columns large-2 medium-3">
    <h3><?= __('Actions') ?></h3>
    <ul class="side-nav">
        <li><?= $this->Html->link(__('New Bugs Attachment'), ['action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Bugs'), ['controller' => 'Bugs', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Bug'), ['controller' => 'Bugs', 'action' => 'add']) ?></li>
    </ul>
</div>
<div class="bugsAttachment index large-10 medium-9 columns">
    <table cellpadding="0" cellspacing="0">
    <thead>
        <tr>
            <th><?= $this->Paginator->sort('id') ?></th>
            <th><?= $this->Paginator->sort('bugs_id') ?></th>
            <th><?= $this->Paginator->sort('path') ?></th>
            <th><?= $this->Paginator->sort('ext') ?></th>
            <th><?= $this->Paginator->sort('filename') ?></th>
            <th><?= $this->Paginator->sort('upload_date') ?></th>
            <th><?= $this->Paginator->sort('upload_by') ?></th>
            <th class="actions"><?= __('Actions') ?></th>
        </tr>
    </thead>
    <tbody>
    <?php foreach ($bugsAttachment as $bugsAttachment): ?>
        <tr>
            <td><?= $this->Number->format($bugsAttachment->id) ?></td>
            <td>
                <?= $bugsAttachment->has('bug') ? $this->Html->link($bugsAttachment->bug->id, ['controller' => 'Bugs', 'action' => 'view', $bugsAttachment->bug->id]) : '' ?>
            </td>
            <td><?= h($bugsAttachment->path) ?></td>
            <td><?= h($bugsAttachment->ext) ?></td>
            <td><?= h($bugsAttachment->filename) ?></td>
            <td><?= h($bugsAttachment->upload_date) ?></td>
            <td><?= $this->Number->format($bugsAttachment->upload_by) ?></td>
            <td class="actions">
                <?= $this->Html->link(__('View'), ['action' => 'view', $bugsAttachment->id]) ?>
                <?= $this->Html->link(__('Edit'), ['action' => 'edit', $bugsAttachment->id]) ?>
                <?= $this->Form->postLink(__('Delete'), ['action' => 'delete', $bugsAttachment->id], ['confirm' => __('Are you sure you want to delete # {0}?', $bugsAttachment->id)]) ?>
            </td>
        </tr>

    <?php endforeach; ?>
    </tbody>
    </table>
    <div class="paginator">
        <ul class="pagination">
            <?= $this->Paginator->prev('< ' . __('previous')) ?>
            <?= $this->Paginator->numbers() ?>
            <?= $this->Paginator->next(__('next') . ' >') ?>
        </ul>
        <p><?= $this->Paginator->counter() ?></p>
    </div>
</div>
