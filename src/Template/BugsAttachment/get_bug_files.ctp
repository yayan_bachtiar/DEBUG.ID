<div class="col-md-12 animated fadeInRight">
    <section class="panel default blue_title h2">
        <div class="panel-heading">
            <i class="fa fa-paperclip"></i>
            <a href="#"><span class="pull-right badge"><?= $number ?> Files</span></a>
        </div>
        <div class="panel-body">
            <ul class="list-group">
                <?php $i=0;
                foreach ($bug_attach as $key ) {?>
                <li class="list-group-item" id='<?=$key->id;?>'>
                    <div class="pull-right">
                        <a href="<?= $this->request->webroot.'webroot/'. $key->path.'/'.$key->filename; ?>" class="" target="_blank">
                        <i class="fa fa-download"></i>
                        </a>
                        <span>&nbsp; </span>
                        <a href="javascript:hapus(<?=$key->id?>)"  onclick="if (confirm('Are you sure you want to delete # <?= $key->filename;?>')) { return true; } return false;"><i class="fa fa-times"></i></a>
                        <span>&nbsp; </span>
                    </div>
                    <i class="fa fa-file-pdf-o"></i>
                    <?= $key->filename;?>
                </li>
                <?php $i++;
                                                                                if($i==5){
                                                                                    break;
                                                                                    }
                }?>
            </ul>
            <?= $this->Html->css('dropzone.css');?>
            <br>
            <div class="container">
                <div class="row">
                    <div class="col l12 center">
                        <form action="<?= $this->Url->build(['controller'=>'bugsAttachment', 'action'=>'add', $bug_id]);?>" class="dropzone" id="dropzone">
                        </form>
                    </div>
                    <br>
                    <!--            <a href="#" class="btn waves-effect waves-blue"></a>-->
                </div>
            </div>
            <?= $this->Html->script('dropzone.js');?>
        </div>
    </section>
    <script type="text/javascript">
        Dropzone.options.dropzone = {
            init: function () {
                this.on("complete", function (file) {
                if (this.getUploadingFiles().length === 0 && this.getQueuedFiles().length === 0) {
                    get();
                    }
                });
            }
        };
    </script>
    <script type="text/javascript">
    function get(){
        $.ajax({
            url: '<?= $this->Url->build(["controller"=>"bugsAttachment", "action"=>"get_bug_files"]);?>',
            type: 'GET',
            dataType: 'html',
            data: {id: '<?= $bug_id;?>'},
        })
        .done(function(response) {
            $("#file_attach").html(response)
        })
        .fail(function() {
            console.log("error");
        })
        .always(function() {
            console.log("complete");
            $("#dropzone").dropzone();
            get_log();
        });

    }
    </script>
    <script type="text/javascript">
                function hapus(id_attachment) {
                    $.ajax({
    url: "<?= $this->Url->build(['controller'=>'bugsAttachment', 'action'=>'delete']);?>",
    data: {id: id_attachment},
    type: 'GET',
    success:function() {
    console.log("success");
    $("#"+id_attachment).hide();
    toastr.success("Data has ben deleted");
    }
    });
    };
    </script>
</div>
