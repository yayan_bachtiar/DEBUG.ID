<?= $this->Html->css('dropzone.css');?>
<br>
<div class="container">
    <div class="row">
        <div class="col l12 center">
            <?php echo $this->Form->create(null, ['class'=>'dropzone']);?>
            <?php echo $this->Form->end();?>
        </div>
        <br>
        <!--            <a href="#" class="btn waves-effect waves-blue"></a>-->
    </div>
    <?=$this->Html->link('Back', ['controller'=>'bugs', 'action'=>'view', $id], ['class'=>'btn btn-primary']);?>
</div>
<?= $this->Html->script('dropzone.js');?>
