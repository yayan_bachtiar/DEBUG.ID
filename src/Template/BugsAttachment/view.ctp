<div class="actions columns large-2 medium-3">
    <h3><?= __('Actions') ?></h3>
    <ul class="side-nav">
        <li><?= $this->Html->link(__('Edit Bugs Attachment'), ['action' => 'edit', $bugsAttachment->id]) ?> </li>
        <li><?= $this->Form->postLink(__('Delete Bugs Attachment'), ['action' => 'delete', $bugsAttachment->id], ['confirm' => __('Are you sure you want to delete # {0}?', $bugsAttachment->id)]) ?> </li>
        <li><?= $this->Html->link(__('List Bugs Attachment'), ['action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Bugs Attachment'), ['action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Bugs'), ['controller' => 'Bugs', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Bug'), ['controller' => 'Bugs', 'action' => 'add']) ?> </li>
    </ul>
</div>
<div class="bugsAttachment view large-10 medium-9 columns">
    <h2><?= h($bugsAttachment->id) ?></h2>
    <div class="row">
        <div class="large-5 columns strings">
            <h6 class="subheader"><?= __('Bug') ?></h6>
            <p><?= $bugsAttachment->has('bug') ? $this->Html->link($bugsAttachment->bug->id, ['controller' => 'Bugs', 'action' => 'view', $bugsAttachment->bug->id]) : '' ?></p>
            <h6 class="subheader"><?= __('Path') ?></h6>
            <p><?= h($bugsAttachment->path) ?></p>
            <h6 class="subheader"><?= __('Ext') ?></h6>
            <p><?= h($bugsAttachment->ext) ?></p>
            <h6 class="subheader"><?= __('Filename') ?></h6>
            <p><?= h($bugsAttachment->filename) ?></p>
        </div>
        <div class="large-2 columns numbers end">
            <h6 class="subheader"><?= __('Id') ?></h6>
            <p><?= $this->Number->format($bugsAttachment->id) ?></p>
            <h6 class="subheader"><?= __('Upload By') ?></h6>
            <p><?= $this->Number->format($bugsAttachment->upload_by) ?></p>
        </div>
        <div class="large-2 columns dates end">
            <h6 class="subheader"><?= __('Upload Date') ?></h6>
            <p><?= h($bugsAttachment->upload_date) ?></p>
        </div>
    </div>
</div>
