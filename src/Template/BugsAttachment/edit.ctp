<div class="actions columns large-2 medium-3">
    <h3><?= __('Actions') ?></h3>
    <ul class="side-nav">
        <li><?= $this->Form->postLink(
                __('Delete'),
                ['action' => 'delete', $bugsAttachment->id],
                ['confirm' => __('Are you sure you want to delete # {0}?', $bugsAttachment->id)]
            )
        ?></li>
        <li><?= $this->Html->link(__('List Bugs Attachment'), ['action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('List Bugs'), ['controller' => 'Bugs', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Bug'), ['controller' => 'Bugs', 'action' => 'add']) ?></li>
    </ul>
</div>
<div class="bugsAttachment form large-10 medium-9 columns">
    <?= $this->Form->create($bugsAttachment) ?>
    <fieldset>
        <legend><?= __('Edit Bugs Attachment') ?></legend>
        <?php
            echo $this->Form->input('bugs_id', ['options' => $bugs]);
            echo $this->Form->input('path');
            echo $this->Form->input('ext');
            echo $this->Form->input('filename');
            echo $this->Form->input('upload_date');
            echo $this->Form->input('upload_by');
        ?>
    </fieldset>
    <?= $this->Form->button(__('Submit')) ?>
    <?= $this->Form->end() ?>
</div>
