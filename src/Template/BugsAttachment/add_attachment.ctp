<?= $this->Html->css('dropzone.css');?>
<br>
<div class="container">
    <div class="row">
        <div class="col l12 center">
            <?php echo $this->Form->create(null, ['class'=>'dropzone']);?>
            <?php echo $this->Form->end();?>
        </div>
        <br>
        <!--            <a href="#" class="btn waves-effect waves-blue"></a>-->
    </div>
    <a href="<?= $_SERVER['HTTP_REFERER']?>" class="btn btn-primary">Back</a>
</div>
<?= $this->Html->script('dropzone.js');?>
