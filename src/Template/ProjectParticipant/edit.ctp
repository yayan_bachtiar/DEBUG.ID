<div class="actions columns large-2 medium-3">
    <h3><?= __('Actions') ?></h3>
    <ul class="side-nav">
        <li><?= $this->Form->postLink(
                __('Delete'),
                ['action' => 'delete', $projectParticipant->id],
                ['confirm' => __('Are you sure you want to delete # {0}?', $projectParticipant->id)]
            )
        ?></li>
        <li><?= $this->Html->link(__('List Project Participant'), ['action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('List Project'), ['controller' => 'Project', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Project'), ['controller' => 'Project', 'action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Users'), ['controller' => 'Users', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New User'), ['controller' => 'Users', 'action' => 'add']) ?> </li>
    </ul>
</div>
<div class="projectParticipant form large-10 medium-9 columns">
    <?= $this->Form->create($projectParticipant); ?>
    <fieldset>
        <legend><?= __('Edit Project Participant') ?></legend>
        <?php
            echo $this->Form->input('project_id', ['options' => $project]);
            echo $this->Form->input('user_id', ['options' => $users]);
        ?>
    </fieldset>
    <?= $this->Form->button(__('Submit')) ?>
    <?= $this->Form->end() ?>
</div>
