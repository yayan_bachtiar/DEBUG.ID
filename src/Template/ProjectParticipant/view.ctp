<div class="actions columns large-2 medium-3">
    <h3><?= __('Actions') ?></h3>
    <ul class="side-nav">
        <li><?= $this->Html->link(__('Edit Project Participant'), ['action' => 'edit', $projectParticipant->id]) ?> </li>
        <li><?= $this->Form->postLink(__('Delete Project Participant'), ['action' => 'delete', $projectParticipant->id], ['confirm' => __('Are you sure you want to delete # {0}?', $projectParticipant->id)]) ?> </li>
        <li><?= $this->Html->link(__('List Project Participant'), ['action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Project Participant'), ['action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Project'), ['controller' => 'Project', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Project'), ['controller' => 'Project', 'action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Users'), ['controller' => 'Users', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New User'), ['controller' => 'Users', 'action' => 'add']) ?> </li>
    </ul>
</div>
<div class="projectParticipant view large-10 medium-9 columns">
    <h2><?= h($projectParticipant->id) ?></h2>
    <div class="row">
        <div class="large-5 columns strings">
            <h6 class="subheader"><?= __('Project') ?></h6>
            <p><?= $projectParticipant->has('project') ? $this->Html->link($projectParticipant->project->id, ['controller' => 'Project', 'action' => 'view', $projectParticipant->project->id]) : '' ?></p>
            <h6 class="subheader"><?= __('User') ?></h6>
            <p><?= $projectParticipant->has('user') ? $this->Html->link($projectParticipant->user->id, ['controller' => 'Users', 'action' => 'view', $projectParticipant->user->id]) : '' ?></p>
        </div>
        <div class="large-2 columns numbers end">
            <h6 class="subheader"><?= __('Id') ?></h6>
            <p><?= $this->Number->format($projectParticipant->id) ?></p>
        </div>
    </div>
</div>
