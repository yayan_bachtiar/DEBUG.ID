<section class="panel default blue_title h2 animated fadeInRight">
    <div class="panel-heading">
        Member Project
        <a href="#"><span class="pull-right badge"><?= $number ?> Member</span></a>
    </div>
    <div class="panel-body">
        <ul class="list-group">
            <?php $i=0;
            foreach ($projectParticipant as $key ) {?>
            <li class="list-group-item" id='<?=$key->user->id;?>'>
            <div class="pull-right">
                <img src="<?=$this->Encryptor->get_avatar($key->user->email);?>" alt="none" style="width:20px;height:20px; border-radius:50%;">
                <span>&nbsp; </span>
                <a href="javascript:remove_user(<?=$key->user->id?>)"  onclick="if (confirm('Are you sure you want to remove # <?= $key->user->username;?>')) { return true; } return false;"><i class="fa fa-times"></i></a>
                <span>&nbsp; </span>
            </div>
                <i class="fa fa-user"></i>
                <?= $key->user->username;?>
            </li>
            <?php $i++;
                if($i==5){
                    break;
                    }
            }?>
        </ul>
        <p class="text-center">
        <div class="col-sm-9">
            <form action="" class="user">
                <input type="text"  id="source2" class="input-xlarge" name="user_id" type="hidden" data-placeholder="Choose An Option..">
                <button type="submit" class="btn btn-primary">Masukan</button>
            </form>
        </div>
        </p>
    </div>
</section>
<script>
      $(function () {
        $('form.user').on('submit', function (e) {

          e.preventDefault();

          $.ajax({
            type: 'post',
            url: "<?= $this->Url->build(['controller'=>'projectParticipant', 'action'=>'get_participant?project_id='. $project_id]); ?>",
            data: $('form.user').serialize(),
            success: function (response) {
              toastr.success("has been saved");
              $("#user").html(response);
            }
          });

        });

      });
</script>
<script type="text/javascript">
$(document).ready(function(){
   $('#source2').select2({
    minimumInputLength: 2,
    ajax: {
      url: "<?= $this->Url->build(['controller'=>'user', 'action'=>'select2user']);?>",
      dataType: 'json',
      data: function (term, page) {
        return {
          q: term
        };
      },
      results: function (data, page) {
        return { results: data };
      }
    }
  });
});
</script>
<script type="text/javascript">
    function remove_user(id_user) {
        $.ajax({
            url: "<?= $this->Url->build(['controller'=>'projectParticipant', 'action'=>'delete', $project_id]);?>",
            data: {id: id_user},
            type: 'GET',
            // dataType: 'default: Intelligent Guess (Other values: xml, json, script, or html)',
            success:function() {
                console.log("success");
                $("#"+id_user).hide();
                toastr.success("Data has ben deleted");
            }
        });
    };
</script>
