<div class="actions columns large-2 medium-3">
    <h3><?= __('Actions') ?></h3>
    <ul class="side-nav">
        <li><?= $this->Html->link(__('New Project Participant'), ['action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Project'), ['controller' => 'Project', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Project'), ['controller' => 'Project', 'action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Users'), ['controller' => 'Users', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New User'), ['controller' => 'Users', 'action' => 'add']) ?> </li>
    </ul>
</div>
<div class="projectParticipant index large-10 medium-9 columns">
    <table cellpadding="0" cellspacing="0">
    <thead>
        <tr>
            <th><?= $this->Paginator->sort('id') ?></th>
            <th><?= $this->Paginator->sort('project_id') ?></th>
            <th><?= $this->Paginator->sort('user_id') ?></th>
            <th class="actions"><?= __('Actions') ?></th>
        </tr>
    </thead>
    <tbody>
    <?php foreach ($projectParticipant as $projectParticipant): ?>
        <tr>
            <td><?= $this->Number->format($projectParticipant->id) ?></td>
            <td>
                <?= $projectParticipant->has('project') ? $this->Html->link($projectParticipant->project->id, ['controller' => 'Project', 'action' => 'view', $projectParticipant->project->id]) : '' ?>
            </td>
            <td>
                <?= $projectParticipant->has('user') ? $this->Html->link($projectParticipant->user->id, ['controller' => 'Users', 'action' => 'view', $projectParticipant->user->id]) : '' ?>
            </td>
            <td class="actions">
                <?= $this->Html->link(__('View'), ['action' => 'view', $projectParticipant->id]) ?>
                <?= $this->Html->link(__('Edit'), ['action' => 'edit', $projectParticipant->id]) ?>
                <?= $this->Form->postLink(__('Delete'), ['action' => 'delete', $projectParticipant->id], ['confirm' => __('Are you sure you want to delete # {0}?', $projectParticipant->id)]) ?>
            </td>
        </tr>

    <?php endforeach; ?>
    </tbody>
    </table>
    <div class="paginator">
        <ul class="pagination">
            <?= $this->Paginator->prev('< ' . __('previous')) ?>
            <?= $this->Paginator->numbers() ?>
            <?= $this->Paginator->next(__('next') . ' >') ?>
        </ul>
        <p><?= $this->Paginator->counter() ?></p>
    </div>
</div>
