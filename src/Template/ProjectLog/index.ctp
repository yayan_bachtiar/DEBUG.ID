<div class="actions columns large-2 medium-3">
    <h3><?= __('Actions') ?></h3>
    <ul class="side-nav">
        <li><?= $this->Html->link(__('New Project Log'), ['action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Bugs'), ['controller' => 'Bugs', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Bug'), ['controller' => 'Bugs', 'action' => 'add']) ?></li>
    </ul>
</div>
<div class="projectLog index large-10 medium-9 columns">
    <table cellpadding="0" cellspacing="0">
    <thead>
        <tr>
            <th><?= $this->Paginator->sort('id') ?></th>
            <th><?= $this->Paginator->sort('project_id') ?></th>
            <th><?= $this->Paginator->sort('created_by') ?></th>
            <th><?= $this->Paginator->sort('created') ?></th>
            <th><?= $this->Paginator->sort('log_type') ?></th>
            <th><?= $this->Paginator->sort('module_id') ?></th>
            <th><?= $this->Paginator->sort('bug_id') ?></th>
            <th class="actions"><?= __('Actions') ?></th>
        </tr>
    </thead>
    <tbody>
    <?php foreach ($projectLog as $projectLog): ?>
        <tr>
            <td><?= $this->Number->format($projectLog->id) ?></td>
            <td><?= $this->Number->format($projectLog->project_id) ?></td>
            <td><?= $this->Number->format($projectLog->created_by) ?></td>
            <td><?= h($projectLog->created) ?></td>
            <td><?= h($projectLog->log_type) ?></td>
            <td><?= $this->Number->format($projectLog->module_id) ?></td>
            <td>
                <?= $projectLog->has('bug') ? $this->Html->link($projectLog->bug->id, ['controller' => 'Bugs', 'action' => 'view', $projectLog->bug->id]) : '' ?>
            </td>
            <td class="actions">
                <?= $this->Html->link(__('View'), ['action' => 'view', $projectLog->id]) ?>
                <?= $this->Html->link(__('Edit'), ['action' => 'edit', $projectLog->id]) ?>
                <?= $this->Form->postLink(__('Delete'), ['action' => 'delete', $projectLog->id], ['confirm' => __('Are you sure you want to delete # {0}?', $projectLog->id)]) ?>
            </td>
        </tr>

    <?php endforeach; ?>
    </tbody>
    </table>
    <div class="paginator">
        <ul class="pagination">
            <?= $this->Paginator->prev('< ' . __('previous')) ?>
            <?= $this->Paginator->numbers() ?>
            <?= $this->Paginator->next(__('next') . ' >') ?>
        </ul>
        <p><?= $this->Paginator->counter() ?></p>
    </div>
</div>
