<div class="actions columns large-2 medium-3">
    <h3><?= __('Actions') ?></h3>
    <ul class="side-nav">
        <li><?= $this->Form->postLink(
                __('Delete'),
                ['action' => 'delete', $projectLog->id],
                ['confirm' => __('Are you sure you want to delete # {0}?', $projectLog->id)]
            )
        ?></li>
        <li><?= $this->Html->link(__('List Project Log'), ['action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('List Bugs'), ['controller' => 'Bugs', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Bug'), ['controller' => 'Bugs', 'action' => 'add']) ?></li>
    </ul>
</div>
<div class="projectLog form large-10 medium-9 columns">
    <?= $this->Form->create($projectLog) ?>
    <fieldset>
        <legend><?= __('Edit Project Log') ?></legend>
        <?php
            echo $this->Form->input('project_id');
            echo $this->Form->input('message');
            echo $this->Form->input('created_by');
            echo $this->Form->input('log_type');
            echo $this->Form->input('related_link');
            echo $this->Form->input('module_id');
            echo $this->Form->input('bug_id', ['options' => $bugs, 'empty' => true]);
        ?>
    </fieldset>
    <?= $this->Form->button(__('Submit')) ?>
    <?= $this->Form->end() ?>
</div>
