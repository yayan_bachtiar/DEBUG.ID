<div class="x_content">
    <div class="dashboard-widget-content">
        <ul class="list-unstyled timeline widget">
            <?php if(!empty($log)){
                    foreach ($log as $logs ) {
                        $detail = json_decode($logs->related_link);
            ?>
            <li>
                <div class="block">
                    <div class="block_content">
                        <h2 class="title">
                        <a><?= $logs->message;?></a>
                        </h2>
                        <div class="byline">
                            <span data-livestamp="<?php echo strtotime($logs->created); ?>"></span> by <?=  $this->Html->link($logs->user->username, ['controller'=>'user', 'action'=>'view', $logs->user->id],['class'=>'', 'escape'=>false]);?>
                        </div>
                        <p class="excerpt"></p>
                    </div>
                </div>
            </li>
            <?php
                        }
            }?>
        </ul>
        <div class="pull-right">
            <ul class="pagination pagination" id="log">
                <?= $this->Paginator->prev('«', $options = ['model'=>'bpass']) ?>
                <?= $this->Paginator->numbers() ?>
                <?= $this->Paginator->next('»', $options = ['model'=>'bpass']) ?>
            </ul>
        </div>
    </div>
</div>
<script type="text/javascript">
$("ul#log.pagination li a").bind("click", function (event) {
    var href = $(this).attr('href');
    if(href!=null){
        $.ajax({
        success:function (response) {
            $("#l_content").html(response);
        },
        url: href,
        error:(function(response){
            // location.reload();
            }),
        });
    }
    return false;
    });
</script>