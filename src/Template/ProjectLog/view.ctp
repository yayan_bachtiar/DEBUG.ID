<div class="actions columns large-2 medium-3">
    <h3><?= __('Actions') ?></h3>
    <ul class="side-nav">
        <li><?= $this->Html->link(__('Edit Project Log'), ['action' => 'edit', $projectLog->id]) ?> </li>
        <li><?= $this->Form->postLink(__('Delete Project Log'), ['action' => 'delete', $projectLog->id], ['confirm' => __('Are you sure you want to delete # {0}?', $projectLog->id)]) ?> </li>
        <li><?= $this->Html->link(__('List Project Log'), ['action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Project Log'), ['action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Bugs'), ['controller' => 'Bugs', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Bug'), ['controller' => 'Bugs', 'action' => 'add']) ?> </li>
    </ul>
</div>
<div class="projectLog view large-10 medium-9 columns">
    <h2><?= h($projectLog->id) ?></h2>
    <div class="row">
        <div class="large-5 columns strings">
            <h6 class="subheader"><?= __('Log Type') ?></h6>
            <p><?= h($projectLog->log_type) ?></p>
            <h6 class="subheader"><?= __('Bug') ?></h6>
            <p><?= $projectLog->has('bug') ? $this->Html->link($projectLog->bug->id, ['controller' => 'Bugs', 'action' => 'view', $projectLog->bug->id]) : '' ?></p>
        </div>
        <div class="large-2 columns numbers end">
            <h6 class="subheader"><?= __('Id') ?></h6>
            <p><?= $this->Number->format($projectLog->id) ?></p>
            <h6 class="subheader"><?= __('Project Id') ?></h6>
            <p><?= $this->Number->format($projectLog->project_id) ?></p>
            <h6 class="subheader"><?= __('Created By') ?></h6>
            <p><?= $this->Number->format($projectLog->created_by) ?></p>
            <h6 class="subheader"><?= __('Module Id') ?></h6>
            <p><?= $this->Number->format($projectLog->module_id) ?></p>
        </div>
        <div class="large-2 columns dates end">
            <h6 class="subheader"><?= __('Created') ?></h6>
            <p><?= h($projectLog->created) ?></p>
        </div>
    </div>
    <div class="row texts">
        <div class="columns large-9">
            <h6 class="subheader"><?= __('Message') ?></h6>
            <?= $this->Text->autoParagraph(h($projectLog->message)) ?>
        </div>
    </div>
    <div class="row texts">
        <div class="columns large-9">
            <h6 class="subheader"><?= __('Related Link') ?></h6>
            <?= $this->Text->autoParagraph(h($projectLog->related_link)) ?>
        </div>
    </div>
</div>
