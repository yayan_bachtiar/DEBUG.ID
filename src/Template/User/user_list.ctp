<div class="row">
    <div class="col-md-12">
        <div class="x_panel">
            <div class="x_content">
                <div class="row">
                    <div class="col-md-12 col-sm-12 col-xs-12" style="text-align:center;">
                        <ul class="pagination pagination-split">
                            <li><a href="#">A</a>
                            </li>
                            <li><a href="#">B</a>
                            </li>
                            <li><a href="#">C</a>
                            </li>
                            <li><a href="#">D</a>
                            </li>
                            <li><a href="#">E</a>
                            </li>
                            <li>
                                <a href="#">E</a>
                            </li>
                            <li>
                                <a href="#">E</a>
                            </li>
                            <li>
                                <a href="#">E</a>
                            </li>
                            <li>
                                <a href="#">E</a>
                            </li>
                            <li>
                                <a href="#">E</a>
                            </li>
                            <li>
                                <a href="#">E</a>
                            </li>
                            <li>
                                <a href="#">E</a>
                            </li>
                            <li>
                                <a href="#">E</a>
                            </li>
                            <li>
                                <a href="#">E</a>
                            </li>
                            <li>
                                <a href="#">E</a>
                            </li>
                            <li>
                                <a href="#">E</a>
                            </li>
                            <li>
                                <a href="#">E</a>
                            </li>
                            <li>
                                <a href="#">E</a>
                            </li>
                            <li>
                                <a href="#">E</a>
                            </li>
                            <li>
                                <a href="#">E</a>
                            </li>
                            <li>
                                <a href="#">E</a>
                            </li>
                            <li>
                                <a href="#">E</a>
                            </li>
                            <li><a href="#">W</a>
                            </li>
                            <li><a href="#">X</a>
                            </li>
                            <li><a href="#">Y</a>
                            </li>
                            <li><a href="#">Z</a>
                            </li>
                        </ul>
                    </div>
                    <div class="clearfix"></div>
                    <?php foreach ($user as $member) {?>
                    <div class="col-md-4 col-sm-4 col-xs-12 animated fadeInDown">
                        <div class="well profile_view">
                            <div class="col-sm-12">
                                <h4 class="brief"><i><?=$member->role->role_desc?></i></h4>
                                <div class="left col-xs-7">
                                    <h2><?= $member->first_name;?></h2>
                                    <p><strong>About: </strong> <?=$member->role->role_desc?></p>
                                    <ul class="list-unstyled">
                                        <li><i class="fa fa-bug"></i> resolved bugs: 100</li>
                                        <li><i class="fa fa-list"></i> Finished Tasks: 123 </li>
                                    </ul>
                                </div>
                                <div class="right col-xs-5 text-center">
                                    <img src="<?php echo $this->General->get_avatar($member->email); ?>" alt="" class="img-circle img-responsive">
                                </div>
                            </div>
                            <div class="col-xs-12 bottom text-center">
                                <div class="col-xs-12 col-sm-8 emphasis">
                                    <p class="ratings">
                                    <a href="mailto:<?=$member->email?>" class="btn btn-xs btn-default"><i class="fa fa-envelope-o"></i></a>
                                    <a href="#" class="btn btn-xs btn-info"><i class="fa fa-twitter"></i></a>
                                    <a href="#" class="btn btn-xs btn-primary"><i class="fa fa-linkedin"></i></a>
                                    <a href="#" class="btn btn-xs btn-primary"><i class="fa fa-facebook"></i></a>
                                    <a href="#" class="btn btn-xs btn-danger"><i class="fa fa-google-plus"></i></a>
                                    </p>
                                </div>
                                <div class="col-xs-12 col-sm-4 emphasis">
                                    <a href="<?= $this->Url->build(['controller'=>'user', 'action'=>'view', $member->id])?>" type="button" class="btn btn-primary btn-xs"> <i class="fa fa-user">
                                    </i> View Profile </a>
                                </div>
                            </div>
                        </div>
                    </div>
                    <?php }?>
                </div>
            </div>
        </div>
    </div>
</div>