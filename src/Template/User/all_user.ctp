<div class="container">
    <div class="row">
        <div class="col m12 l12">
            <span onload="toast"></span>
            <script>
                function toast(){
                    Materialize.toast('sdadasdsadsad asd ass', 4000)
                }
            </script>

            <table>
                <thead>
                    <tr>
                        <th>No</th>
                        <th><?= $this->Paginator->sort('username') ?></th>
                        <th><?= $this->Paginator->sort('register_date') ?></th>
                        <th class="actions"><?= __('Actions') ?></th>
                    </tr>
                </thead>
                <tbody>
                    <?php $i=1; foreach ($users as $user): ?>
                    <tr>
                        <td><?= $i++; ?></td>
                        <td><?= h($user->username) ?></td>
                        <td><?= h($user->register_date) ?></td>
                        <td class="actions">
                            <?= $this->Html->link(__('View'), ['action' => 'view', $user->id]) ?>
                            <?= $this->Html->link(__('Edit'), ['action' => 'edit', $user->id]) ?>
                            <?= $this->Form->postLink(__('Delete'), ['action' => 'delete', $user->id], ['confirm' => __('Are you sure you want to delete # {0}?', $user->id)]) ?>
                        </td>
                    </tr>

                    <?php endforeach; ?>
                </tbody>
            </table>
            <ul class="pagination">
                <?= $this->Paginator->prev('<i class="mdi-navigation-chevron-left"></i> ', ['escape'=>false]) ?>
                <?= $this->Paginator->numbers() ?>
                <?= $this->Paginator->next('<i class="mdi-navigation-chevron-right"></i>', ['escape'=>false]) ?>
            </ul>
            <p><?= $this->Paginator->counter() ?></p>
            <br>
        </div>
    </div>
</div>
<div class="fixed-action-btn" style="bottom: 45px; right: 24px;">
    <?= $this->Html->link('<i class="large mdi-content-add"></i>', ['controller'=>'add_user'], ['class'=>'btn-floating btn-large waves-effect waves-teal red', 'escape'=>false]);?>
</div>
