<div class="profile_img">
    <!-- end of image cropping -->
    <div id="crop-avatar">
        <!-- Current avatar -->
        <div class="">
            <img src="<?= $this->General->get_avatar($user->email);?>" style="width: 100%;border-radius: 50%;box-shadow: 6px 8px 9px rgb(201, 201, 201);"/>
        </div>
        <div class="loading" aria-label="Loading" role="img" tabindex="-1">
        </div>
    </div>
    <!-- end of image cropping -->
</div>
<h3 class="text-center"><?= ucfirst($user->firstname)?></h3>
<ul class="list-unstyled user_data">
    <li><i class="fa fa-map-marker user-profile-icon"></i> San Francisco, California, USA
    </li>
    <li>
        <i class="fa fa-briefcase user-profile-icon"></i> <?= $user->role->role_desc;?>
    </li>
    <li class="m-top-xs">
        <i class="fa fa-external-link user-profile-icon"></i>
        <a href="#" target="_blank">www.mysite.com</a>
    </li>
</ul>
<a class="btn btn-success" href="<?= $this->Url->build(['action'=>'edit', $user->id]);?>"><i class="fa fa-edit m-right-xs"></i>Edit Profile</a>
<br />
<!-- start skills -->
<h4>Skills</h4>
<ul class="list-unstyled user_data">
    <li>
        <p>Web Applications</p>
        <div class="progress progress_sm">
            <div class="progress-bar bg-green" role="progressbar" data-transitiongoal="50"></div>
        </div>
    </li>
    <li>
        <p>Website Design</p>
        <div class="progress progress_sm">
            <div class="progress-bar bg-green" role="progressbar" data-transitiongoal="70"></div>
        </div>
    </li>
    <li>
        <p>Automation & Testing</p>
        <div class="progress progress_sm">
            <div class="progress-bar bg-green" role="progressbar" data-transitiongoal="30"></div>
        </div>
    </li>
    <li>
        <p>UI / UX</p>
        <div class="progress progress_sm">
            <div class="progress-bar bg-green" role="progressbar" data-transitiongoal="50"></div>
        </div>
    </li>
</ul>
<!-- end of skills -->