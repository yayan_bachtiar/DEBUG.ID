<?= $this->Html->css("select/select2.min.css");?>
<?= $this->Html->css("summernote/summernote.css");?>
<div class="clearfix"></div>
<div class="row">
    <div class="col-md-12 col-sm-12 col-xs-12">
        <div class="x_panel">
            <div class="x_title">
                <h2>Users<small>add newU user</small></h2>
                <ul class="nav navbar-right panel_toolbox">
                    <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                    </li>
                    <li><a class="close-link"><i class="fa fa-close"></i></a>
                    </li>
                </ul>
                <div class="clearfix"></div>
            </div>
            <div class="x_content">
                <form class="form-horizontal form-label-left" novalidate method="POST" action="<?= $this->Url->build(['action'=>'add'])?>">
                    <span class="section">Project Detail</span>
                    <div class="item form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="name">Project Name <span class="required">*</span>
                        </label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                            <input id="name" class="form-control col-md-7 col-xs-12" data-validate-length-range="6" name="project_name" required="required" type="text">
                        </div>
                    </div>
                    <div class="item form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="textarea">Project Description <span class="required">*</span>
                        </label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                            <textarea id="summernote" required="required" name="project_desc" class="form-control col-md-7 col-xs-12" style="min-height:200px" ></textarea >
                        </div>
                    </div>
                    <div class="item form-group">
                        <div class="controls">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="textarea">Project Description <span class="required">*</span>
                            </label>
                            <div class="col-md-6 col-sm-6 col-xs-12">
                                <div class="input-prepend input-group">
                                    <span class="add-on input-group-addon"><i class="glyphicon glyphicon-calendar fa fa-calendar"></i></span>
                                    <input type="text" style="width: 200px" name="reservation" id="reservation" class="form-control" value="" required="required"/>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="item form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="number">Select Project Manager <span class="required">*</span>
                        </label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                            <select id="heard" class="select2_single form-control" required="required" name="project_owner">
                                <option value="" >Choose..</option>
                            <?php 
                            $selected = '';
                            foreach ($user as $key) {
                              if($key->id==$project->project_owner){
                                $selected = 'selected';
                              }
                                echo "<option value='$key->id' $selected>$key->username</option>";
                              }
                              
                            ?>
                            </select>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-md-6 col-md-offset-3">
                            <button type="submit" class="btn btn-primary">Cancel</button>
                            <button id="send" type="submit" class="btn btn-success">Submit</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<?=$this->element('collapse-box')?>
<?= $this->Html->script("validator/validator.js")?>
<?= $this->Html->script("moment.min2.js")?>
<?= $this->Html->script("datepicker/daterangepicker.js")?>
<?= $this->Html->script("select/select2.full.js")?>
<?= $this->Html->script("summernote/summernote.min.js")?>
<script>
    $(document).ready(function () {
        $(".select2_single").select2({
            placeholder: "Select a state",
            allowClear: true
        });
        $("#summernote").summernote({
            height: 150,
            focus:true
        });
    });
</script>
<script type="text/javascript">
$(document).ready(function () {
    $('#reservation').daterangepicker({
        format: 'YYYY-MM-DD',
        }, function (start, end, label) {
            console.log(start.toISOString(), end.toISOString(), label);
        });
});
</script>
<script>
        // initialize the validator function
        validator.message['date'] = 'not a real date';
        // validate a field on "blur" event, a 'select' on 'change' event & a '.reuired' classed multifield on 'keyup':
        $('form')
            .on('blur', 'input[required], input.optional, select.required', validator.checkField)
            .on('change', 'select.required', validator.checkField)
            .on('keypress', 'input[required][pattern]', validator.keypress);
        $('.multi.required')
            .on('keyup blur', 'input', function () {
                validator.checkField.apply($(this).siblings().last()[0]);
            });
        // bind the validation to the form submit event
        //$('#send').click('submit');//.prop('disabled', true);
        $('form').submit(function (e) {
            e.preventDefault();
            var submit = true;
            // evaluate the form using generic validaing
            if (!validator.checkAll($(this))) {
                submit = false;
            }
            if (submit)
                this.submit();
            return false;
        });
        /* FOR DEMO ONLY */
        $('#vfields').change(function () {
            $('form').toggleClass('mode2');
        }).prop('checked', false);
        $('#alerts').change(function () {
            validator.defaults.alerts = (this.checked) ? false : true;
            if (this.checked)
                $('form .alert').remove();
        }).prop('checked', false);
</script>

<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('List Users'), ['action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('List Role'), ['controller' => 'Role', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Role'), ['controller' => 'Role', 'action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Bugs Participant'), ['controller' => 'BugsParticipant', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Bugs Participant'), ['controller' => 'BugsParticipant', 'action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Module Participant'), ['controller' => 'ModuleParticipant', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Module Participant'), ['controller' => 'ModuleParticipant', 'action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Project Participant'), ['controller' => 'ProjectParticipant', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Project Participant'), ['controller' => 'ProjectParticipant', 'action' => 'add']) ?></li>
    </ul>
</nav>
<div class="users form large-9 medium-8 columns content">
    <?= $this->Form->create($user) ?>
    <fieldset>
        <legend><?= __('Add User') ?></legend>
        <?php
            echo $this->Form->input('username');
            echo $this->Form->input('email');
            echo $this->Form->input('password');
            echo $this->Form->input('first_name');
            echo $this->Form->input('last_name');
            echo $this->Form->input('token');
            echo $this->Form->input('token_expires');
            echo $this->Form->input('api_token');
            echo $this->Form->input('activation_date');
            echo $this->Form->input('tos_date');
            echo $this->Form->input('active');
            echo $this->Form->input('is_superuser');
            echo $this->Form->input('role', ['options' => $role, 'empty' => true]);
        ?>
    </fieldset>
    <?= $this->Form->button(__('Submit')) ?>
    <?= $this->Form->end() ?>
</div>
