
<div id="login" class="animate form">
    <section class="login_content">
        <?php echo $this->Form->create('User', ['class'=>'form-horizontal', 'role'=>'form']);?>
        <h1>Login Form</h1>
        <div>
            <input type="text" class="form-control" placeholder="Username" required="" name="username" />
        </div>
        <div>
            <input type="password" class="form-control" placeholder="Password" required="" name="password" />
        </div>
        <div>
            <button class="btn btn-default submit" type="submit">Log in</button>
            <a class="reset_pass" onclick="alert('on doing, please contact your admin')">Lost your password?</a>
        </div>
        <div class="clearfix"></div>
        <div class="separator">
            <p class="change_link">New to site?
            <a href="#toregister" class="to_register" > Create Account </a>
            </p>
            <div class="clearfix"></div>
            <br />
            <div>
                <h1><i class="fa fa-paw" style="font-size: 26px;"></i> Gentelella Alela!</h1>
                <p>©2015 All Rights Reserved. Gentelella Alela! is a Bootstrap 3 template. Privacy and Terms</p>
            </div>
        </div>
        <?= $this->Form->end();?>
    </section>
    <!-- content -->
</div>
<div id="register" class="animate form">
    <section class="login_content">
        <form action="<?= $this->Url->build(['controller'=>'user', 'action'=>'add']);?>" method="POST">
            <h1>Create Account</h1>
            <div class="form-group">
                <input name="username" parsley-trigger="change" required="" placeholder="Enter user name" class="form-control" type="text" >
            </div>
            <div class="form-group">
                <input name="email" parsley-trigger="change" required="" placeholder="Enter email" class="form-control" type="email" >
            </div>
            <div class="form-group">
                <input id="pass1" placeholder="Password" required="" class="form-control" type="password">
            </div>
            <div class="form-group">
                <input data-parsley-equalto="#pass1" required="" placeholder="Password" class="form-control" type="password" name="password">
            </div>
            <div class="form-group">
                <input name="firstname" parsley-trigger="change" required="" placeholder="Enter first name" class="form-control" type="text" >
            </div>
            <div class="form-group">
                <input name="lastname" parsley-trigger="change" required="" placeholder="Enter last name" class="form-control" type="text" >
            </div>
            <div class="form-group">
                <input type="text" class="form-control form-control-inline input-medium default-date-picker" name="birthdate" placeholder="birthdate" parsley-trigger="change" required="required">
            </div>
            <div class="form-group">
                <select name="role" class="form-control">
                    <option value="" disabled selected >Choose Role</option>
                    <?php foreach($role as $role):?>
                    <?= "<option value='$role->id'>$role->role_name - $role->role_desc </option>"?>
                    <?php endforeach; ?>
                </select>
            </div>
            <button class="btn btn-primary" type="submit">Submit</button>
            <div class="separator">
                <p class="change_link">Already a member ?
                <a href="#tologin" class="to_register" > Log in </a>
                </p>
                <div class="clearfix"></div>
                <br />
                <div>
                    <h1><i class="fa fa-paw" style="font-size: 26px;"></i> Gentelella Alela!</h1>
                    <p>©2015 All Rights Reserved. Gentelella Alela! is a Bootstrap 3 template. Privacy and Terms</p>
                </div>
            </div>
        </form>
        <!-- form -->
    </section>
    <!-- content -->
</div>