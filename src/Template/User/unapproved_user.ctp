<div class="" id="p_content">
    <div class="page-title">
        <div class="title_left">
            <h3>Unapproved Users <small>XS Technologies Contributor</small></h3>
        </div>
        <div class="title_right">
            <div class="col-md-5 col-sm-5 col-xs-12 form-group pull-right top_search">
                <div class="input-group">
                    <input type="text" class="form-control" id="jetssearch" placeholder="Search for...">
                    <span class="input-group-btn">
                    <button class="btn btn-default" type="button">Go!</button>
                    </span>
                </div>
            </div>
        </div>
    </div>
    <div class="clearfix"></div>
    <div class="row" >
        <div class="col-md-12">
            <div class="x_panel">
                <div class="x_title">
                    <h2>Unapproved Users</h2>
                    <ul class="nav navbar-right panel_toolbox">
                        <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                        </li>
                        <li class="dropdown">
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i class="fa fa-wrench"></i></a>
                            <ul class="dropdown-menu" role="menu">
                                <li><a href="#">Settings 1</a>
                                </li>
                                <li><a href="#">Settings 2</a>
                                </li>
                            </ul>
                        </li>
                        <li><a class="close-link"><i class="fa fa-close"></i></a>
                        </li>
                    </ul>
                    <div class="clearfix"></div>
                </div>
                <div class="x_content">
                    <a href="<?= $this->Url->build(['action'=>'add'])?>" class="btn-xs btn-primary btn"><i class="fa fa-plus"></i>Add User</a>
                    <!-- start project list -->
                    <table  class="table responsive-utilities ">
                        <thead>
                            <tr>
                                <th style="width: 1%">#</th>
                                <th style="width: 20%">User Name</th>
                                <th>First Name</th>
                                <th>Last Name</th>
                                <th>Role Description</th>
                                <th style="width: 20%">#Edit</th>
                            </tr>
                        </thead>
                        <tbody id="jetssearch_source">
                            <?php foreach ($user as $list) {?>
                            <tr>
                                <td>#</td>
                                <td>
                                    <?= $this->Html->link($list->username, ['action'=>'view', $list->id])?>
                                    <br />
                                    <small> registration date <?= $list->created?></small>
                                </td>
                                <td>
                                    <?php echo $list->first_name ?>
                                </td>
                                <td>
                                    <?php echo $list->last_name ?>
                                </td>
                                <td>
                                    <?php echo $list->role->role_desc ?>
                                </td>
                                <td>
                                    <?=$this->Html->link('<i class="fa fa-thumbs-o-up"></i> Approve ', ['action'=>'approve', $list->id], ['class'=>'btn btn-success btn-xs activate', 'escape'=>false])?>
                                    <?=$this->Html->link('<i class="fa fa-folder"></i> View ', ['action'=>'view', $list->id], ['class'=>'btn btn-primary btn-xs', 'escape'=>false])?>
                                    <a href="#" class="btn btn-danger btn-xs"><i class="fa fa-trash-o"></i> Delete </a>
                                </td>
                            </tr>
                            <?php } ?>
                        </tbody>
                    </table>
                    <!-- end project list -->
                    <!-- paginator -->
                    <div class="pull-right">
                        <ul class="pagination pagination" id="passed">
                            <?= $this->Paginator->prev('«', $options = ['model'=>'bpass']) ?>
                            <?= $this->Paginator->numbers() ?>
                            <?= $this->Paginator->next('»', $options = ['model'=>'bpass']) ?>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <?= $this->element('collapse-box');?>
    <script type="text/javascript">
        $("ul#passed.pagination li a").bind("click", function (event) {
            var href = $(this).attr('href');
            $.ajax({
            success:function (response) {
                $("#p_content").html(response);
            },
            url: href,
            error:(function(response){
                location.reload();
                }),
            });
            return false;
        });
    </script>
    <script type="text/javascript">
        $("a.activate").bind("click", function (event) {
            var href = $(this).attr('href');
            $.confirm({
                theme: 'supervan',
                title: 'Confirmation!',
                content: 'are you sure to activate this user?!',
                confirm: function(){
                    $.ajax({
                        url: href,
                        type: 'GET',
                    })
                    .done(function(response) {
                        toastr.success("Bugs has been open");
                    })
                    .fail(function(response) {
                        setTimeout(toastr.error("boo, error!!"), 3000);
                        // location.reload(); 
                        console.log(response);
                    })
                    .always(function() {
                    });
                    
                },
                cancel: function(){
                    toastr.success("action canceled");
                }
            });
            return false;
        });
    </script>
    <?= $this->element('progress')?>
</div>