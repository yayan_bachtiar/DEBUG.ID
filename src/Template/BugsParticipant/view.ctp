<div class="actions columns large-2 medium-3">
    <h3><?= __('Actions') ?></h3>
    <ul class="side-nav">
        <li><?= $this->Html->link(__('Edit Bugs Participant'), ['action' => 'edit', $bugsParticipant->id]) ?> </li>
        <li><?= $this->Form->postLink(__('Delete Bugs Participant'), ['action' => 'delete', $bugsParticipant->id], ['confirm' => __('Are you sure you want to delete # {0}?', $bugsParticipant->id)]) ?> </li>
        <li><?= $this->Html->link(__('List Bugs Participant'), ['action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Bugs Participant'), ['action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Bugs'), ['controller' => 'Bugs', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Bug'), ['controller' => 'Bugs', 'action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Users'), ['controller' => 'Users', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New User'), ['controller' => 'Users', 'action' => 'add']) ?> </li>
    </ul>
</div>
<div class="bugsParticipant view large-10 medium-9 columns">
    <h2><?= h($bugsParticipant->id) ?></h2>
    <div class="row">
        <div class="large-5 columns strings">
            <h6 class="subheader"><?= __('Bug') ?></h6>
            <p><?= $bugsParticipant->has('bug') ? $this->Html->link($bugsParticipant->bug->id, ['controller' => 'Bugs', 'action' => 'view', $bugsParticipant->bug->id]) : '' ?></p>
            <h6 class="subheader"><?= __('User') ?></h6>
            <p><?= $bugsParticipant->has('user') ? $this->Html->link($bugsParticipant->user->id, ['controller' => 'Users', 'action' => 'view', $bugsParticipant->user->id]) : '' ?></p>
        </div>
        <div class="large-2 columns numbers end">
            <h6 class="subheader"><?= __('Id') ?></h6>
            <p><?= $this->Number->format($bugsParticipant->id) ?></p>
        </div>
    </div>
</div>
