<div class="actions columns large-2 medium-3">
    <h3><?= __('Actions') ?></h3>
    <ul class="side-nav">
        <li><?= $this->Html->link(__('New Bugs Participant'), ['action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Bugs'), ['controller' => 'Bugs', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Bug'), ['controller' => 'Bugs', 'action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Users'), ['controller' => 'Users', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New User'), ['controller' => 'Users', 'action' => 'add']) ?></li>
    </ul>
</div>
<div class="bugsParticipant index large-10 medium-9 columns">
    <table cellpadding="0" cellspacing="0">
    <thead>
        <tr>
            <th><?= $this->Paginator->sort('id') ?></th>
            <th><?= $this->Paginator->sort('bug_id') ?></th>
            <th><?= $this->Paginator->sort('user_id') ?></th>
            <th class="actions"><?= __('Actions') ?></th>
        </tr>
    </thead>
    <tbody>
    <?php foreach ($bugsParticipant as $bugsParticipant): ?>
        <tr>
            <td><?= $this->Number->format($bugsParticipant->id) ?></td>
            <td>
                <?= $bugsParticipant->has('bug') ? $this->Html->link($bugsParticipant->bug->id, ['controller' => 'Bugs', 'action' => 'view', $bugsParticipant->bug->id]) : '' ?>
            </td>
            <td>
                <?= $bugsParticipant->has('user') ? $this->Html->link($bugsParticipant->user->id, ['controller' => 'Users', 'action' => 'view', $bugsParticipant->user->id]) : '' ?>
            </td>
            <td class="actions">
                <?= $this->Html->link(__('View'), ['action' => 'view', $bugsParticipant->id]) ?>
                <?= $this->Html->link(__('Edit'), ['action' => 'edit', $bugsParticipant->id]) ?>
                <?= $this->Form->postLink(__('Delete'), ['action' => 'delete', $bugsParticipant->id], ['confirm' => __('Are you sure you want to delete # {0}?', $bugsParticipant->id)]) ?>
            </td>
        </tr>

    <?php endforeach; ?>
    </tbody>
    </table>
    <div class="paginator">
        <ul class="pagination">
            <?= $this->Paginator->prev('< ' . __('previous')) ?>
            <?= $this->Paginator->numbers() ?>
            <?= $this->Paginator->next(__('next') . ' >') ?>
        </ul>
        <p><?= $this->Paginator->counter() ?></p>
    </div>
</div>
