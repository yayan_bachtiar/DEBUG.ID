<div class="col-md-12">
<?=$this->Html->link('View Project', ['controller'=>'project', 'action'=>'edit', $project->id])?>    <div class="block-web">
        <div class="header">
            <h3 class="content-header">Graph</h3>
        </div>
        <div class="porlets-content">
            <div id="graph"></div>
        </div>
        <!--/porlets-content-->
        <script type="text/javascript">
            $(function(){
                $('#graph').graphify({
                    //options: true,
                    start: 'combo',
                    obj: {
                        id: 'ggg',
                        width: '100%',
                        height: 375,
                        xGrid: false,
                        legend: true,
                        title: 'Opened Bug & Bugs Resolved',
                        points: [
                            [7, 26, 33, 74, 12, 49, 33, 33, 74, 12, 49, 33],
                            [32, 46, 75, 38, 62, 20, 52, 75, 38, 62, 20, 52]
                        ],
                        pointRadius: 3,
                        colors: ['#428bca', '#1caf9a'],
                        xDist: 100,
                        dataNames: ['Bugs Opened', 'Bugs Fixed'],
                        xName: 'Day',
                        tooltipWidth: 15,
                        animations: true,
                        pointAnimation: true,
                        averagePointRadius: 5,
                        design: {
                            tooltipColor: '#fff',
                            gridColor: '#f3f1f1',
                            tooltipBoxColor: '#d9534f',
                            averageLineColor: '#d9534f',
                            pointColor: '#d9534f',
                            lineStrokeColor: 'grey',
                        }
                    }
                });
                bar.init();
            });
        </script>
    </div>
</div>
<div class="col-md-6">
    <section class="panel panel-default profile_bg">
        <h4 class="font-thin padder">Latest Tweets</h4>
        <ul class="profile_list">
            <li class="profile_list-item">
                <p>Wellcome <a class="text-info" href="#">@Drew Wllon</a> and play this web application template, have fun1 </p>
            <small class="tweets"><i class="fa fa-clock-o"></i> 2 minuts ago</small> </li>
            <li class="profile_list-item">
                <p>Morbi nec <a class="text-info" href="#">@Jonathan George</a> nunc condimentum ipsum dolor sit amet, consectetur</p>
            <small class="tweets"><i class="fa fa-clock-o"></i> 1 hour ago</small> </li>
            <li class="profile_list-item">
                <p><a class="text-info" href="#">@Josh Long</a> Vestibulum ullamcorper sodales nisi nec adipiscing elit. </p>
            <small class="tweets"><i class="fa fa-clock-o"></i> 2 hours ago</small> </li>
        </ul>
    </section>
</div>
<div class="col-md-6">
    <div class="block-web">
        <h3 class="content-header">Note</h3>
        <div class="block widget-notes">
            <div contenteditable="true" class="paper"> Send e-mail to supplier<br>
                <s>Conference at 4 pm.</s><br>
                <s>Order a pizza</s><br>
                <s>Buy flowers</s><br>
                Buy some coffee.<br>
                Dinner at Plaza.<br>
                Take Alex for walk.<br>
                Buy some coffee.<br>
            </div>
        </div>
        <!--/widget-notes-->
    </div>
</div>