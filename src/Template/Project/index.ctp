<div class="" id="p_content">
    <div class="page-title">
        <div class="title_left">
            <h3>Projects <small>XS Technology</small></h3>
        </div>
        <div class="title_right">
            <div class="col-md-5 col-sm-5 col-xs-12 form-group pull-right top_search">
                <div class="input-group">
                    <input type="text" class="form-control" id="jetssearch" placeholder="Search for...">
                    <span class="input-group-btn">
                    <button class="btn btn-default" type="button">Go!</button>
                    </span>
                </div>
            </div>
        </div>
    </div>
    <div class="clearfix"></div>
    <div class="row" >
        <div class="col-md-12">
            <div class="x_panel">
                <div class="x_title">
                    <h2>Projects</h2>
                    <ul class="nav navbar-right panel_toolbox">
                        <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                        </li>
                        <li class="dropdown">
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i class="fa fa-wrench"></i></a>
                            <ul class="dropdown-menu" role="menu">
                                <li><a href="#">Settings 1</a>
                                </li>
                                <li><a href="#">Settings 2</a>
                                </li>
                            </ul>
                        </li>
                        <li><a class="close-link"><i class="fa fa-close"></i></a>
                        </li>
                    </ul>
                    <div class="clearfix"></div>
                </div>
                <div class="x_content">
                <a href="<?= $this->Url->build(['action'=>'add'])?>" class="btn-xs btn-primary btn"><i class="fa fa-plus"></i>Add Project</a>
                    <p>Project handle by PT. Xs Technologies</p>
                    <!-- start project list -->
                    <table  class="table responsive-utilities ">
                        <thead>
                            <tr>
                                <th style="width: 1%">#</th>
                                <th style="width: 20%">Project Name</th>
                                <th>Team Members</th>
                                <th>Project Progress</th>
                                <th>Status</th>
                                <th style="width: 20%">#Edit</th>
                            </tr>
                        </thead>
                        <tbody id="jetssearch_source">
                            <?php foreach ($project as $list) {?>
                            <tr>
                                <td>#</td>
                                <td>
                                    <?= $this->Html->link($list->project_name, ['action'=>'view', $list->id])?>
                                    <br />
                                    <small><?= $list->created?></small>
                                </td>
                                <td>
                                    <ul class="list-inline">
                                        <?php foreach ($list->project_participant as $users) {?>
                                        <li>
                                            <img src="<?= $this->General->get_avatar($users->user->email)?>" class="avatar" alt="Avatar" title="<?= $users->user->username;?>">
                                        </li>
                                        <?php }?>
                                    </ul>
                                </td>
                                <script type="text/javascript">
                                                                $.ajax({
                                url: "<?= $this->Url->build(['controller'=>'Project', 'action'=>'get_statistic'])?>",
                                type: 'GET',
                                data: {id: '<?php echo $list->id;?>'},
                                })
                                .done(function(data) {
                                var json = jQuery.parseJSON(data);
                                $("#stat<?php echo $list->id ?>").attr('style', "width:"+json.percent+"%");
                                $("#num<?php echo $list->id ?>").html(json.percent+"%  Complete ("+json.finished+" finished task of "+json.total+" total Tasks) and "+json.bug_pass+" bug(s) PASSED "+ json.bug_ready+" bug(s) Ready To Test of "+json.all_bug + " Total Bugs");
                                // console.log("response");
                                })
                                .fail(function(response) {
                                console.log(response);
                                })
                                .always(function(response) {
                                console.log(response);
                                });

                                </script>
                                <td class="project_progress">
                                    <div class="progress">
                                        <div class="progress-bar bg-green" id="stat<?php echo $list->id;  ?>" role="progressbar" aria-valuenow="20" aria-valuemin="0" aria-valuemax="100" style="width:50%">
                                        </div>
                                    </div>
                                    <small id="num<?php echo $list->id;?>">50% Complete 0 finished of 0 total Modules</small>
                                </td>
                                <td>
                                    <button type="button" class="btn btn-success btn-xs"><?= $list->status?></button>
                                </td>
                                <td>
                                    <?=$this->Html->link('<i class="fa fa-folder"></i> View ', ['action'=>'view', $list->id], ['class'=>'btn btn-primary btn-xs', 'escape'=>false])?>
                                    <?=$this->Html->link('<i class="fa fa-pencil"></i> Edit ', ['action'=>'edit', $list->id], ['class'=>'btn btn-info btn-xs', 'escape'=>false])?>
                                    <a href="#" class="btn btn-danger btn-xs"><i class="fa fa-trash-o"></i> Delete </a>
                                </td>
                            </tr>
                            <?php } ?>
                        </tbody>
                    </table>
                    <!-- end project list -->
                    <!-- paginator -->
                    <div class="pull-right">
                        <ul class="pagination pagination" id="passed">
                            <?= $this->Paginator->prev('«', $options = ['model'=>'bpass']) ?>
                            <?= $this->Paginator->numbers() ?>
                            <?= $this->Paginator->next('»', $options = ['model'=>'bpass']) ?>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <?= $this->element('collapse-box');?>
    <script type="text/javascript">
        $("ul#passed.pagination li a").bind("click", function (event) {
            var href = $(this).attr('href');
            $.ajax({
            success:function (response) {
                $("#p_content").html(response);
            },
            url: href,
            error:(function(response){
                location.reload();
                }),
            });
            return false;
        });
    </script>
    <?= $this->element('progress')?>
</div>