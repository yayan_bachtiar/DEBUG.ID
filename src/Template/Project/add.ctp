<?= $this->Html->css("select/select2.min.css");?>
<?= $this->Html->css("summernote/summernote.css");?>
<div class="clearfix"></div>
<div class="row">
    <div class="col-md-12 col-sm-12 col-xs-12">
        <div class="x_panel">
            <div class="x_title">
                <h2><?=$title?><small><?php //echo isset($project->project_name)?$project->project_name:'';?></small></h2>
                <ul class="nav navbar-right panel_toolbox">
                    <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                    </li>
                    <li><a class="close-link"><i class="fa fa-close"></i></a>
                    </li>
                </ul>
                <div class="clearfix"></div>
            </div>
            <div class="x_content">
            <?php echo $this->Form->create($project, ["class"=>"form-horizontal form-label-left",  "novalidate", "method"=>"POST"]);?>
                    <span class="section">Project Detail</span>
                    <div class="item form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="project_name">Project Name <span class="required">*</span>
                        </label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                        <?= $this->Form->input('project_name', ["class"=>"form-control col-md-7 col-xs-12", "data-validate-length-range"=>"6", "required"=>"required", "type"=>"text", 'label'=>false]);?>
                        </div>
                    </div>
                    <div class="item form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="textarea">Project Description <span class="required">*</span>
                        </label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                        <?= $this->Form->input('project_desc', ["class"=>"form-control col-md-7 col-xs-12", "required"=>"required", 'label'=>false, 'id'=>'summernote',  "style"=>"min-height:200px"]);?>
                        </div>
                    </div>
                    <div class="item form-group">
                        <div class="controls">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="textarea">Project Target <span class="required">*</span>
                            </label>
                            <div class="col-md-6 col-sm-6 col-xs-12">
                                <div class="input-prepend input-group">
                                    <span class="add-on input-group-addon"><i class="glyphicon glyphicon-calendar fa fa-calendar"></i></span>
                                    <?= $this->Form->input('reservation', ["class"=>"form-control col-md-7 col-xs-12", "required"=>"required", 'label'=>false]);?>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="item form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="number">Select Project Manager <span class="required">*</span>
                        </label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                            <select id="heard" class="select2_single form-control" required="required" name="project_owner">
                                <option value="" >Choose..</option>
                            <?php 
                            $selected = '';
                            foreach ($user as $key) {
                              if($key->id==$project->project_owner){
                                $selected = 'selected';
                              }
                                echo "<option value='$key->id' $selected>$key->username</option>";
                              }
                              
                            ?>
                            </select>
                        </div>
                    </div>
                    <div class="item form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="number">Select Project Contributor<span class="required">*</span>
                        </label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                            <select id="heard" class="select2_single form-control" required="required" name="contributor[]" multiple="multiple">
                                <option value="" >Choose..</option>
                            <?php 
                            $selected = '';
                            foreach ($user as $key) {
                                if(isset($project->project_participant)){
                                    $selected = '';
                                    foreach ($project->project_participant as $ppt ) {
                                      if($key->id==$ppt->user_id){
                                        $selected = 'selected';
                                      }
                                    }
                                  }
                                  echo "<option value='$key->id' $selected>$key->username</option>";
                                  
                                }
                            ?>
                            </select>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-md-6 col-md-offset-3">
                            <button type="submit" class="btn btn-primary">Cancel</button>
                            <button id="send" type="submit" class="btn btn-success">Submit</button>
                        </div>
                    </div>
                <?= $this->Form->end()?>
            </div>
        </div>
    </div>
</div>
<?=$this->element('collapse-box')?>
<?= $this->Html->script("validator/validator.js")?>
<?= $this->Html->script("moment.min2.js")?>
<?= $this->Html->script("datepicker/daterangepicker.js")?>
<?= $this->Html->script("select/select2.full.js")?>
<?= $this->Html->script("summernote/summernote.min.js")?>
<script>
    $(document).ready(function () {
        $(".select2_single").select2({
            placeholder: "Select a state",
            allowClear: true
        });
        $("#summernote").summernote({
            height: 150,
            focus:true
        });
    });
</script>
<script type="text/javascript">
$(document).ready(function () {
    $('#reservation').daterangepicker({
        format: 'YYYY-MM-DD',
        }, function (start, end, label) {
            console.log(start.toISOString(), end.toISOString(), label);
        });
});
</script>
<script>
        // initialize the validator function
        validator.message['date'] = 'not a real date';
        // validate a field on "blur" event, a 'select' on 'change' event & a '.reuired' classed multifield on 'keyup':
        $('form')
            .on('blur', 'input[required], input.optional, select.required', validator.checkField)
            .on('change', 'select.required', validator.checkField)
            .on('keypress', 'input[required][pattern]', validator.keypress);
        $('.multi.required')
            .on('keyup blur', 'input', function () {
                validator.checkField.apply($(this).siblings().last()[0]);
            });
        // bind the validation to the form submit event
        //$('#send').click('submit');//.prop('disabled', true);
        $('form').submit(function (e) {
            e.preventDefault();
            var submit = true;
            // evaluate the form using generic validaing
            if (!validator.checkAll($(this))) {
                submit = false;
            }
            if (submit)
                this.submit();
            return false;
        });
        /* FOR DEMO ONLY */
        $('#vfields').change(function () {
            $('form').toggleClass('mode2');
        }).prop('checked', false);
        $('#alerts').change(function () {
            validator.defaults.alerts = (this.checked) ? false : true;
            if (this.checked)
                $('form .alert').remove();
        }).prop('checked', false);
</script>
