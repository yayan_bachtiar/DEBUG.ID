<div class="" id="p_content">
    <div class="page-title">
        <div class="title_left">
            <h3>Module <small><?= $project->project_name?></small></h3>
        </div>
        <div class="title_right">
            <div class="col-md-5 col-sm-5 col-xs-12 form-group pull-right top_search">
                <div class="input-group">
                    <input type="text" class="form-control" id="jetssearch" placeholder="Search for...">
                    <span class="input-group-btn">
                    <button class="btn btn-default" type="button">Go!</button>
                    </span>
                </div>
            </div>
        </div>
    </div>
    <div class="clearfix"></div>
    <div class="row" >
        <div class="col-md-12">
            <div class="x_panel">
                <div class="x_title">
                    <h2>Module List</h2>
                    <ul class="nav navbar-right panel_toolbox">
                        <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                        </li>
                        <li><a href="<?= $this->Url->build(['controller'=>'module','action'=>'add', $project->id])?>"><i class="fa fa-plus"></i></a>
                        </li>
                        <li class="dropdown">
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i class="fa fa-wrench"></i></a>
                            <ul class="dropdown-menu" role="menu">
                                <li><a href="#">Settings 1</a>
                                </li>
                                <li><a href="#">Settings 2</a>
                                </li>
                            </ul>
                        </li>
                        <li><a class="close-link"><i class="fa fa-close"></i></a>
                        </li>
                    </ul>
                    <div class="clearfix"></div>
                </div>
                <div class="x_content">
                    <p><?= $project->project_desc;?> </p>
                    <!-- start project list -->
                    <table  class="table responsive-utilities ">
                        <thead>
                            <tr>
                                <th style="width: 1%">#</th>
                                <th style="width: 20%">Project Name</th>
                                <th>Team Members</th>
                                <th>Project Progress</th>
                                <th>Status</th>
                                <th style="width: 20%">#Edit</th>
                            </tr>
                        </thead>
                        <tbody id="jetssearch_source">
                        <?php if(!empty($module)){?>
                            <tr>
                                <td>No Data</td>
                            </tr>
                        <?php }?>
                            <?php foreach ($module as $list) {?>
                            <tr>
                                <td>#</td>
                                <td>
                                    <a><?= $list->project_name?></a>
                                    <br />
                                    <small><?= $list->created?></small>
                                </td>
                                <td>
                                    <ul class="list-inline">
                                        <li>
                                            <img src="http://localhost/personal/njoroge/assets3/img.jpg" class="avatar" alt="Avatar">
                                        </li>
                                        <li>
                                            <img src="http://localhost/personal/njoroge/assets3/img.jpg" class="avatar" alt="Avatar">
                                        </li>
                                        <li>
                                            <img src="http://localhost/personal/njoroge/assets3/img.jpg" class="avatar" alt="Avatar">
                                        </li>
                                        <li>
                                            <img src="http://localhost/personal/njoroge/assets3/img.jpg" class="avatar" alt="Avatar">
                                        </li>
                                    </ul>
                                </td>
                                <td class="project_progress">
                                    <div class="progress progress_sm">
                                        <div class="progress-bar bg-green" role="progressbar" data-transitiongoal="57"></div>
                                    </div>
                                    <small>57% Complete</small>
                                </td>
                                <td>
                                    <button type="button" class="btn btn-success btn-xs"><?= $list->status?></button>
                                </td>
                                <td>
                                <?=$this->Html->link('<i class="fa fa-folder"></i> View ', ['action'=>'view', $list->id], ['class'=>'btn btn-primary btn-xs', 'escape'=>false])?>
                                <?=$this->Html->link('<i class="fa fa-pencil"></i> Edit ', ['action'=>'edit', $list->id], ['class'=>'btn btn-info btn-xs', 'escape'=>false])?>
                                    <a href="#" class="btn btn-danger btn-xs"><i class="fa fa-trash-o"></i> Delete </a>
                                </td>
                            </tr>
                            <?php } ?>
                        </tbody>
                    </table>
                    <!-- end project list -->
                    <!-- paginator -->
                    <div class="pull-right">
                        <ul class="pagination pagination" id="passed">
                            <?= $this->Paginator->prev('«', $options = ['model'=>'bpass']) ?>
                            <?= $this->Paginator->numbers() ?>
                            <?= $this->Paginator->next('»', $options = ['model'=>'bpass']) ?>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <?= $this->element('collapse-box');?>
    <?= $this->element('progress');?>
    <script type="text/javascript">
        $("ul#passed.pagination li a").bind("click", function (event) {
            var href = $(this).attr('href');
            $.ajax({
            success:function (response) {
                $("#p_content").html(response);
            },
            url: href,
            error:(function(response){
                location.reload();
                }),
            });
            return false;
        });
    </script>
        <?= $this->element('progress')?>
</div>
<style type="text/css">
.table td img {
    max-width: 30px !important;
}
</style>
<table class="table">
    <thead>
        <tr>
            <th data-field="id">Module ID</th>
            <th data-field="name">Module Name</th>
            <th data-field="price">Start Date</th>
            <th data-field="price">End Date</th>
            <th data-field="price">Assigner</th>
        </tr>
    </thead>
    <tbody>
        <?php foreach($module as $modul):?>
        <tr>
            <td><?= $this->Html->link('#'.$modul->id, ['controller'=>'module', 'action'=>'view?mod='.$this->Encryptor->encryptor('encrypt', $modul->id).'&pid='.$this->Encryptor->encryptor('encrypt', $modul->project_id)]);?></td>
            <td><?= $modul->module_name;?></td>
            <td><?= $modul->start_date;?></td>
            <td><?= $modul->end_date;?></td>
            <td><?php foreach ($modul->module_participant as $user) {
                                                                echo $this->Html->image($this->Encryptor->get_avatar($user->user->email), ['class'=>'circle responsive-img tooltipped', 'data-position'=>'bottom', 'data-delay'=>'50','data-tooltip' => $user->user->firstname.' '.$user->user->lastname]);
                                                                echo " ";
            };?></td>
        </tr>
        <?php endforeach;?>
    </tbody>
</table>
<div class="">
    <ul class="pagination">
        <?= $this->Paginator->prev('<i class="mdi-navigation-chevron-left"></i> ', ['class'=>'', 'escape'=>false]) ?>
        <?= $this->Paginator->numbers() ?>
        <?= $this->Paginator->next('<i class="mdi-navigation-chevron-right"></i> ', ['class'=>'', 'escape'=>false]) ?>
    </ul>
    <p><?= $this->Paginator->counter() ?></p>
</div>
<script type="text/javascript">
    $("ul.pagination li a").bind("click", function (event) {
        var href = $(this).attr('href');
        console.debug(href);
            $( "#loader" ).addClass( "active" );
        $.ajax({
            data: {
"status" : <?= $status; ?>
},
success:function (response) {
$( "#loader" ).removeClass( "active" );
$("#gadul").html(response);
},
url: href,
error:(function(response){
location.reload();
}),
});
return false;
});
$(document).ready(function(){
$('.tooltipped').tooltip({delay: 50});
});
</script>