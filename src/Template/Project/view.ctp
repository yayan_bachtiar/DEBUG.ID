<div class="">
    <div class="page-title">
        <div class="title_left">
            <h3>Project Detail <small> <?=$project->project_name?></small></h3>
        </div>
        <div class="title_right">
            <div class="col-md-5 col-sm-5 col-xs-12 form-group pull-right top_search">
                <div class="input-group">
                    <input type="text" class="form-control" placeholder="Search for...">
                    <span class="input-group-btn">
                    <button class="btn btn-default" type="button">Go!</button>
                    </span>
                </div>
            </div>
        </div>
    </div>
    <div class="clearfix"></div>
    <div class="row">
        <div class="col-md-12">
            <div class="x_panel">
                <div class="x_title">
                    <h2>New Partner Contracts Consultancy</h2>
                    <ul class="nav navbar-right panel_toolbox">
                        <li><a href="#" class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                        </li>
                        <li class="dropdown">
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i class="fa fa-wrench"></i></a>
                            <ul class="dropdown-menu" role="menu">
                                <li><?= $this->Html->link('Edit', ['action'=>'edit', $project->id]);?>
                                </li>
                            </ul>
                        </li>
                        <li><a href="#"><i class="fa fa-close"></i></a>
                        </li>
                    </ul>
                    <div class="clearfix"></div>
                </div>
                <div class="x_content">
                    <div class="col-md-9 col-sm-9 col-xs-12">
                        <div class="row top_tiles">
                            <div class="animated flipInY col-lg-4 col-md-4 col-sm-6 col-xs-12">
                                <a class="tile-stats" href="<?= $this->url->build(['controller'=>'module', 'action'=>'all_modules?pid='.$project->id])?>">
                                <div class="icon"><i class="fa fa-cubes"></i>
                                </div>
                                <div class="count" id="modules">0</div>
                                <h3>Module Finished</h3>
                                <p class="mod"></p>
                                </a>
                            </div>
                            <div class="animated flipInY col-lg-4 col-md-4 col-sm-6 col-xs-12">
                                <a class="tile-stats" href="<?= $this->Url->build(['controller'=>'task', 'action'=>'all_tasks?pid='.$project->id]);?>">
                                <div class="icon"><i class="fa fa-tasks"></i>
                                </div>
                                <div class="count" id="tasks">0</div>
                                <h3>Taks Finished</h3>
                                <p class="tasks"></p>
                                </a>
                            </div>
                            <div class="animated flipInY col-lg-4 col-md-4 col-sm-6 col-xs-12">
                                <a class="tile-stats" href="<?= $this->Url->build(['controller'=>'bugs', 'action'=>'all_bugs?pid='.$project->id]);?>">
                                <div class="icon"><i class="fa fa-bug"></i>
                                </div>
                                <div class="count" id="bugs">0</div>
                                <h3>Bug Fixed</h3>
                                <p class="bugs"></p>
                                </a>
                            </div>
                        </div>
                        <br />
                        <div id="mainb" style="height:350px;">
                        </div>
                        <div class="x_content">
                            <div class="" role="tabpanel" data-example-id="togglable-tabs">
                                <ul id="myTab" class="nav nav-tabs bar_tabs" role="tablist">
                                    <li role="presentation" class="active"><a href="#tab_content1" id="home-tab" role="tab" data-toggle="tab" aria-expanded="true">Task</a>
                                    </li>
                                    <li role="presentation" class=""><a href="#tab_content2" role="tab" id="profile-tab" data-toggle="tab"  aria-expanded="false">Bugs</a>
                                    </li>
                                    <li role="presentation" class=""><a href="#tab_content3" role="tab" id="profile-tab2" data-toggle="tab" aria-expanded="false">Logs</a>
                                    </li>
                                </ul>
                                <div id="myTabContent" class="tab-content">
                                    <div role="tabpanel" class="tab-pane fade active in" id="tab_content1" aria-labelledby="home-tab">
                                        <div id="t_content">
                                        </div>
                                    </div>
                                    <div role="tabpanel" class="tab-pane fade" id="tab_content2" aria-labelledby="profile-tab">
                                        <div id="b_content">
                                        </div>
                                    </div>
                                    <div role="tabpanel" class="tab-pane fade" id="tab_content3" aria-labelledby="profile-tab">
                                        <div id="l_content">

                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                    </div>
                    <!-- start project-detail sidebar -->
                    <div class="col-md-3 col-sm-3 col-xs-12">
                        <section class="panel">
                            <div class="x_title">
                                <h2>Project Description</h2>
                                <div class="clearfix"></div>
                            </div>
                            <div class="panel-body">
                                <h3 class="green"><i class="fa fa-paint-brush"></i> <?=$project->project_name?></h3>
                                <p><?=$project->project_desc?></p>
                                <br />
                                <div class="project_detail">
                                    <p class="title">Client Company</p>
                                    <p>Deveint Inc</p>
                                    <p class="title">Project Leader</p>
                                    <p><?=$project->user->first_name?></p>
                                </div>
                                <br />
                                <h5>Project files</h5>
                                <ul class="list-unstyled project_files">
                                    <?php foreach ($project->project_attachment as $key) {
                                        $ext = $key->ext;
                                        $fa ="fa fa-file";
                                        if($ext == 'png'||$ext == 'jpg'){
                                            $fa ="fa fa-picture-o";
                                        }
                                        if($ext == 'pdf'){
                                            $fa ="fa fa-file-pdf-o";
                                        }
                                        if($ext == 'doc'|| $ext == 'docx'){
                                            $fa ="fa-file-word-o";
                                        }
                                        echo '<li><a href='.$this->request->webroot.'webroot/'. $key->path.'/'.$key->filename.' class="" target="_blank"><i class="'.$fa.'"></i>' .$key->filename.'</a></li>';
                                    }?>
                                </ul>
                                <br />
                                <div class="text-center mtop20">
                                    <!-- Small modal -->
                                    <button type="button" class="btn btn-sm btn-primary" data-toggle="modal" data-target=".bs-example-modal-sm">Add Files</button>
                                    <div class="modal fade bs-example-modal-sm" tabindex="-1" role="dialog" aria-hidden="true">
                                        <div class="modal-dialog modal-sm">
                                            <div class="modal-content">
                                                <div class="modal-header">
                                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span>
                                                    </button>
                                                    <h4 class="modal-title" id="myModalLabel2">Upload Files</h4>
                                                </div>
                                                <div class="modal-body">
                                                    <form action="<?= $this->Url->build(['controller'=>'projectAttachment', 'action'=>'add?id='.$project->id]);?>" class="dropzone" style="border: 1px solid #e5e5e5; height: 100%; ">
                                                    </form>
                                                </div>
                                                <div class="modal-footer">
                                                    <button type="button" class="btn btn-primary">Save changes</button>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </section>
                    </div>
                    <!-- end project-detail sidebar -->
                </div>
            </div>
        </div>
    </div>
</div>
<!-- dropzone -->
<?= $this->Html->script('dropzone/dropzone.js');?>
<!-- echart -->
<?= $this->Html->script('echart/echarts-all.js');?>
<?= $this->Html->script('echart/green.js');?>
<?= $this->element('collapse-box');?>
<script type="text/javascript">
    get_log();
    get_count();
    get_task();
    get_bug();
    function get_task(){
        $.ajax({
        url: "<?= $this->Url->build(['controller'=>'task', 'action'=>'module_tasks'])?>",
        type: 'GET',
        data: {pid: "<?php echo $project->id ?>"},
        })
        .done(function(response) {
            $("#t_content").html(response)
            console.log("success");
        })
        .fail(function() {
            console.log("error");
        })
        .always(function() {
            console.log("complete");
        });
    }

    function get_bug(){
        $.ajax({
            url: "<?= $this->Url->build(['controller'=>'bugs', 'action'=>'project_get_bug', $project->id])?>",
            type: 'GET',
            data: {pid: "<?php echo $project->id ?>"},
        })
        .done(function(response) {
            $("#b_content").html(response)
            console.log("success");
        })
        .fail(function() {
            console.log("error");
        })
        .always(function() {
            console.log("complete");
        });
    }

        /**
    * get log
    * @return {[type]} [description]
    */
    function get_log(){
    $.ajax({
        url: "<?= $this->Url->build(['controller'=>'ProjectLog', 'action'=>'get_log']);?>",
        type: 'GET',
        data: {p_id: "<?= $project->id;?>"},
        })
        .done(function(response) {
        $("#l_content").html(response);
        console.log("success");
        })
        .fail(function() {
        console.log("error");
        })
        .always(function() {
        console.log("complete");
        });
        }
    function get_count(){
    $.ajax({
    url: "<?= $this->Url->build(['controller'=>'project', 'action'=>'get_count']);?>",
    type: 'GET',
    // dataType: 'json',
    data: {id: '<?=$project->id;?>'},
    })
    .done(function(response) {
        var json = jQuery.parseJSON(response);
        $("#modules").html(json.modules);
        $("#tasks").html(json.tasks);
        $("#bugs").html(json.bugs);
        $(".mod").html(json.modules_open+' module open of '+json.modules_all);
        $(".tasks").html(json.tasks_ready+' tasks ready to test of '+json.tasks_all);
        $(".bugs").html(json.bugs_tested+' bugs tested of '+json.bugs_all);
        })
        .fail(function() {
        console.log("error");
        })
        .always(function() {
        console.log("complete");
        });
    }   
</script>
<script>
        var myChart9 = echarts.init(document.getElementById('mainb'), theme);
        myChart9.setOption({
            title: {
                x: 'center',
                y: 'top',
                padding: [0, 0, 20, 0],
                text: 'Project Perfomance :: Revenue vs Input vs Time Spent',
                textStyle: {
                    fontSize: 15,
                    fontWeight: 'normal'
                }
            },
            tooltip: {
                trigger: 'axis'
            },
            toolbox: {
                show: true,
                feature: {
                    dataView: {
                        show: true,
                        readOnly: false
                    },
                    restore: {
                        show: true
                    },
                    saveAsImage: {
                        show: true
                    }
                }
            },
            calculable: true,
            legend: {
                data: ['Revenue', 'Cash Input', 'Time Spent'],
                y: 'bottom'
            },
            xAxis: [
                {
                    type: 'category',
                    data: ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec']
            }
        ],
            yAxis: [
                {
                    type: 'value',
                    name: 'Amount',
                    axisLabel: {
                        formatter: '{value} ml'
                    }
            },
                {
                    type: 'value',
                    name: 'Hours',
                    axisLabel: {
                        formatter: '{value} °C'
                    }
            }
        ],
            series: [
                {
                    name: 'Revenue',
                    type: 'bar',
                    data: [2.0, 4.9, 7.0, 23.2, 25.6, 76.7, 135.6, 162.2, 32.6, 20.0, 6.4, 3.3]
            },
                {
                    name: 'Cash Input',
                    type: 'bar',
                    data: [2.6, 5.9, 9.0, 26.4, 28.7, 70.7, 175.6, 182.2, 48.7, 18.8, 6.0, 2.3]
            },
                {
                    name: 'Time Spent',
                    type: 'line',
                    yAxisIndex: 1,
                    data: [2.0, 2.2, 3.3, 4.5, 6.3, 10.2, 20.3, 23.4, 23.0, 16.5, 12.0, 6.2]
            }
        ]
        });
</script>