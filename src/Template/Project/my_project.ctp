<div class="container">
    <div class="row">
        <div class="col l12">
            <br>
            <nav>
                <div class="nav-wrapper">
                    <form action="<?= $this->Url->build(null);?>" method="post">
                        <div class="input-field">
                            <input id="search" type="search" required autocomplete="off" placeholder="Search">
                            <label for="search"><i class="mdi-action-search"></i></label>
                            <i class="mdi-navigation-close"></i>
                        </div>
                    </form>
                </div>
            </nav>
            <br>
            <div class="right-align">
                <a class='dropdown-button btn' href='#' data-activates='dropdown1'><i class="mdi-content-sort"></i> Sorting By</a>
                <!-- Dropdown Structure -->
                <ul id='dropdown1' class='dropdown-content'>
                    <li><?= $this->Paginator->sort('project_name') ?></li>
                    <li><?= $this->Paginator->sort('project_start') ?></li>
                    <li><?= $this->Paginator->sort('project_end') ?></li>
                    <li><?= $this->Paginator->sort('created') ?></li>
                </ul>
            </div>
            <div class="project index large-10 medium-9 columns">
                <div class="row">
                    <?php foreach($project as $project):?>
                    <div class="col l6 s12">
                        <div class="card">
                            <div class="card-image waves-effect waves-block waves-light">
                                <?php if(file_exists(WWW_ROOT .'images/'.$project->project->project_image)){
    echo $this->Html->image('../images/'.$project->project->project_image, ['class'=>'activator']);
}else{
    echo $this->Html->image('../images/Project-Management-Task-Board.jpg', ['class'=>'activator']);
} ?>
                            </div>
                            <div class="card-content">
                                <span class="card-title activator grey-text text-darken-4"><?= $project->project->project_name;?><i class="mdi-navigation-more-vert right"></i></span>
                                <p><?= $this->Html->link('Open Project', ['action'=>'view', $this->Encryptor->encryptor('encrypt', $project->project->id)]); ?></p>
                            </div>
                            <div class="card-reveal">
                                <span class="card-title grey-text text-darken-4"><?= $project->project->project_name?><i class="mdi-navigation-close right"></i></span>
                                <p><?= $project->project->project_desc;?></p>
                                <p>Project Start :<?= $project->project->project_start;?></p>
                                <p>Project End :<?= $project->project->project_end;?></p>
                                <p>Last Modified :<?= $project->project->modified;?></p>
                            </div>
                        </div>
                    </div>
                    <?php endforeach;?>
                </div>
                <div class="">
                    <ul class="pagination">
                        <?= $this->Paginator->prev('<i class="mdi-navigation-chevron-left"></i> ', ['class'=>'', 'escape'=>false]) ?>
                        <?= $this->Paginator->numbers() ?>
                        <?= $this->Paginator->next('<i class="mdi-navigation-chevron-right"></i> ', ['class'=>'', 'escape'=>false]) ?>
                    </ul>
                    <p><?= $this->Paginator->counter() ?></p>
                </div>
            </div>
        </div>
    </div>
</div>