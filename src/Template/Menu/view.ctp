<div class="actions columns large-2 medium-3">
    <h3><?= __('Actions') ?></h3>
    <ul class="side-nav">
        <li><?= $this->Html->link(__('Edit Menu'), ['action' => 'edit', $menu->id]) ?> </li>
        <li><?= $this->Form->postLink(__('Delete Menu'), ['action' => 'delete', $menu->id], ['confirm' => __('Are you sure you want to delete # {0}?', $menu->id)]) ?> </li>
        <li><?= $this->Html->link(__('List Menu'), ['action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Menu'), ['action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Role'), ['controller' => 'Role', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Role'), ['controller' => 'Role', 'action' => 'add']) ?> </li>
    </ul>
</div>
<div class="menu view large-10 medium-9 columns">
    <h2><?= h($menu->id) ?></h2>
    <div class="row">
        <div class="large-5 columns strings">
            <h6 class="subheader"><?= __('Menu') ?></h6>
            <p><?= h($menu->menu) ?></p>
            <h6 class="subheader"><?= __('Link') ?></h6>
            <p><?= h($menu->link) ?></p>
        </div>
        <div class="large-2 columns numbers end">
            <h6 class="subheader"><?= __('Id') ?></h6>
            <p><?= $this->Number->format($menu->id) ?></p>
            <h6 class="subheader"><?= __('Urutan') ?></h6>
            <p><?= $this->Number->format($menu->urutan) ?></p>
            <h6 class="subheader"><?= __('Parent') ?></h6>
            <p><?= $this->Number->format($menu->parent) ?></p>
        </div>
    </div>
</div>
<div class="related row">
    <div class="column large-12">
    <h4 class="subheader"><?= __('Related Role') ?></h4>
    <?php if (!empty($menu->role)): ?>
    <table cellpadding="0" cellspacing="0">
        <tr>
            <th><?= __('Id') ?></th>
            <th><?= __('Role Name') ?></th>
            <th><?= __('Created By') ?></th>
            <th><?= __('Created') ?></th>
            <th><?= __('Modified') ?></th>
            <th><?= __('Role Desc') ?></th>
            <th class="actions"><?= __('Actions') ?></th>
        </tr>
        <?php foreach ($menu->role as $role): ?>
        <tr>
            <td><?= h($role->id) ?></td>
            <td><?= h($role->role_name) ?></td>
            <td><?= h($role->created_by) ?></td>
            <td><?= h($role->created) ?></td>
            <td><?= h($role->modified) ?></td>
            <td><?= h($role->role_desc) ?></td>

            <td class="actions">
                <?= $this->Html->link(__('View'), ['controller' => 'Role', 'action' => 'view', $role->id]) ?>

                <?= $this->Html->link(__('Edit'), ['controller' => 'Role', 'action' => 'edit', $role->id]) ?>

                <?= $this->Form->postLink(__('Delete'), ['controller' => 'Role', 'action' => 'delete', $role->id], ['confirm' => __('Are you sure you want to delete # {0}?', $role->id)]) ?>

            </td>
        </tr>

        <?php endforeach; ?>
    </table>
    <?php endif; ?>
    </div>
</div>
