<div class="menu_section">
    <h3>General</h3>
    <ul class="nav side-menu">
        <?php foreach ($menu as $menulist) {?>
        <li><a><i class="fa fa-<?= $menulist->icons;?>"></i> <?=$menulist->menu;?> <span class="fa fa-chevron-down"></span></a>
            <ul class="nav child_menu" style="display: none">
                <?php foreach ($menulist->menu_modul as $key) {?>
                <li><a id="ajax" href="<?= $this->Url->build(['controller'=>$key->link, 'action'=>'index']);?>"><?=$key->modul?></a>
                </li>
                <?php }?>
            </ul>
        </li>
        <?php }?>
    </ul>
</div>
<script type="text/javascript">
    $('#sidebar-menu li ul').slideUp();
    // $('#sidebar-menu li').removeClass('active');
    $('#sidebar-menu li').click(function() {
        if ($(this).is('.active')) {
            $(this).removeClass('active');
            // $('ul', this).slideUp();
            $(this).removeClass('nv');
            $(this).addClass('vn');
        } else {
            $('#sidebar-menu li ul').slideUp();
            $(this).removeClass('vn');
            $(this).addClass('nv');
            $('ul', this).slideDown();
            $('#sidebar-menu li').removeClass('active');
            $(this).addClass('active');
        }
    });
</script>
<script type="text/javascript">
//         $("a#ajax").bind("click", function (event) {
//             var href = $(this).attr('href');
//             $.ajax({
//             success:function (response) {
//                 NProgress.start();
//                 $("#content").html(response);
//             },
//             always:function(){
//                 NProgress.done();
//             },
//             url: href,
//             error:(function(response){
//                 location.reload();
//                 }),
//             });
//             return false;
//         });
</script>