<div class="container">
    <ul class="collection" id="toggle-user" >
        <?php foreach($users as $users):?>
        <li  class="collection-item avatar col s12 l4 <?php
            if (in_array($users->id, $user)) {
                echo "active";
            }
            ?>" id="avatar<?= $users->id;?>" onclick="showonlyone('<?= $users->id;?>')">
            <?= $this->Html->image($this->Encryptor->get_avatar($users->email), ['class'=>'circle', 'user image', 'height'=>'42']);?>
            <!--            <img src="images/yuna.jpg" alt="" class="circle">-->
            <span class="title"><?= $users->firstname.' '.$users->lastname;?></span>
            <p>Position : <?= $users->role ?></p>
        </li>
        <?php endforeach;?>
    </ul>
    <a class="btn waves-effect waves-light orange" href="<?= $this->Url->build(['controller'=>'module', 'action'=>'view?pid='.$project_id.'&mod='.$modul_id]);?>">Cancel
    <i class="material-icons left">replay</i>
    </a>
    <a class="btn waves-effect waves-light" href="<?= $this->Url->build(['controller'=>'module_attachment', 'action'=>'add?mod='.$modul_id."&pid=".$project_id]);?>">Next
    <i class="mdi-content-send right"></i>
    </a>
</div>
<script type="text/javascript">
    function showonlyone(thechosenone) {
        $('#avatar'+thechosenone).each(function(){
            $(this).toggleClass('active');
            method = 'drop';
            if( $(this).hasClass('active')){
                method = 'add';
            }
            $.ajax(document.URL, {
                // Pass our data to the server
                data: { "id" : thechosenone , "method" : method},
                // Pass using the appropriate method
                method: "POST",
                // When the request is completed and successful, run this code.
                success: function (response) {
                    // Successfully added to favorites. JS code goes here for this condition.
                                        //                    alert (response)
                },
                error: function (response){
                                        //                    alert (response);
                }
            });
        });
    }
</script>