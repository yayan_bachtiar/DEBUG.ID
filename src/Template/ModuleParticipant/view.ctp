<div class="actions columns large-2 medium-3">
    <h3><?= __('Actions') ?></h3>
    <ul class="side-nav">
        <li><?= $this->Html->link(__('Edit Module Participant'), ['action' => 'edit', $moduleParticipant->id]) ?> </li>
        <li><?= $this->Form->postLink(__('Delete Module Participant'), ['action' => 'delete', $moduleParticipant->id], ['confirm' => __('Are you sure you want to delete # {0}?', $moduleParticipant->id)]) ?> </li>
        <li><?= $this->Html->link(__('List Module Participant'), ['action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Module Participant'), ['action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Module'), ['controller' => 'Module', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Module'), ['controller' => 'Module', 'action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Users'), ['controller' => 'Users', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New User'), ['controller' => 'Users', 'action' => 'add']) ?> </li>
    </ul>
</div>
<div class="moduleParticipant view large-10 medium-9 columns">
    <h2><?= h($moduleParticipant->id) ?></h2>
    <div class="row">
        <div class="large-5 columns strings">
            <h6 class="subheader"><?= __('Module') ?></h6>
            <p><?= $moduleParticipant->has('module') ? $this->Html->link($moduleParticipant->module->id, ['controller' => 'Module', 'action' => 'view', $moduleParticipant->module->id]) : '' ?></p>
            <h6 class="subheader"><?= __('User') ?></h6>
            <p><?= $moduleParticipant->has('user') ? $this->Html->link($moduleParticipant->user->id, ['controller' => 'Users', 'action' => 'view', $moduleParticipant->user->id]) : '' ?></p>
        </div>
        <div class="large-2 columns numbers end">
            <h6 class="subheader"><?= __('Id') ?></h6>
            <p><?= $this->Number->format($moduleParticipant->id) ?></p>
        </div>
    </div>
</div>
