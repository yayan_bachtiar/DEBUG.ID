<div class="actions columns large-2 medium-3">
    <h3><?= __('Actions') ?></h3>
    <ul class="side-nav">
        <li><?= $this->Html->link(__('New Module Participant'), ['action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Module'), ['controller' => 'Module', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Module'), ['controller' => 'Module', 'action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Users'), ['controller' => 'Users', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New User'), ['controller' => 'Users', 'action' => 'add']) ?> </li>
    </ul>
</div>
<div class="moduleParticipant index large-10 medium-9 columns">
    <table cellpadding="0" cellspacing="0">
    <thead>
        <tr>
            <th><?= $this->Paginator->sort('id') ?></th>
            <th><?= $this->Paginator->sort('modul_id') ?></th>
            <th><?= $this->Paginator->sort('user_id') ?></th>
            <th class="actions"><?= __('Actions') ?></th>
        </tr>
    </thead>
    <tbody>
    <?php foreach ($moduleParticipant as $moduleParticipant): ?>
        <tr>
            <td><?= $this->Number->format($moduleParticipant->id) ?></td>
            <td>
                <?= $moduleParticipant->has('module') ? $this->Html->link($moduleParticipant->module->id, ['controller' => 'Module', 'action' => 'view', $moduleParticipant->module->id]) : '' ?>
            </td>
            <td>
                <?= $moduleParticipant->has('user') ? $this->Html->link($moduleParticipant->user->id, ['controller' => 'Users', 'action' => 'view', $moduleParticipant->user->id]) : '' ?>
            </td>
            <td class="actions">
                <?= $this->Html->link(__('View'), ['action' => 'view', $moduleParticipant->id]) ?>
                <?= $this->Html->link(__('Edit'), ['action' => 'edit', $moduleParticipant->id]) ?>
                <?= $this->Form->postLink(__('Delete'), ['action' => 'delete', $moduleParticipant->id], ['confirm' => __('Are you sure you want to delete # {0}?', $moduleParticipant->id)]) ?>
            </td>
        </tr>

    <?php endforeach; ?>
    </tbody>
    </table>
    <div class="paginator">
        <ul class="pagination">
            <?= $this->Paginator->prev('< ' . __('previous')) ?>
            <?= $this->Paginator->numbers() ?>
            <?= $this->Paginator->next(__('next') . ' >') ?>
        </ul>
        <p><?= $this->Paginator->counter() ?></p>
    </div>
</div>
