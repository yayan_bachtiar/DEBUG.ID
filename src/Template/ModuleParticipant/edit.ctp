<div class="actions columns large-2 medium-3">
    <h3><?= __('Actions') ?></h3>
    <ul class="side-nav">
        <li><?= $this->Form->postLink(
                __('Delete'),
                ['action' => 'delete', $moduleParticipant->id],
                ['confirm' => __('Are you sure you want to delete # {0}?', $moduleParticipant->id)]
            )
        ?></li>
        <li><?= $this->Html->link(__('List Module Participant'), ['action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('List Module'), ['controller' => 'Module', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Module'), ['controller' => 'Module', 'action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Users'), ['controller' => 'Users', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New User'), ['controller' => 'Users', 'action' => 'add']) ?> </li>
    </ul>
</div>
<div class="moduleParticipant form large-10 medium-9 columns">
    <?= $this->Form->create($moduleParticipant); ?>
    <fieldset>
        <legend><?= __('Edit Module Participant') ?></legend>
        <?php
            echo $this->Form->input('modul_id', ['options' => $module]);
            echo $this->Form->input('user_id', ['options' => $users]);
        ?>
    </fieldset>
    <?= $this->Form->button(__('Submit')) ?>
    <?= $this->Form->end() ?>
</div>
