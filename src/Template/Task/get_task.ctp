<?php echo $this->Html->css('icheck/flat/green.css'); ?>
<ul class="to_do">
<?= $this->Paginator->counter(
    'Page {{page}} of {{pages}}, showing {{current}} records out of
     {{count}} total, starting on record {{start}}, ending on {{end}}'
);?>
    <?php foreach ($tasks as $key) {
        $checked = 'unchecked';
        if($key->status == 4){
            $checked = 'checked';
    }?>
    <li>
        <p>
        <input type="checkbox" <?= $checked; ?> class="flat" value="<?= $key->id;?>"> TASKID #<?= $key->id?> <?= $key->task_name;?></p>
    </li>
    <?php }?>
</ul>
<div class="pull-right">
    <ul class="pagination pagination" id="task">
        <?= $this->Paginator->prev('«', $options = ['model'=>'bpass']) ?>
        <?= $this->Paginator->numbers() ?>
        <?= $this->Paginator->next('»', $options = ['model'=>'bpass']) ?>
    </ul>
</div>
<?=  $this->Html->script('icheck/icheck.min.js');?>

<script type="text/javascript">
if ($("input.flat")[0]) {
    $(document).ready(function() {
        $('input.flat').iCheck({
            checkboxClass: 'icheckbox_flat-green',
            radioClass: 'iradio_flat-green'
        });
    });
}
$("ul#task.pagination li a").bind("click", function (event) {
    var href = $(this).attr('href');
    if(href!=null){
        $.ajax({
        success:function (response) {
            $("#t_content").html(response);
        },
        url: href,
        error:(function(response){
            // location.reload();
            }),
        });
    }
    return false;
    });
</script>
<script type="text/javascript">
    if ($("input.flat")[0]) {
    $(document).ready(function () {
        $('input.flat').on('ifChecked', function(event){
            var data = $(this).val();
            $.ajax({
                url: "<?= $this->Url->build(['controller'=>'task', 'action'=>'ready'])?>",
                type: 'GET',
                data: {id: data},
            })
            .done(function(response) {
                toastr.success("Task has been finished");
            })
            .fail(function() {
                toastr.warning("You must login first");
                location.reload();
                console.log("error");
            })
            .always(function() {
                console.log("complete");
            });
            
        });
        // $('input.flat').iCheck({
                    //     checkboxClass: 'icheckbox_flat-green',
                    //     radioClass: 'iradio_flat-green'
        // });
    });
}
</script>
<script type="text/javascript">
     $('input.flat').on('ifUnchecked', function(event){
            var data = $(this).val();
            $.ajax({
                url: "<?= $this->Url->build(['controller'=>'task', 'action'=>'unready'])?>",
                type: 'GET',
                data: {id: data},
            })
            .done(function(response) {

                toastr.success("Task has been canceled");
            })
            .fail(function() {
                console.log("error");
            })
            .always(function() {
                console.log("complete");
            });
            
        });
</script>