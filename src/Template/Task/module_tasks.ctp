<table  class="table responsive-utilities ">
    <thead>
        <tr>
            <th style="width: 1%">#</th>
            <th style="width: 20%">Task</th>
            <th>Team Members</th>
            <th>Module</th>
            <th>Status</th>
            <th style="width: 20%">#Edit</th>
        </tr>
    </thead>
    <tbody id="jetssearch_source">
        <?php foreach ($task as $list) {?>
        <tr>
            <td>#<?php echo $list->id;?> </td>
            <td>
                <?= $this->Html->link($list->task_name, ['controller'=>'module', 'action'=>'view', $list->modul_id])?>
                <br />
                <small><?= $list->created?></small>
            </td>
            <td>
                <ul class="list-inline">
                    <?php $participant = $list->module->module_participant;
                    foreach ($participant as $users) {?>
                    <li>
                        <img src="<?= $this->General->get_avatar($users->user->email)?>" class="avatar" alt="Avatar" title="<?= $users->user->username;?>">
                    </li>
                    <?php }?>
                </ul>
            </td>
            <td class="module">
                <?php echo $list->module->module_name ?>
            </td>
            <td>
                <button type="button" class="btn btn-success btn-xs"><?= $list->status->name?></button>
            </td>
            <td>
                <?=$this->Html->link('<i class="fa fa-folder"></i> View ', ['controller'=>'module', 'action'=>'view', $list->module->id], ['class'=>'btn btn-primary btn-xs', 'escape'=>false])?>
                <?=$this->Html->link('<i class="fa fa-pencil"></i> Edit ', ['controller'=>'module', 'action'=>'edit', $list->module->id], ['class'=>'btn btn-info btn-xs', 'escape'=>false])?>
                <a href="#" class="btn btn-danger btn-xs"><i class="fa fa-trash-o"></i> Delete </a>
            </td>
        </tr>
        <?php } ?>
    </tbody>
</table>
<!-- end project list -->
<!-- paginator -->
<div class="pull-right">
    <ul class="pagination pagination" id="task">
        <?= $this->Paginator->prev('«', $options = ['model'=>'bpass']) ?>
        <?= $this->Paginator->numbers() ?>
        <?= $this->Paginator->next('»', $options = ['model'=>'bpass']) ?>
    </ul>
</div>
<script type="text/javascript">
    $("ul#task.pagination li a").bind("click", function (event) {
        var href = $(this).attr('href');
        $.ajax({
        success:function (response) {
            $("#t_content").html(response);
        },
        url: href,
        error:(function(response){
            location.reload();
            }),
        });
        return false;
    });
</script>
