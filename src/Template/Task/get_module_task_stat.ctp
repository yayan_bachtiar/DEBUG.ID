<div class="container">
    <div class="panel">
        <div class="block-web">
            <div class="header">
                <div class="actions">
                    <a href="#" class="minimize"><i class="fa fa-chevron-down"></i></a>
                    <a href="#" class="refresh" onclick="get_task()"><i class="fa fa-repeat"></i></a>
                    <a href="#" class="close-down" ><i class="fa fa-times"></i></a>
                </div>
                <h3>Task Statistic</h3>
            </div>
            <div class="porlets-content">
                <div class="ticket_open_grid text-center"><b class=""> <?=$percent;?>% Done</b><span class="ticket_open_grid_progress" style="width:<?= $percent;?>%; margin-top:-19px"></span>
                </div>
                <div class="row">
                    <div class="col-md-6">
                        <p>Total task : <?=$this->Html->link($task_sum,  ['controller'=>'task', 'action'=>'get_module_task?module_id='. $modul_id."&type=all"])?></p>
                        <p>Total done : <?=$this->Html->link($task_sum_done,  ['controller'=>'task', 'action'=>'get_module_task?module_id='. $modul_id."&type=done"])?></p>
                        <p>Total not yet : <?=$this->Html->link($task_sum_undone,  ['controller'=>'task', 'action'=>'get_module_task?module_id='. $modul_id."&type=undone"])?></p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>