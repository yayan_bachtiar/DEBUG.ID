<div class="todo_body animated fadeInRight" >
    <h5 class="blue_bg"> <i class="fa fa-tasks"></i>  Task List (
    <small><?= $numbers?></small>
    )
    </h5>
    <?php
                    if($numbers==0){
    ?>
    <li class="list-group-item">
        Task No Longer Available
    </li>
    <?php }
                    $i=0;
    foreach ($tasks as $task) {?>
    <ul class="group_sortable1">
        <li>
            <span class=""><input name="" value="" type="checkbox"></span>
            <p><strong>Task  #<?=$task->id?></strong>
            - <?= $task->task_name;?> . [
            <a class="font-xs" href="javascript:void(0);">More Details</a>]
            <b><?= substr($task->description, 0, 100)?></b>
            <b><?= date('M, d Y', strtotime($task->created));?></b>
            </p>
        </li>
    </ul>
    <?php }?>
</div>
<div class="pull-right">
    <ul class="pagination pagination" id="passed">
        <?= $this->Paginator->prev('«', $options = ['model'=>'bpass']) ?>
        <?= $this->Paginator->numbers() ?>
        <?= $this->Paginator->next('»', $options = ['model'=>'bpass']) ?>
    </ul>
</div>
<script type="text/javascript">
    $("ul#passed.pagination li a").bind("click", function (event) {
        var href = $(this).attr('href');
        $.ajax({
        success:function (response) {
            $("#contentpanel").html(response);
        },
        url: href,
        error:(function(response){
            location.reload();
            }),
        });
        return false;
    });
</script>