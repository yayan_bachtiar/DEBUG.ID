<?php extract($content);?>
<p>Dear <?php echo $realname ?></p>
<p>Satu bug di temukan  pada module <?php echo $this->Html->link($module_name, ['_full'=>true, 'controller'=>'module', 'action'=>"view", $module_id]) ?> dengan detail sebagai berikut:</p>
<p>Nama Bug   : <?php echo $this->Html->link($bugs_name, ['_full'=>true, 'controller'=>'bugs', 'action'=>"view", $bug_id]) ?></p>
<p>Skenario   : <?php echo $scenario ?></p>
<p>Ekspektasi : <?php echo $expektasi ?></p>
<p>Status     : <?php echo $status ?></p>
<p>Prioritas  : <?php echo $priority ?></p>
<p>Start Date : <?php echo $start_date ?></p>
<p>Due Date   : <?php echo $due_date ?></p>
<br>
<p>Reported By  : <?=  $this->Html->link($full_name, ['_full'=>true, 'controller'=>'user', 'action'=>'view', $created_by],['class'=>'', 'escape'=>false]);?></p>
<p>silahkan klik <?php echo $this->Html->link("disini", ['_full'=>true, 'controller'=>'bugs', 'action'=>"view", $bug_id]) ?> untuk melihatnya lebih detail</p>

<p></p>
<p></p>
<p></p>
<p>Terima Kasih</p>
<p>Salam</p>
<p>Assigner</p>
<p></p>
<p></p>
<p><u><?= $full_name;?></u></p>
<p>----------------------</p>