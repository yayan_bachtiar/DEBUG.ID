<div class="actions columns large-2 medium-3">
    <h3><?= __('Actions') ?></h3>
    <ul class="side-nav">
        <li><?= $this->Html->link(__('Edit Menu Modul'), ['action' => 'edit', $menuModul->id]) ?> </li>
        <li><?= $this->Form->postLink(__('Delete Menu Modul'), ['action' => 'delete', $menuModul->id], ['confirm' => __('Are you sure you want to delete # {0}?', $menuModul->id)]) ?> </li>
        <li><?= $this->Html->link(__('List Menu Modul'), ['action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Menu Modul'), ['action' => 'add']) ?> </li>
    </ul>
</div>
<div class="menuModul view large-10 medium-9 columns">
    <h2><?= h($menuModul->id) ?></h2>
    <div class="row">
        <div class="large-2 columns numbers end">
            <h6 class="subheader"><?= __('Id') ?></h6>
            <p><?= $this->Number->format($menuModul->id) ?></p>
            <h6 class="subheader"><?= __('Menu Id') ?></h6>
            <p><?= $this->Number->format($menuModul->menu_id) ?></p>
            <h6 class="subheader"><?= __('Link') ?></h6>
            <p><?= $this->Number->format($menuModul->link) ?></p>
        </div>
    </div>
</div>
