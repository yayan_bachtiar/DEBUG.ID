<div class="actions columns large-2 medium-3">
    <h3><?= __('Actions') ?></h3>
    <ul class="side-nav">
        <li><?= $this->Html->link(__('List Menu Modul'), ['action' => 'index']) ?></li>
    </ul>
</div>
<div class="menuModul form large-10 medium-9 columns">
    <?= $this->Form->create($menuModul) ?>
    <fieldset>
        <legend><?= __('Add Menu Modul') ?></legend>
        <?php
            echo $this->Form->input('menu_id');
            echo $this->Form->input('link');
        ?>
    </fieldset>
    <?= $this->Form->button(__('Submit')) ?>
    <?= $this->Form->end() ?>
</div>
