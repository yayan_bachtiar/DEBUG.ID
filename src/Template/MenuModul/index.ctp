<div class="actions columns large-2 medium-3">
    <h3><?= __('Actions') ?></h3>
    <ul class="side-nav">
        <li><?= $this->Html->link(__('New Menu Modul'), ['action' => 'add']) ?></li>
    </ul>
</div>
<div class="menuModul index large-10 medium-9 columns">
    <table cellpadding="0" cellspacing="0">
    <thead>
        <tr>
            <th><?= $this->Paginator->sort('id') ?></th>
            <th><?= $this->Paginator->sort('menu_id') ?></th>
            <th><?= $this->Paginator->sort('link') ?></th>
            <th class="actions"><?= __('Actions') ?></th>
        </tr>
    </thead>
    <tbody>
    <?php foreach ($menuModul as $menuModul): ?>
        <tr>
            <td><?= $this->Number->format($menuModul->id) ?></td>
            <td><?= $this->Number->format($menuModul->menu_id) ?></td>
            <td><?= $this->Number->format($menuModul->link) ?></td>
            <td class="actions">
                <?= $this->Html->link(__('View'), ['action' => 'view', $menuModul->id]) ?>
                <?= $this->Html->link(__('Edit'), ['action' => 'edit', $menuModul->id]) ?>
                <?= $this->Form->postLink(__('Delete'), ['action' => 'delete', $menuModul->id], ['confirm' => __('Are you sure you want to delete # {0}?', $menuModul->id)]) ?>
            </td>
        </tr>

    <?php endforeach; ?>
    </tbody>
    </table>
    <div class="paginator">
        <ul class="pagination">
            <?= $this->Paginator->prev('< ' . __('previous')) ?>
            <?= $this->Paginator->numbers() ?>
            <?= $this->Paginator->next(__('next') . ' >') ?>
        </ul>
        <p><?= $this->Paginator->counter() ?></p>
    </div>
</div>
