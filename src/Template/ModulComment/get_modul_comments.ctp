<section class="panel default  h2">
    <div class="panel-heading blue_title">
        <i class="fa fa-comments"></i> Comment
        <a href="#"><span class="pull-right badge">2 Files</span></a>
    </div>
    <div class="panel-body">
        <div class="container">
            <div class="row">
                <div class="col l12 ">
                <?php foreach ($comments as $comment) {?>
                    <div class="media"> <a class="pull-left" href="#">
                        <img class="media-object" height="40" width="40" src="<?= $this->Encryptor->get_avatar($comment->user->email)?>" alt="gravatar">
                        </a>
                        <div class="media-body">
                            <span class="media-meta pull-right" data-livestamp="<?= strtotime($comment->created)?>" title="<?= $comment->created ?>"></span>
                            <h4 class="text-primary"><?= $comment->user->username?></h4>
                            <!-- <small class="text-muted">From: ramanip@riaxe.com</small> -->
                        </div>
                        <p><?=$comment->comment?></p>
                    </div>
                <?php }?>
                    <div class="media">
                        <a class="pull-left" href="#">
                        <img class="media-object" height="40" width="40" src="" alt="" id="avatar">
                        </a>
                        <div class="media-body">
                            <form action="" method="post" class="comment">
                                <textarea name="comment" rows="5" class="form-control"></textarea>
                                <!-- <input type="text" name="comment"> -->
                                <button class="btn btn-primary pull-right" type="submit">Send</button>
                            </form>
                        </div>
                    </div><!-- /media -->
                </div>
            </div>
                <br>
        </div>
    </div>
</section>
<script>
$(function () {
    $('form.comment').on('submit', function (e) {
    e.preventDefault();
    $.ajax({
        type: 'post',
        url: "<?= $this->Url->build(['controller'=>'modulComment', 'action'=>'add', $module_id]);?>",
        data: $('form.comment').serialize(),
        success: function (response) {
            toastr.success("Comment has been post");
            $("#module_comment").html(response);
        }
        });
        });
    });
</script>
