<div class="actions columns large-2 medium-3">
    <h3><?= __('Actions') ?></h3>
    <ul class="side-nav">
        <li><?= $this->Html->link(__('Edit Bugs Comment'), ['action' => 'edit', $bugsComment->id]) ?> </li>
        <li><?= $this->Form->postLink(__('Delete Bugs Comment'), ['action' => 'delete', $bugsComment->id], ['confirm' => __('Are you sure you want to delete # {0}?', $bugsComment->id)]) ?> </li>
        <li><?= $this->Html->link(__('List Bugs Comment'), ['action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Bugs Comment'), ['action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Bugs'), ['controller' => 'Bugs', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Bug'), ['controller' => 'Bugs', 'action' => 'add']) ?> </li>
    </ul>
</div>
<div class="bugsComment view large-10 medium-9 columns">
    <h2><?= h($bugsComment->id) ?></h2>
    <div class="row">
        <div class="large-5 columns strings">
            <h6 class="subheader"><?= __('Comment') ?></h6>
            <p><?= h($bugsComment->comment) ?></p>
            <h6 class="subheader"><?= __('Bug') ?></h6>
            <p><?= $bugsComment->has('bug') ? $this->Html->link($bugsComment->bug->id, ['controller' => 'Bugs', 'action' => 'view', $bugsComment->bug->id]) : '' ?></p>
        </div>
        <div class="large-2 columns numbers end">
            <h6 class="subheader"><?= __('Id') ?></h6>
            <p><?= $this->Number->format($bugsComment->id) ?></p>
            <h6 class="subheader"><?= __('Created By') ?></h6>
            <p><?= $this->Number->format($bugsComment->created_by) ?></p>
            <h6 class="subheader"><?= __('Modified By') ?></h6>
            <p><?= $this->Number->format($bugsComment->modified_by) ?></p>
        </div>
        <div class="large-2 columns dates end">
            <h6 class="subheader"><?= __('Created') ?></h6>
            <p><?= h($bugsComment->created) ?></p>
            <h6 class="subheader"><?= __('Modified') ?></h6>
            <p><?= h($bugsComment->modified) ?></p>
        </div>
    </div>
</div>
