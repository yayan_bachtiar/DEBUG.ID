<section class="panel default">
    <div class="panel-body">
        <div class="container">
            <div class="row">
                <div class="col l12 ">
                <div class="media">
                        <div class="media-body">
                            <form action="#" method="post" class="comment">
                                <textarea class="summernote" name="comments" rows="5" class="form-control"></textarea>
                                <button class="btn btn-primary pull-right" type="submit">Send</button>
                            </form>
                        </div>
                    </div>
                <?php foreach ($comments as $comment) {?>
                    <div class="media"> <a class="pull-left" href="#">
                        <img class="media-object" height="40" width="40" src="<?= $this->general->get_avatar($comment->user->email)?>" alt="gravatar">
                        </a>
                        <div class="media-body">
                            <span class="media-meta pull-right" data-livestamp="<?= strtotime($comment->created)?>" title="<?= $comment->created ?>"></span>
                            <h4 class="text-primary"><?= $comment->user->username?></h4>
                        </div>
                        <p><?=$comment->comments?></p>
                    </div>
                <?php }?>
                    
                </div>
            </div>
            <br>
        </div>
    </div>
</section>
<script>
$(function () {
    $(".summernote").summernote();
    $('form.comment').on('submit', function (e) {
    e.preventDefault();
    $.ajax({
        type: 'post',
        url: "<?= $this->Url->build(['controller'=>'bugsComment', 'action'=>'add', $bugs_id]);?>",
        data: $('form.comment').serialize(),
        success: function (response) {
            get_comment();
            get_comment_count();
        }
        });
        });
    });
</script>
