<div class="actions columns large-2 medium-3">
    <h3><?= __('Actions') ?></h3>
    <ul class="side-nav">
        <li><?= $this->Form->postLink(
                __('Delete'),
                ['action' => 'delete', $bugsComment->id],
                ['confirm' => __('Are you sure you want to delete # {0}?', $bugsComment->id)]
            )
        ?></li>
        <li><?= $this->Html->link(__('List Bugs Comment'), ['action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('List Bugs'), ['controller' => 'Bugs', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Bug'), ['controller' => 'Bugs', 'action' => 'add']) ?></li>
    </ul>
</div>
<div class="bugsComment form large-10 medium-9 columns">
    <?= $this->Form->create($bugsComment) ?>
    <fieldset>
        <legend><?= __('Edit Bugs Comment') ?></legend>
        <?php
            echo $this->Form->input('comment');
            echo $this->Form->input('bugs_id', ['options' => $bugs]);
            echo $this->Form->input('created_by');
            echo $this->Form->input('modified_by');
        ?>
    </fieldset>
    <?= $this->Form->button(__('Submit')) ?>
    <?= $this->Form->end() ?>
</div>
