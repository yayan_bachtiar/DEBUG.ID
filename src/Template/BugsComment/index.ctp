<div class="actions columns large-2 medium-3">
    <h3><?= __('Actions') ?></h3>
    <ul class="side-nav">
        <li><?= $this->Html->link(__('New Bugs Comment'), ['action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Bugs'), ['controller' => 'Bugs', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Bug'), ['controller' => 'Bugs', 'action' => 'add']) ?></li>
    </ul>
</div>
<div class="bugsComment index large-10 medium-9 columns">
    <table cellpadding="0" cellspacing="0">
    <thead>
        <tr>
            <th><?= $this->Paginator->sort('id') ?></th>
            <th><?= $this->Paginator->sort('comment') ?></th>
            <th><?= $this->Paginator->sort('bugs_id') ?></th>
            <th><?= $this->Paginator->sort('created') ?></th>
            <th><?= $this->Paginator->sort('created_by') ?></th>
            <th><?= $this->Paginator->sort('modified') ?></th>
            <th><?= $this->Paginator->sort('modified_by') ?></th>
            <th class="actions"><?= __('Actions') ?></th>
        </tr>
    </thead>
    <tbody>
    <?php foreach ($bugsComment as $bugsComment): ?>
        <tr>
            <td><?= $this->Number->format($bugsComment->id) ?></td>
            <td><?= h($bugsComment->comment) ?></td>
            <td>
                <?= $bugsComment->has('bug') ? $this->Html->link($bugsComment->bug->id, ['controller' => 'Bugs', 'action' => 'view', $bugsComment->bug->id]) : '' ?>
            </td>
            <td><?= h($bugsComment->created) ?></td>
            <td><?= $this->Number->format($bugsComment->created_by) ?></td>
            <td><?= h($bugsComment->modified) ?></td>
            <td><?= $this->Number->format($bugsComment->modified_by) ?></td>
            <td class="actions">
                <?= $this->Html->link(__('View'), ['action' => 'view', $bugsComment->id]) ?>
                <?= $this->Html->link(__('Edit'), ['action' => 'edit', $bugsComment->id]) ?>
                <?= $this->Form->postLink(__('Delete'), ['action' => 'delete', $bugsComment->id], ['confirm' => __('Are you sure you want to delete # {0}?', $bugsComment->id)]) ?>
            </td>
        </tr>

    <?php endforeach; ?>
    </tbody>
    </table>
    <div class="paginator">
        <ul class="pagination">
            <?= $this->Paginator->prev('< ' . __('previous')) ?>
            <?= $this->Paginator->numbers() ?>
            <?= $this->Paginator->next(__('next') . ' >') ?>
        </ul>
        <p><?= $this->Paginator->counter() ?></p>
    </div>
</div>
