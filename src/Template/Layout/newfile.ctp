<header>
            <nav class="top-nav">
                <div class="container">
                    <div class="nav-wrapper"><a class="page-title"><?= !empty($title)?$title : "";?></a></div>
                </div>
            </nav>
            <div class="container"><a href="#" data-activates="nav-mobile" class="button-collapse top-nav waves-effect waves-light circle hide-on-large-only"><i class="mdi-navigation-menu"></i></a></div>
            <ul id="nav-mobile" class="side-nav fixed">
                <li class="logo"><?= $this->Html->link($this->Html->image('material.png'), ['controller'=>'pages', 'action'=>'display'], ['id'=>'logo-container', 'class'=>'brand-logo', 'escape'=>false]);?>
                </li>
                <li class="bold"><?= $this->Html->link('Home', ['controller'=>'pages', 'action'=>'home'], ['class'=>'waves-effect wave-teal','escape'=>false]);?></li>
                <li class="bold"><?= $this->Html->link('My Porject', ['controller'=>'project', 'action'=>'index'], ['class'=>'waves-effect wave-teal', 'escape'=>false]);?></li>
                <li class="no-padding">
                    <ul class="collapsible collapsible-accordion">
                        <li class="bold"><a class="collapsible-header waves-effect waves-teal">Tasks</a>
                            <div class="collapsible-body">
                                <ul>
                                    <li><?=$this->Html->link('My Project',['controller'=>'my_project'], ['class'=>' waves-effect waves-teal'])?></li>
                                    <li><?=$this->Html->link('My Tasks',['controller'=>'my_task'], ['class'=>' waves-effect waves-teal'])?></li>
                                    <li><?=$this->Html->link('My Open Issues',['controller'=>'my_issue'], ['class'=>' waves-effect waves-teal'])?></li>
                                    <li><?=$this->Html->link('My Closed Issues',['controller'=>'my_closed_issue'], ['class'=>' waves-effect waves-teal'])?></li>
                                </ul>
                            </div>
                        </li>
                        <li class="bold"><a class="collapsible-header waves-effect waves-teal">Issues</a>
                            <div class="collapsible-body">
                                <ul>
                                    <li><?=$this->Html->link('My Issues',['controller'=>'bugs', 'action'=>'my_issue'], ['class'=>' waves-effect waves-teal'])?></li>
                                    <li><?=$this->Html->link('All Issues',['controller'=>'bugs', 'action'=>'index'], ['class'=>' waves-effect waves-teal'])?></li>
                                    <li><?=$this->Html->link('Open Issues',['controller'=>'bugs', 'action'=>'open_issue'], ['class'=>' waves-effect waves-teal'])?></li>
                                    <li><?=$this->Html->link('Close Issues',['controller'=>'bugs', 'action'=>'closed_issue'], ['class'=>' waves-effect waves-teal'])?></li>
                                </ul>
                            </div>
                        </li>
                        <li class="bold"><a class="collapsible-header  waves-effect waves-teal">Debuger</a>
                            <div class="collapsible-body">
                                <ul>
                                    <li><?=$this->Html->link('My Test', ['controller'=>'Bugs', 'action'=>'my_test'])?></li>
                                    <li><?=$this->Html->link('All Test', ['controller'=>'Bugs', 'action'=>'all_test'])?></li>
                                    <li><?=$this->Html->link('Resolved Bugs', ['controller'=>'Bugs', 'action'=>'resolved'])?></li>
                                    <li><?=$this->Html->link('Bugs Issue', ['controller'=>'Bugs', 'action'=>'bugs_issue'])?></li>
                                </ul>
                            </div>
                        </li>
                        <li class="bold"><a class="collapsible-header  waves-effect waves-teal">Account</a>
                            <div class="collapsible-body">
                                <ul>
                                    <li><?=$this->Html->link('My Account', ['controller'=>'user', 'action'=> 'view'], ['escape'=>false]);?></li>
                                    <li><?=$this->Html->link('Edit My Account', ['controller'=>'user', 'action'=> 'edit'], ['escape'=>false]);?></li>
                                    <li><?=$this->Html->link('My Password', ['controller'=>'user', 'action'=> 'edit_password'], ['escape'=>false]);?></li>
                                    <li><?=$this->Html->link('Log Out', ['controller'=>'user', 'action'=> 'logout'], ['escape'=>false]);?></li>
                                </ul>
                            </div>
                        </li>
                        <li class="bold"><a class="collapsible-header  waves-effect waves-teal">System</a>
                            <div class="collapsible-body">
                                <ul>
                                    <li><?=$this->Html->link('User', ['controller'=>'user', 'action'=> 'all_user'], ['escape'=>false]);?></li>
                                    <li><?=$this->Html->link('Role', ['controller'=>'Role', 'action'=> 'index'], ['escape'=>false]);?></li>
                                    <li><?=$this->Html->link('Status', ['controller'=>'status', 'action'=> 'index'], ['escape'=>false]);?></li>
                                </ul>
                            </div>
                        </li>
                    </ul>
                </li>
            </ul>
        </header>