<?php
/**
* CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
* Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
*
* Licensed under The MIT License
* For full copyright and license information, please see the LICENSE.txt
* Redistributions of files must retain the above copyright notice.
*
        * @copyright     Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
                * @link          http://cakephp.org CakePHP(tm) Project
                * @since         0.10.0
        * @license       http://www.opensource.org/licenses/mit-license.php MIT License
*/
?>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="msapplication-tap-highlight" content="no">
        <meta name="description" content="">
        <!-- Favicons-->
        <link rel="apple-touch-icon-precomposed" href="images/favicon/apple-touch-icon-152x152.png">
        <meta name="msapplication-TileColor" content="#FFFFFF">
        <meta name="msapplication-TileImage" content="images/favicon/mstile-144x144.png">
        <link rel="icon" href="img/favicon/favicon-32x32.png" sizes="32x32">
        <!--  Android 5 Chrome Color-->
        <meta name="theme-color" content="#EE6E73">
        <?= $this->Html->charset() ?>
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <title>
        <?= $title ?>
        </title>
        <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
        <?= $this->Html->css('font-awesome.css'); ?>
        <?= $this->Html->css('bootstrap.min.css'); ?>
        <?= $this->Html->css('admin.css'); ?>
        <?= $this->Html->css('animate.css'); ?>
        <?= $this->Html->css('jquerysctipttop.css') ?>
        <?= $this->Html->css('../plugins/kalendar/kalendar.css') ?>
        <?= $this->Html->css('../plugins/scroll/nanoscroller.css') ?>
        <?= $this->Html->css('toastr.css') ?>
        <?= $this->Html->css('index.css') ?>
        <?= $this->Html->script("jquery-2.1.0.js");?>
    </head>
    <body class="fixed_header left_nav_fixed atm-spmenu-push light_theme">
        <div class="wrapper">
            <!--\\\\\\\ wrapper Start \\\\\\-->
            <div class="header_bar">
                <!--\\\\\\\ header Start \\\\\\-->
                <div class="brand">
                    <!--\\\\\\\ brand Start \\\\\\-->
                    <div class="logo" style="display:block">
                        <span class="theme_color">ULTIMO</span> Admin
                    </div>
                    <div class="small_logo" style="display:none">
                        <img src="images/s-logo.png" width="50" height="47" alt="s-logo" />
                        <img src="images/r-logo.png" width="122" height="20" alt="r-logo" />
                    </div>
                </div>
                <!--\\\\\\\ brand end \\\\\\-->
                <div class="header_top_bar">
                    <!--\\\\\\\ header top bar start \\\\\\-->
                    <a href="javascript:void(0);" class="menutoggle"> <i class="fa fa-bars"></i> </a>
                    <a href="javascript:void(0);" class="add_user" data-toggle="modal" data-target="#myModal">
                    <i class="fa fa-plus-square"></i>
                    <span> New Task</span>
                    </a>
                    <div class="top_right_bar">
                        <div class="top_right">
                            <div class="top_right_menu">
                                <ul>
                                    <li class="dropdown">
                                        <a href="javascript:void(0);" data-toggle="dropdown">
                                        Tasks <span class="badge badge">8</span>
                                        </a>
                                        <ul class="drop_down_task dropdown-menu">
                                            <div class="top_pointer">
                                            </div>
                                            <li>
                                                <p class="number">You have 7 pending tasks</p>
                                            </li>
                                            <li>
                                                <a href="task.html" class="task">
                                                <div class="green_status task_height" style="width:80%;">
                                                </div>
                                                <div class="task_head">
                                                    <span class="pull-left">Task Heading</span>
                                                    <span class="pull-right green_label">80%</span>
                                                </div>
                                                <div class="task_detail">
                                                    Task details goes here
                                                </div>
                                                </a>
                                            </li>
                                            <li>
                                                <a href="task.html" class="task">
                                                <div class="yellow_status task_height" style="width:50%;">
                                                </div>
                                                <div class="task_head">
                                                    <span class="pull-left">Task Heading</span>
                                                    <span class="pull-right yellow_label">50%</span>
                                                </div>
                                                <div class="task_detail">
                                                Task details goes here</div>
                                                </a>
                                            </li>
                                            <li>
                                                <a href="task.html" class="task">
                                                <div class="blue_status task_height" style="width:70%;">
                                                </div>
                                                <div class="task_head">
                                                    <span class="pull-left">Task Heading</span>
                                                    <span class="pull-right blue_label">70%
                                                    </span>
                                                </div>
                                                <div class="task_detail">
                                                    Task details goes here
                                                </div>
                                                </a>
                                            </li>
                                            <li>
                                                <a href="task.html" class="task">
                                                <div class="red_status task_height" style="width:85%;">
                                                </div>
                                                <div class="task_head">
                                                    <span class="pull-left">
                                                    Task Heading</span>
                                                    <span class="pull-right red_label">85%</span>
                                                </div>
                                                <div class="task_detail">
                                                    Task details goes here
                                                </div>
                                                </a>
                                            </li>
                                            <li>
                                                <span class="new">
                                                <a href="task.html" class="pull_left">Create New</a>
                                                <a href="task.html" class="pull-right">
                                                View All</a>
                                                </span>
                                            </li>
                                        </ul>
                                    </li>
                                    <li class="dropdown">
                                        <a href="javascript:void(0);" data-toggle="dropdown">
                                        Mail
                                        <span class="badge badge color_1">4</span>
                                        </a>
                                        <ul class="drop_down_task dropdown-menu">
                                            <div class="top_pointer"></div>
                                            <li>
                                                <p class="number">You have 4 mails</p>
                                            </li>
                                            <li>
                                                <a href="readmail.html" class="mail">
                                                <span class="photo">
                                                <img src="images/user.png" />
                                                </span>
                                                <span class="subject">
                                                <span class="from">sarat m</span>
                                                <span class="time">just now</span>
                                                </span>
                                                <span class="message">Hello,this is an example msg.</span>
                                                </a>
                                            </li>
                                            <li>
                                                <a href="readmail.html" class="mail">
                                                <span class="photo">
                                                <img src="images/user.png" />
                                                </span>
                                                <span class="subject">
                                                <span class="from">sarat m</span>
                                                <span class="time">just now</span>
                                                </span>
                                                <span class="message">Hello,this is an example msg.</span>
                                                </a>
                                            </li>
                                            <li>
                                                <a href="readmail.html" class="mail red_color">
                                                <span class="photo">
                                                <img src="images/user.png" />
                                                </span>
                                                <span class="subject">
                                                <span class="from">sarat m</span>
                                                <span class="time">just now</span>
                                                </span>
                                                <span class="message">Hello,this is an example msg.</span>
                                                </a>
                                            </li>
                                            <li>
                                                <a href="readmail.html" class="mail">
                                                <span class="photo">
                                                <img src="images/user.png" />
                                                </span>
                                                <span class="subject">
                                                <span class="from">sarat m</span>
                                                <span class="time">just now</span>
                                                </span>
                                                <span class="message">Hello,this is an example msg.</span>
                                                </a>
                                            </li>
                                        </ul>
                                    </li>
                                    <li class="dropdown">
                                        <a href="javascript:void(0);" data-toggle="dropdown"> notification
                                        <span class="badge badge color_2">6</span>
                                        </a>
                                        <div class="notification_drop_down dropdown-menu">
                                            <div class="top_pointer"></div>
                                            <div class="box">
                                                <a href="inbox.html">
                                                <span class="block primery_6">
                                                <i class="fa fa-envelope-o"></i>
                                                </span>
                                                <span class="block_text">Mailbox</span>
                                                </a>
                                            </div>
                                            <div class="box">
                                                <a href="calendar.html">
                                                <span class="block primery_2">
                                                <i class="fa fa-calendar-o"></i>
                                                </span>
                                                <span class="block_text">Calendar</span>
                                                </a>
                                            </div>
                                            <div class="box">
                                                <a href="maps.html">
                                                <span class="block primery_4">
                                                <i class="fa fa-map-marker"></i>
                                                </span>
                                                <span class="block_text">Map</span>
                                                </a>
                                            </div>
                                            <div class="box">
                                                <a href="todo.html">
                                                <span class="block primery_3">
                                                <i class="fa fa-plane"></i>
                                                </span>
                                                <span class="block_text">To-Do</span>
                                                </a>
                                            </div>
                                            <div class="box">
                                                <a href="task.html">
                                                <span class="block primery_5">
                                                <i class="fa fa-picture-o"></i>
                                                </span>
                                                <span class="block_text">Tasks</span>
                                                </a>
                                            </div>
                                            <div class="box">
                                                <a href="timeline.html">
                                                <span class="block primery_1">
                                                <i class="fa fa-clock-o"></i>
                                                </span>
                                                <span class="block_text">Timeline</span>
                                                </a>
                                            </div>
                                        </div>
                                    </li>
                                </ul>
                            </div>
                        </div>
                        <div class="user_admin dropdown">
                            <a href="javascript:void(0);" data-toggle="dropdown">
                            <img src="" alt="" id="gravatar" width="40" height="40">
                            <span class="user_adminname"></span>
                            <b class="caret"></b>
                            </a>
                            <ul class="dropdown-menu">
                                <div class="top_pointer"></div>
                                <li><?= $this->Html->link('<i class="fa fa-user"></i> Profile', ['controller'=>'profile'], ['escape'=>false]);?></li>
                                <li><?= $this->Html->link('<i class="fa fa-question-circle"></i> Help', ['controller'=>'help'], ['escape'=>false]);?></li>
                                <li><?= $this->Html->link('<i class="fa fa-cog"></i> Setting', ['controller'=>'settings'], ['escape'=>false]);?></li>
                                <li><?= $this->Html->link('<i class="fa fa-power-off"></i> Logout', ['controller'=>'logout'], ['escape'=>false]);?></li>
                            </ul>
                        </div>
                        <a href="javascript:;" class="toggle-menu menu-right push-body jPushMenuBtn rightbar-switch">
                        <i class="fa fa-comment chat"></i>
                        </a>
                    </div>
                </div>
                <!--\\\\\\\ header top bar end \\\\\\-->
            </div>
            <!--\\\\\\\ header end \\\\\\-->
            <div class="inner">
                <!--\\\\\\\ inner start \\\\\\-->
                <div class="left_nav">
                    <!--\\\\\\\left_nav start \\\\\\-->
                    <div class="search_bar">
                        <i class="fa fa-search"></i>
                        <input name="" type="text" class="search" placeholder="Search Dashboard..." />
                    </div>
                    <div class="left_nav_slidebar" id="menu">
                    </div>
                </div>
                <!--\\\\\\\left_nav end \\\\\\-->
                <div class="contentpanel">
                    <!--\\\\\\\ contentpanel start\\\\\\-->
                    <div class="pull-left breadcrumb_admin clear_both">
                        <div class="pull-left page_title theme_color">
                            <h1>Dashboard</h1>
                            <h2 class="">Subtitle goes here...</h2>
                        </div>
                        <div class="pull-right">
                            <ol class="breadcrumb">
                                <li><a href="#">Home</a></li>
                                <li><a href="#">DASHBOARD</a></li>
                                <li class="active">Dashboard</li>
                            </ol>
                        </div>
                    </div>
                    <div class="container clear_both padding_fix" id="contentpanel">
                        <?= $this->fetch('content') ?>
                    </div>
                    <!--\\\\\\\ container  end \\\\\\-->
                </div>
                <!--\\\\\\\ content panel end \\\\\\-->
            </div>
            <!--\\\\\\\ inner end\\\\\\-->
        </div>
        <!--
        <script id="cid0020000102932491550" data-cfasync="false" async src="//st.chatango.com/js/gz/emb.js" style="width: 344px;height: 322px;">{"handle":"xstech","arch":"js","styles":{"a":"0084ef","b":100,"c":"FFFFFF","d":"FFFFFF","k":"0084ef","l":"0084ef","m":"0084ef","n":"FFFFFF","p":"10","q":"0084ef","r":100,"pos":"br","cv":1,"cvfntw":"bold","cvbg":"3366ff","cvbga":76,"cvw":800,"cvh":30,"ticker":1,"fwtickm":1}}</script>-->
        <!--\\\\\\\ wrapper end\\\\\\-->
        <!-- Modal -->
        <!-- sidebar chats -->
        <nav class="atm-spmenu atm-spmenu-vertical atm-spmenu-right side-chat">
            <div id="tlkio" data-channel="xstech" style="width:100%; height:100%">
            </div>
            <script async src="http://tlk.io/embed.js" type="text/javascript"></script>
        </nav>
        <!-- /sidebar chats -->
        <!-- sidebar chats -->
        <!-- /sidebar chats -->
        <?= $this->Html->script("bootstrap.min.js");?>
        <?= $this->Html->script("app.js")?>
        <?= $this->Html->script("pace.js")?>
        <?= $this->Html->script("moment.min.js")?>
        <?= $this->Html->script("livestamp.min.js")?>
        <?= $this->Html->script("toastr.min.js");?>
        <?php echo $this->Html->script("common-script.js");?>
        <?= $this->Html->script("jquery.slimscroll.min.js");?>
        <?= $this->Html->script("jquery.sparkline.js");?>
        <?= $this->Html->script("sparkline-chart.js");?>
        <?= $this->Html->script("graph.js");?>
        <?= $this->Html->script("toastr.js");?>
        <?= $this->Html->script("parsley.min.js");?>
        <?= $this->Html->script("../plugins/bootstrap-editable/bootstrap-editable.min.js");?>
        <?= $this->Html->script("../plugins/mockjax/jquery.mockjax.min.js");?>
        <?= $this->Html->script("../plugins/kalendar/kalendar.js");?>
        <?= $this->Html->script("../plugins/kalendar/edit-kalendar.js");?>
        <?= $this->Html->script("../plugins/sparkline/jquery.sparkline.js");?>
        <?= $this->Html->script("../plugins/sparkline/jquery.customSelect.min.js");?>
        <?= $this->Html->script("../plugins/sparkline/sparkline-chart.js");?>
        <?php //echo  $this->Html->script("../plugins/sparkline/easy-pie-chart.js");?>
        <?php //echo $this->Html->script("../plugins/morris/morris.min.js");?>
        <?php //echo $this->Html->script("../plugins/morris/morris-script.js");?>
        <?php //echo $this->Html->script("../plugins/demo-slider/demo-slider.js");?>
        <?php //echo $this->Html->script("../plugins/knob/jquery.knob.min.js");?>
        <?= $this->Html->script("jPushMenu.js");?>
        <?= $this->Html->script("side-chats.js");?>
        <?= $this->Html->script("jquery.slimscroll.min.js");?>
        <?= $this->Html->script("../plugins/scroll/jquery.nanoscroller.js");?>
    </body>
    <script type="text/javascript">
        $("a#ajax").bind("click", function (event) {
            var href = $(this).attr('href');
            $.ajax({
            success:function (response) {
                $("#contentpanel").html(response);
            },
            url: href,
            error:(function(response){
                location.reload();
                }),
            });
            return false;
        });
    </script>
</html>