<?php
/**
* CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
* Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
*
* Licensed under The MIT License
* For full copyright and license information, please see the LICENSE.txt
* Redistributions of files must retain the above copyright notice.
*
* @copyright     Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
* @link          http://cakephp.org CakePHP(tm) Project
* @since         0.10.0
* @license       http://www.opensource.org/licenses/mit-license.php MIT License
*/
$cakeDescription = 'CakePHP: the rapid development php framework';
?>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="msapplication-tap-highlight" content="no">
        <meta name="description" content="">
        <!-- Favicons-->
        <link rel="apple-touch-icon-precomposed" href="images/favicon/apple-touch-icon-152x152.png">
        <meta name="msapplication-TileColor" content="#FFFFFF">
        <meta name="msapplication-TileImage" content="images/favicon/mstile-144x144.png">
        <link rel="icon" href="img/favicon/favicon-32x32.png" sizes="32x32">
        <!--  Android 5 Chrome Color-->
        <meta name="theme-color" content="#EE6E73">
        <?= $this->Html->charset() ?>
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <title>
        <?= "500 Internal Server Error" ?>
        </title>
        <?= $this->Html->css('font-awesome.css'); ?>
        <?= $this->Html->css('bootstrap.min.css'); ?>
        <?= $this->Html->css('animate.css'); ?>
        <?= $this->Html->css('admin.css') ?>
        <?php // $this->Html->css('prism.css') ?>
        <?= $this->Html->css('page-center.css') ?>
        <style>
        body {
        display: flex;
        min-height: 100vh;
        flex-direction: column;
        }
        main {
        flex: 1 0 auto;
        }
        </style>
    </head>
    <body class="light_theme  fixed_header left_nav_fixed">
        <div class="wrapper">
            <!--\\\\\\\ wrapper Start \\\\\\-->
            <div class="login_page">
                <div class="login_content">
                    <div class="panel-heading border login_heading">sign in now</div>
                    <?= $this->fetch('content') ?>
                </div>
            </div>
        </div>
        <?= $this->Html->script('jquery-2.1.0.js');?>
        <?= $this->Html->script('bootstrap.min.js');?>
        <?= $this->Html->script('common-script.js');?>
        <?= $this->Html->script('materialize.js');?>
        <?= $this->Html->script('jquery.slimscroll.min.js');?>
    </body>
</html>
