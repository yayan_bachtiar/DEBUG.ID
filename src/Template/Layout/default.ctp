<!DOCTYPE html>
<html lang="en">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <!-- Meta, title, CSS, favicons, etc. -->
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>Gentallela Alela! | </title>
        <!-- Bootstrap core CSS -->
        <?= $this->Html->css("bootstrap.min.css");?>
        <?= $this->Html->css("../fonts/css/font-awesome.min.css");?>
        <?= $this->Html->css("animate.min.css");?>
        <!-- Custom styling plus plugins -->
        <?= $this->Html->css("custom.css");?>
        <?= $this->Html->css("maps/jquery-jvectormap-2.0.1.css");?>
        <?= $this->Html->css("icheck/flat/green.css");?>
        <?= $this->Html->css("floatexamples.css");?>
        <?= $this->Html->css("toastr.css");?>
        <?= $this->Html->css("pace.css");?>
        <?= $this->Html->css("jsq/jquery-confirm.min.css");?>
        <?= $this->Html->script("jquery.min.js");?>


        <!--[if lt IE 9]>
        <script src="../assets/js/ie8-responsive-file-warning.js"></script>
        <![endif]-->
        <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
        <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
        <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
        <![endif]-->
    </head>
    <body class="nav-md">
        <div class="container body">
            <div class="main_container">
                <div class="col-md-3 left_col">
                    <div class="left_col scroll-view">
                        <div class="navbar nav_title" style="border: 0;">
                            <a href="<?= $this->Url->build(['controller'=>'pages', 'action'=>'home'])?>" class="site_title"><i class="fa fa-paw"></i> <span>Gentellela Alela!</span></a>
                        </div>
                        <div class="clearfix"></div>
                        <!-- menu prile quick info -->
                        <div class="profile">
                            <div class="profile_pic">
                                <img src="<?=$this->General->get_avatar( $this->request->session()->read('Auth.User.email'))?>" alt="..." class="img-circle profile_img">
                            </div>
                            <div class="profile_info">
                                <span>Welcome,</span>
                                <h2><?=$this->request->session()->read('Auth.User.firstname')?></h2>
                            </div>
                        </div>
                        <!-- /menu prile quick info -->
                        <br />
                        <br />
                        <!-- sidebar menu -->
                        <div id="sidebar-menu" class="main_menu_side hidden-print main_menu">
                        </div>
                        <!-- /sidebar menu -->
                        <!-- /menu footer buttons -->
                        <div class="sidebar-footer hidden-small">
                            <a data-toggle="tooltip" data-placement="top" title="Settings">
                            <span class="glyphicon glyphicon-cog" aria-hidden="true"></span>
                            </a>
                            <a data-toggle="tooltip" data-placement="top" title="FullScreen">
                            <span class="glyphicon glyphicon-fullscreen" aria-hidden="true"></span>
                            </a>
                            <a data-toggle="tooltip" data-placement="top" title="Lock">
                            <span class="glyphicon glyphicon-eye-close" aria-hidden="true"></span>
                            </a>
                            <?= $this->Html->link('<span class="glyphicon glyphicon-off" aria-hidden="true"></span>', ['controller'=>'user', 'action'=>'logout'], ['data-toggle'=>'tooltip', 'data-placement'=>'top', 'escape'=>false, 'title'=>'Logout']);?>
                        </div>
                        <!-- /menu footer buttons -->
                    </div>
                </div>
                <!-- top navigation -->
                <div class="top_nav">
                    <div class="nav_menu">
                        <nav class="" role="navigation">
                            <div class="nav toggle">
                                <a id="menu_toggle"><i class="fa fa-bars"></i></a>
                            </div>
                            <ul class="nav navbar-nav navbar-right">
                                <li class="">
                                    <a href="javascript:;" class="user-profile dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
                                    <img src="<?=$this->General->get_avatar( $this->request->session()->read('Auth.User.email'))?>" alt=""><?=$this->request->session()->read('Auth.User.firstname')?>
                                    <span class=" fa fa-angle-down"></span>
                                    </a>
                                    <ul class="dropdown-menu dropdown-usermenu animated fadeInDown pull-right">
                                        <li><?= $this->Html->link('Profile', ['controller'=>'user', 'action'=>'index'])?>
                                        </li>
                                        <li>
                                            <a href="javascript:;">
                                            <span>Settings</span>
                                            </a>
                                        </li>
                                        <li>
                                            <a href="javascript:;">Help</a>
                                        </li>
                                        <li><?= $this->Html->link('<i class="fa fa-sign-out pull-right"></i> Log Out', ['controller'=>'logout'], ['escape'=>false]);?>
                                        </li>
                                    </ul>
                                </li>
                                <li role="presentation" class="dropdown">
                                    <a href="javascript:;" class="dropdown-toggle info-number" data-toggle="dropdown" aria-expanded="false">
                                    <i class="fa fa-envelope-o"></i>
                                    <span class="badge bg-green">6</span>
                                    </a>
                                    <ul id="menu1" class="dropdown-menu list-unstyled msg_list animated fadeInDown" role="menu">
                                        <li>
                                            <a>
                                            <span class="image">
                                            <img src="images/img.jpg" alt="Profile Image" />
                                            </span>
                                            <span>
                                            <span>John Smith</span>
                                            <span class="time">3 mins ago</span>
                                            </span>
                                            <span class="message">
                                            Film festivals used to be do-or-die moments for movie makers. They were where...
                                            </span>
                                            </a>
                                        </li>
                                        <li>
                                            <a>
                                            <span class="image">
                                            <img src="images/img.jpg" alt="Profile Image" />
                                            </span>
                                            <span>
                                            <span>John Smith</span>
                                            <span class="time">3 mins ago</span>
                                            </span>
                                            <span class="message">
                                            Film festivals used to be do-or-die moments for movie makers. They were where...
                                            </span>
                                            </a>
                                        </li>
                                        <li>
                                            <a>
                                            <span class="image">
                                            <img src="images/img.jpg" alt="Profile Image" />
                                            </span>
                                            <span>
                                            <span>John Smith</span>
                                            <span class="time">3 mins ago</span>
                                            </span>
                                            <span class="message">
                                            Film festivals used to be do-or-die moments for movie makers. They were where...
                                            </span>
                                            </a>
                                        </li>
                                        <li>
                                            <a>
                                            <span class="image">
                                            <img src="images/img.jpg" alt="Profile Image" />
                                            </span>
                                            <span>
                                            <span>John Smith</span>
                                            <span class="time">3 mins ago</span>
                                            </span>
                                            <span class="message">
                                            Film festivals used to be do-or-die moments for movie makers. They were where...
                                            </span>
                                            </a>
                                        </li>
                                        <li>
                                            <div class="text-center">
                                                <a>
                                                <strong><a href="inbox.html">See All Alerts</strong>
                                                <i class="fa fa-angle-right"></i>
                                                </a>
                                            </div>
                                        </li>
                                    </ul>
                                </li>
                            </ul>
                        </nav>
                    </div>
                </div>
                <!-- /top navigation -->
                <!-- page content -->
                <div class="right_col" role="main" id="content">
                    <!-- content -->
                    <?= $this->fetch('content') ?>
                    <!-- footer content -->
                    <footer>
                        <div class="">
                            <p class="pull-right">Gentelella Alela! a Bootstrap 3 template by <a>Kimlabs</a>. |
                            <span class="lead"> <i class="fa fa-paw"></i> Gentelella Alela!</span>
                            </p>
                        </div>
                        <div class="clearfix"></div>
                    </footer>
                    <!-- /footer content -->
                </div>
                <!-- /page content -->
            </div>
        </div>
        <div id="custom_notifications" class="custom-notifications dsp_none">
            <ul class="list-unstyled notifications clearfix" data-tabbed_notifications="notif-group">
            </ul>
            <div class="clearfix"></div>
            <div id="notif-group" class="tabbed_notifications"></div>
        </div>
        <?= $this->Html->script("bootstrap.min.js");?>
        <!-- bootstrap progress js -->
        <?= $this->Html->script("nicescroll/jquery.nicescroll.min.js");?>
        <!-- daterangepicker -->
        <?= $this->Html->script("moment.min.js");?>
        <?= $this->Html->script("datepicker/daterangepicker.js");?>
        <!-- livestamp -->
        <?= $this->Html->script("livestamp.min.js")?>

        <?= $this->Html->script("custom.js");?>
        <?= $this->Html->script("pace.js");?>
        <?= $this->Html->script("toastr/toastr.js");?>
        <?= $this->Html->script("jsq/jquery-confirm.min.js");?>
        <!-- datepicker -->
        <script>
            $.ajax({
                url: '<?= $this->Url->build(["controller"=>"menu", "action"=>"get_menu"]);?>',
                type: 'GET',
                // dataType: 'default: Intelligent Guess (Other values: xml, json, script, or html)',
                // data: {param1: 'value1'},
            }).done(function(response) {
                $("#sidebar-menu").html(response);
            });
        </script>
        <!-- /datepicker -->
        <!-- /footer content -->
    </body>
</html>