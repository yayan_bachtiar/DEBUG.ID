<ul>
    <li class="left_nav_active theme_border">
        <a href="javascript:void(0)">
            {{icon}}
            {{title}}
            <span class="left_nav_pointer">
            </span>
            <span class="plus">
                <i class="fa fa-plus"></i>
            </span>
        </a>
        <ul class="opened" style="display:block">
            <li>
                <a href="{{URL}}">
                    <span>&nbsp;</span>
                    <i class="fa fa-circle theme_color"></i>
                    <b class="theme_color">Dashboard</b>
                </a>
            </li>
            <li>
                <a href="{{URL}}">
                    <span>&nbsp;</span>
                    <i class="fa fa-circle theme_color"></i>
                    <b class="">Dashboard</b>
                </a>
            </li>
            <li>
                <a href="{{URL}}">
                    <span>&nbsp;</span>
                    <i class="fa fa-circle theme_color"></i>
                    <b class="">Dashboard</b>
                </a>
            </li>
        </ul>
    </li>
    <li>
        <a href="javascript:void(0)">
            {{icon}}
            {{title}}
            <span class="">
            </span>
            <span class="plus">
                <i class="fa fa-plus"></i>
            </span>
        </a>
        <ul>
            <li>
                <a href="{{URL}}">
                    <span>&nbsp;</span>
                    <i class="fa fa-circle theme_color"></i>
                    <b class="theme_color">Dashboard</b>
                </a>
            </li>
            <li>
                <a href="{{URL}}">
                    <span>&nbsp;</span>
                    <i class="fa fa-circle theme_color"></i>
                    <b class="">Dashboard</b>
                </a>
            </li>
            <li>
                <a href="{{URL}}">
                    <span>&nbsp;</span>
                    <i class="fa fa-circle theme_color"></i>
                    <b class="">Dashboard</b>
                </a>
            </li>
        </ul>
    </li>
</ul>
