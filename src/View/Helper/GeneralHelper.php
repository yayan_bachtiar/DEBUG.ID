<?php
namespace App\View\Helper;

use Cake\View\Helper;

class GeneralHelper extends Helper
{

    public function encryptor($action, $string) {
        $output = false;

        $encrypt_method = "AES-256-CBC";
        //pls set your unique hashing key
        $secret_key = 'xsisthebest';
        $secret_iv = 'byyayanbachtiar';

        // hash
        $key = hash('sha256', $secret_key);

        // iv - encrypt method AES-256-CBC expects 16 bytes - else you will get a warning
        $iv = substr(hash('sha256', $secret_iv), 0, 16);

        //do the encyption given text/string/number
        if( $action == 'encrypt' ) {
            $output = openssl_encrypt($string, $encrypt_method, $key, 0, $iv);
            $output = base64_encode($output);
        }
        else if( $action == 'decrypt' ){
            //decrypt the given text/string/number
            $output = openssl_decrypt(base64_decode($string), $encrypt_method, $key, 0, $iv);
        }

        return $output;
    }

    public function get_avatar($email){
        $grav_url = "http://www.gravatar.com/avatar/" . md5( strtolower( trim( $email ) ) );
        return $grav_url;
    }
}
