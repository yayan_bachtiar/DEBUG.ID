<?php
namespace App\Controller;

use App\Controller\AppController;

/**
 * Menu Controller
 *
 * @property \App\Model\Table\MenuTable $Menu
 */
class MenuController extends AppController
{
    
    /**
     * Index method
     *
     * @return void
     */
    public function initialize() {
        parent::initialize();
    }
    public function index() {
        $this->paginate = ['contain' => ['menuModul']];
        $this->set('menu', $this->paginate($this->Menu));
        $this->set('_serialize', ['menu']);
    }
    
    /**
     * View method
     *
     * @param string|null $id Menu id.
     * @return void
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function view($id = null) {
        $menu = $this->Menu->get($id, ['contain' => ['Role']]);
        $this->set('menu', $menu);
        $this->set('_serialize', ['menu']);
    }
    
    /**
     * Add method
     *
     * @return void Redirects on successful add, renders view otherwise.
     */
    public function add() {
        $menu = $this->Menu->newEntity();
        if ($this->request->is('post')) {
            $menu = $this->Menu->patchEntity($menu, $this->request->data);
            if ($this->Menu->save($menu)) {
                $this->Flash->success(__('The menu has been saved.'));
                return $this->redirect(['action' => 'index']);
            } 
            else {
                $this->Flash->error(__('The menu could not be saved. Please, try again.'));
            }
        }
        $role = $this->Menu->Role->find('list', ['limit' => 200]);
        $this->set(compact('menu', 'role'));
        $this->set('_serialize', ['menu']);
    }
    
    /**
     * Edit method
     *
     * @param string|null $id Menu id.
     * @return void Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null) {
        $menu = $this->Menu->get($id, ['contain' => ['Role']]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $menu = $this->Menu->patchEntity($menu, $this->request->data);
            if ($this->Menu->save($menu)) {
                $this->Flash->success(__('The menu has been saved.'));
                return $this->redirect(['action' => 'index']);
            } 
            else {
                $this->Flash->error(__('The menu could not be saved. Please, try again.'));
            }
        }
        $role = $this->Menu->Role->find('list', ['limit' => 200]);
        $this->set(compact('menu', 'role'));
        $this->set('_serialize', ['menu']);
    }
    
    /**
     * Delete method
     *
     * @param string|null $id Menu id.
     * @return void Redirects to index.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function delete($id = null) {
        $this->request->allowMethod(['post', 'delete']);
        $menu = $this->Menu->get($id);
        if ($this->Menu->delete($menu)) {
            $this->Flash->success(__('The menu has been deleted.'));
        } 
        else {
            $this->Flash->error(__('The menu could not be deleted. Please, try again.'));
        }
        return $this->redirect(['action' => 'index']);
    }
    
    public function get_menu() {
        if ($this->request->is('ajax')) {
            $this->viewBuilder()->layout('ajax');
        }
        $this->set('menu', $this->Menu->find('all', ['contain' => ['MenuModul']]));
    }
}
