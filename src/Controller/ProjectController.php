<?php
namespace App\Controller;

use App\Controller\AppController;
use App\Controller\ModuleController;
use Cake\Utility\Security;

/**
 * Project Controller
 *
 * @property \App\Model\Table\ProjectTable $Project
 */
class ProjectController extends AppController
{
    public $helpers = ['General'];
    public $components = ['RequestHandler', 'Paginator'];
    
    public function initialize() {
        parent::initialize();
        $session = $this->request->session();
    }
    
    /**
     * [load_data description]
     * @return [type] [description]
     */
    public function load_data() {
        $this->viewBuilder()->layout('ajax');
        $id = $this->request->data['id'];
        $project = $this->Project->get($id);
        $this->set(compact('project'));
    }
    
    /**
     * Index method
     *
     * @return void
     */
    public $paginate = ['limit' => 4];
    
    public function index() {
        
        // $data = $this->Project->find()->leftJoinWith('ProjectParticipant.Users')
        // ->group(['Project.id']);
        $data = $this->Project->find('all', ['contain' => ['ProjectParticipant' => ['Users']]]);
        $this->set('title', 'List of Project');
        $this->set('project', $this->paginate($data));
        $this->set('_serialize', ['project']);
    }
    
    public function get_statistic() {
        $this->autoRender = false;
        $id = $_GET['id'];
        
        //get ready
        $finished = $this->Project->Module->Task->find()->contain(['Module'])->where(['Module.project_id' => $id, 'Task.status' => 4, 'Task.is_deleted' => 0])->count();
        $open = $this->Project->Module->Task->find()->contain(['Module'])->where(['Module.project_id' => $id, 'Task.status' => 7, 'Task.is_deleted' => 0])->count();
        $total = $this->Project->Module->Task->find()->contain(['Module'])->where(['Module.project_id' => $id, 'Task.is_deleted' => 0])->count();
        $percent = 0;
        if ($total == null) {
            $percent = 0;
        }
        if ($finished != 0) {
            $percent = $finished / $total;
        }
        $bug_faild = $this->Project->Module->Bugs->find()->contain(['Module'])->where(['Module.project_id' => $id, 'Bugs.status' => 1, 'Bugs.is_deleted' => 0])->count();
        $bug_pass = $this->Project->Module->Bugs->find()->contain(['Module'])->where(['Module.project_id' => $id, 'Bugs.status' => 3, 'Bugs.is_deleted' => 0])->count();
        $bug_ready = $this->Project->Module->Bugs->find()->contain(['Module'])->where(['Module.project_id' => $id, 'Bugs.status' => 4, 'Bugs.is_deleted' => 0])->count();
        $all_bug = $this->Project->Module->Bugs->find()->contain(['Module'])->where(['Module.project_id' => $id, 'Bugs.is_deleted' => 0])->count();
        if ($all_bug == null) {
            $percent2 = 0;
        }
        if ($all_bug != 0) {
            $percent2 = ($bug_pass + $bug_ready) / $all_bug;
        }
        $percent2 = (($percent + $percent2) / 2) * 100;
        $data['percent'] = number_format($percent2, 2, '.', '');
        $data['finished'] = $finished;
        $data['total'] = $total;
        $data['all_bug'] = $all_bug;
        $data['bug_ready'] = $bug_ready;
        $data['bug_pass'] = $bug_pass;
        $data['bug_faild'] = $bug_faild;
        echo json_encode($data);
    }
    
    /**
     * View method
     *
     * @param string|null $id Project id.
     * @return void
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function view($id = null) {
        $this->set(compact('id'));
        $project = $this->Project->get($id, ['contain' => ['Users', 'ProjectParticipant' => ['Users'], 'ProjectAttachment']]);
        $this->set('_serialize', ['project']);
        $this->set('project', $project);
        $this->set('title', $project->project_name);
    }
    
    public function get_module($id) {
        $this->loadModel('Module');
        $project = $this->Project->get($id, ['field' => ['project_desc', 'project_name']]);
        
        $status = $_GET['status'];
        $this->paginate = ['limit' => 5, 'contain' => ['ModuleParticipant' => ['Users']], 'conditions' => ['Module.project_id' => $id, 'Module.status' => $status]];
        $module = $this->paginate($this->Module);
        
        $this->set(compact('module', 'status', 'project'));
        
        $this->render('load_module');;
    }
    
    /**
     * Add method
     *
     * @return void Redirects on successful add, renders view otherwise.
     */
    public function add() {
        $project = $this->Project->newEntity();
        $file_path = WWW_ROOT . 'img/';
        if ($this->request->is('post')) {
            $project = $this->Project->patchEntity($project, $this->request->data);
            $date = explode(' - ', $this->request->data('reservation'));
            $project->project_start = $date[0];
            $project->project_end = $date[1];
            $uid = $this->Auth->User('id');
            $this->request->data['created_by'] = $uid;
            
            $this->request->data['project_owner'] = $this->request->data('project_owner');
            if (!empty($_FILE['project_image'])) {
                move_uploaded_file($this->request->data['project_image']['tmp_name'], $file_path . $this->request->data['project_image']['name']);
                $this->request->data['project_image'] = $this->request->data['project_image']['name'];
            }
            
            $result = $this->Project->save($project);
            if ($result) {
                $username = $this->Auth->User('username');
                $related_link = json_encode($project);
                $pid =  $result->id;
                $contributor = $this->request->data('contributor');
                if (!empty($contributor)) {
                    $this->loadModel('ProjectParticipant');
                    foreach ($contributor as $key) {
                        $query = $this->ProjectParticipant->query();        
                        $query->insert(['project_id', 'user_id'])->values(['project_id' => $pid, 'user_id' => $key])->execute();
                    }
                }
                $this->write_log($result->id, "Project has been created by $username", "project_created", $related_link, null, null);
                return $this->redirect(['controller' => 'project', 'action' => 'view', $result->id]);
            } 
            else {
                $this->Flash->error('The project could not be saved. Please, try again.');
            }
        }
        
        $user = $this->Project->Users->find('all')->select(['id', 'username']);
        $title = "Build a project";
        $this->set(compact('project', 'title', 'user'));
    }
    
    public function get_project_list() {
        $this->paginate = ['contain' => ['Module']];
        $this->set('title', 'List of Project');
        $this->set('project', $this->paginate($this->Project));
        $this->set('_serialize', ['project']);
    }
    
    /**
     * Edit method
     *
     * @param string|null $id Project id.
     * @return void Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null) {
        $project = $this->Project->get($id, ['contain' => ['ProjectParticipant']]);
        $project->reservation = date('Y-m-d', strtotime($project->project_start)).' - '.date('Y-m-d', strtotime($project->project_end));
        $this->set('title', $project->project_name);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $project = $this->Project->patchEntity($project, $this->request->data);
            $date = explode(' - ', $this->request->data('reservation'));
            $project->project_start = $date[0];
            $project->project_end = $date[1];
            if ($this->Project->save($project)) {
                $username = $this->Auth->User('username');
                $related_link = json_encode($project);
                $pid =  $id;
                $contributor = $this->request->data('contributor');
                if (!empty($contributor)) {
                    $this->loadModel('ProjectParticipant');
                    $query = $this->ProjectParticipant->query();
                    $this->ProjectParticipant->deleteAll(['ProjectParticipant.project_id'=>$pid]);
                    foreach ($contributor as $key) {
                        $perintah = $query->insert(['project_id', 'user_id'])->values(['project_id' => $pid, 'user_id' => $key]);
                    }
                    $perintah->execute();
                }
                $related_link = json_encode($project);
                $this->write_log($id, "Project has been edit by $username", "project_edited", $related_link, null, null);
                return $this->redirect(['controller' => 'project', 'action' => 'view', $id]);
            } 
            else {
                $this->Flash->error('The project could not be saved. Please, try again.');
            }
        }
        $user = $this->Project->Users->find('all')->select(['id', 'username']);
        $title = 'Edit ';
        $this->set(compact('project', 'user', 'title'));
        $this->render('add');
        $this->set('_serialize', ['project']);
    }
    
    /**
     * Delete method
     *
     * @param string|null $id Project id.
     * @return void Redirects to index.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function delete($id = null) {
        $this->request->allowMethod(['post', 'delete']);
        $project = $this->Project->get($id);
        if ($this->Project->delete($project)) {
            $this->Flash->success('The project has been deleted.');
        } 
        else {
            $this->Flash->error('The project could not be deleted. Please, try again.');
        }
        return $this->redirect(['action' => 'index']);
    }
    
    public function my_project() {
        $this->loadModel('ProjectParticipant');
        $user_id = $this->Auth->User('id');
        $this->paginate = ['contain' => ['Project'], 'conditions' => ['ProjectParticipant.user_id' => $user_id], 'limit' => 5];
        
        // $this->paginate = ['conditions' => ['ProjectParticipant.user_id' => $user_id], 'limit'=> 5];
        $project = $this->paginate($this->ProjectParticipant);
        $title = 'My Project';
        $this->set(compact('project', 'title'));
    }
    
    public function open_project() {
        $pid = $_GET['pid'];
        $this->render('');
    }
    
    public function get_count() {
        $this->autoRender = false;
        $id = $_GET['id'];
        $data['modules'] = $this->Project->Module->find()->where(['Module.project_id' => $id, 'Module.is_deleted' => 0, 'Module.status' => 6])->count();
        $data['tasks'] = $this->Project->Module->Task->find('all', ['contain' => ['Module']])->where(['Module.project_id' => $id, 'Task.is_deleted' => 0, 'Task.status' => 1])->count();
        $data['bugs'] = $this->Project->Module->Bugs->find()->contain(['Module'])->where(['Module.project_id' => $id, 'Bugs.is_deleted' => 0, 'Bugs.status' => 9])->count();
        
        $data['modules_all'] = $this->Project->Module->find()->where(['Module.project_id' => $id, 'Module.is_deleted' => 0])->count();
        $data['tasks_all'] = $this->Project->Module->Task->find('all', ['contain' => ['Module']])->where(['Module.project_id' => $id, 'Task.is_deleted' => 0])->count();
        $data['bugs_all'] = $this->Project->Module->Bugs->find()->contain(['Module'])->where(['Module.project_id' => $id, 'Bugs.is_deleted' => 0])->count();
        
        $data['modules_open'] = $this->Project->Module->find()->where(['Module.project_id' => $id, 'Module.is_deleted' => 0, 'Module.status' => 9])->count();
        $data['tasks_ready'] = $this->Project->Module->Task->find('all', ['contain' => ['Module']])->where(['Module.project_id' => $id, 'Task.is_deleted' => 0, 'Task.status' => 4])->count();
        $data['bugs_tested'] = $this->Project->Module->Bugs->find()->contain(['Module'])->where(['Module.project_id' => $id, 'Bugs.is_deleted' => 0, 'Bugs.status' => 5])->count();
        echo json_encode($data);
    }
}
