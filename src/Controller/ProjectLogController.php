<?php
namespace App\Controller;

use App\Controller\AppController;

/**
 * ProjectLog Controller
 *
 * @property \App\Model\Table\ProjectLogTable $ProjectLog
 */
class ProjectLogController extends AppController
{
    
    /**
     * Index method
     *
     * @return void
     */
    public function initialize() {
        parent::initialize();
    }
    public function index() {
        $this->paginate = ['contain' => ['Projects', 'Modules', 'Bugs']];
        $this->set('projectLog', $this->paginate($this->ProjectLog));
        $this->set('_serialize', ['projectLog']);
    }
    
    /**
     * View method
     *
     * @param string|null $id Project Log id.
     * @return void
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function view($id = null) {
        $projectLog = $this->ProjectLog->get($id, ['contain' => ['Projects', 'Modules', 'Bugs']]);
        $this->set('projectLog', $projectLog);
        $this->set('_serialize', ['projectLog']);
    }
    
    public function get_log() {
        $log = null;
        if (!empty($_GET['bug_id'])) {
            $log = $this->ProjectLog->find()->contain(['Users'])->where(['bug_id' => $_GET['bug_id']])->order(['ProjectLog.created' => 'DESC']);
        }
        if (!empty($_GET['mod_id'])) {
            $log = $this->ProjectLog->find()->contain(['Users'])->where(['module_id' => $_GET['mod_id']])->order(['ProjectLog.created' => 'DESC']);
        }
        if (!empty($_GET['p_id'])) {
            $log = $this->ProjectLog->find()->contain(['Users'])->where(['project_id' => $_GET['p_id']])->order(['ProjectLog.created' => 'DESC']);
        }
        if (!empty($_GET['u_id'])) {
            $log = $this->ProjectLog->find()->contain(['Users'])->where(['ProjectLog.created_by' => $_GET['u_id']])->order(['ProjectLog.created' => 'DESC']);
        }
        $this->paginate= ['limit'=>10];
        $log = $this->paginate($log);
        $this->set(compact('log'));
    }
    
    /**
     * Add method
     *
     * @return void Redirects on successful add, renders view otherwise.
     */
    public function add() {
        $projectLog = $this->ProjectLog->newEntity();
        if ($this->request->is('post')) {
            $projectLog = $this->ProjectLog->patchEntity($projectLog, $this->request->data);
            if ($this->ProjectLog->save($projectLog)) {
                $this->Flash->success(__('The project log has been saved.'));
                return $this->redirect(['action' => 'index']);
            } 
            else {
                $this->Flash->error(__('The project log could not be saved. Please, try again.'));
            }
        }
        $projects = $this->ProjectLog->Projects->find('list', ['limit' => 200]);
        $modules = $this->ProjectLog->Modules->find('list', ['limit' => 200]);
        $bugs = $this->ProjectLog->Bugs->find('list', ['limit' => 200]);
        $this->set(compact('projectLog', 'projects', 'modules', 'bugs'));
        $this->set('_serialize', ['projectLog']);
    }
    
    /**
     * Edit method
     *
     * @param string|null $id Project Log id.
     * @return void Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null) {
        $projectLog = $this->ProjectLog->get($id, ['contain' => []]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $projectLog = $this->ProjectLog->patchEntity($projectLog, $this->request->data);
            if ($this->ProjectLog->save($projectLog)) {
                $this->Flash->success(__('The project log has been saved.'));
                return $this->redirect(['action' => 'index']);
            } 
            else {
                $this->Flash->error(__('The project log could not be saved. Please, try again.'));
            }
        }
        $projects = $this->ProjectLog->Projects->find('list', ['limit' => 200]);
        $modules = $this->ProjectLog->Modules->find('list', ['limit' => 200]);
        $bugs = $this->ProjectLog->Bugs->find('list', ['limit' => 200]);
        $this->set(compact('projectLog', 'projects', 'modules', 'bugs'));
        $this->set('_serialize', ['projectLog']);
    }
    
    /**
     * Delete method
     *
     * @param string|null $id Project Log id.
     * @return void Redirects to index.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function delete($id = null) {
        $this->request->allowMethod(['post', 'delete']);
        $projectLog = $this->ProjectLog->get($id);
        if ($this->ProjectLog->delete($projectLog)) {
            $this->Flash->success(__('The project log has been deleted.'));
        } 
        else {
            $this->Flash->error(__('The project log could not be deleted. Please, try again.'));
        }
        return $this->redirect(['action' => 'index']);
    }
}
