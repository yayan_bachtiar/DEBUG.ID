<?php
namespace App\Controller;
use App\Controller\AppController;

/**
 *
 */
class ReportController extends AppController
{
    
    /**
     * Index method
     *
     * @return void
     */
    
    public function initialize() {
        parent::initialize();
        $this->loadComponent('Paginator');
    }
    
    public function bugs() {
        $this->loadModel('Bugs');
        $bug = $this->Bugs->find()->contain(['Status', 'BugsParticipant' => ['Users'], 'Module' => ['Project']]);
        $this->paginate = ['limit' => 10];
        $bugs = $this->paginate($bug);
        
        // $this->loadComponent('Paginator');
        $this->set(compact('bug'));
        $this->set(compact('_serialize'));
    }
    
    public function dashboard_statistic() {
        $this->autoRender = false;
        $data['project'] = $this->get_project();
        $data['users'] = $this->get_users();
        $data['bugs'] = $this->get_bugs();
        $data['task'] = $this->get_task();
        echo json_encode($data);
    }
    
    private function get_project() {
        $this->loadModel('Project');
        return $this->Project->find('all')->select(['id'])->count();
    }
    
    private function get_users() {
        $this->loadModel('Users');
        return $this->Users->find('all')->select(['id'])->count();
    }
    private function get_task() {
        $this->loadModel('Task');
        return $this->Task->find('all')->select(['id'])->where(['is_deleted' => 0])->count();
    }
    private function get_bugs() {
        $this->loadModel('Bugs');
        return $this->Bugs->find('all')->select(['id'])->where(['is_deleted' => 0])->count();
    }
}
