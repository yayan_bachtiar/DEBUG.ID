<?php
namespace App\Controller;

use App\Controller\AppController;
use App\Controller\UsersController;

/**
 * ProjectParticipant Controller
 *
 * @property \App\Model\Table\ProjectParticipantTable $ProjectParticipant
 */
class ProjectParticipantController extends AppController
{
    
    /**
     * Index method
     *
     * @return void
     */
    public function initialize() {
        parent::initialize();
    }
    public function index() {
        $this->paginate = ['contain' => ['Project', 'Users']];
        $this->set('projectParticipant', $this->paginate($this->ProjectParticipant));
        $this->set('_serialize', ['projectParticipant']);
    }
    
    /**
     * View method
     *
     * @param string|null $id Project Participant id.
     * @return void
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function view($id = null) {
        $projectParticipant = $this->ProjectParticipant->get($id, ['contain' => ['Project', 'Users']]);
        $this->set('projectParticipant', $projectParticipant);
        $this->set('_serialize', ['projectParticipant']);
    }
    
    /**
     * Add method
     *
     * @return void Redirects on successful add, renders view otherwise.
     */
    
    public function get_participant() {
        if ($this->request->is('ajax')) {
            $project_id = $_GET['project_id'];
            $projectParticipant = $this->ProjectParticipant->find('all', ['contain' => ['Users']])->where(['project_id' => $project_id]);
            
            // var_dump($projectParticipant);
            $number = $projectParticipant->count();
            $this->set(compact('project_id', 'number', 'projectParticipant'));
            
            $projectParticipant = $this->ProjectParticipant->newEntity();
            $this->viewBuilder()->layout= 'ajax';
            if (!empty($this->request->data['user_id'])) {
                $query = $this->ProjectParticipant->query();
                $id = $this->request->data['user_id'];
                $query->insert(['project_id', 'user_id'])->values(['project_id' => $project_id, 'user_id' => $id])->execute();
                $this->render('get_participant');
                
                $users = $this->ProjectParticipant->Users->get($id);
                $project = $this->ProjectParticipant->Project->get($project_id, ['contain' => 'Users']);
                $email = $users->email;
                $firstname = $users->firstname;
                $lastname = $users->lastname;
                $contain['realname'] = $firstname . ' ' . $lastname;
                $contain['project_name'] = $project->project_name;
                $contain['project_desc'] = $project->project_desc;
                $contain['project_desc'] = $project->project_desc;
                $contain['project_start'] = $project->project_start;
                $contain['project_start'] = $project->project_start;
                $contain['project_end'] = $project->project_end;
                // $contain['project_owner'] = $project->Users->firstname . ' ' . $project->Users->lastname;
                $contain['created'] = $project->created;
                $contain['project_image'] = $project->project_image;
                
                $related_link = json_encode($project);
                $this->send_mail('default', 'addtoproject', 'html', 'abiedoank@gmail.com', $email, $contain['realname'], 'You have Assigned on ' . $project->project_name . ' project', $contain);
                $this->write_log($id, "$firstname . ' ' . $lastname added to project", "add_user", $related_link, null, null);
            }
        }
    }
    
    public function delete($id_project) {
        $projectParticipant = $this->ProjectParticipant->newEntity();
        if ($this->request->is('ajax')) {
            $query = $this->ProjectParticipant->query();
            $id = $_GET['id'];
            $query->delete()->where(['user_id' => $id, 'project_id' => $id_project])->execute();
        }
    }
    
    /**
     * Edit method
     *
     * @param string|null $id Project Participant id.
     * @return void Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null) {
        $projectParticipant = $this->ProjectParticipant->get($id, ['contain' => []]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $projectParticipant = $this->ProjectParticipant->patchEntity($projectParticipant, $this->request->data);
            if ($this->ProjectParticipant->save($projectParticipant)) {
                $this->Flash->success('The project participant has been saved.');
                return $this->redirect(['action' => 'index']);
            } 
            else {
                $this->Flash->error('The project participant could not be saved. Please, try again.');
            }
        }
        $project = $this->ProjectParticipant->Project->find('list', ['limit' => 200]);
        $users = $this->ProjectParticipant->Users->find('list', ['limit' => 200]);
        $this->set(compact('projectParticipant', 'project', 'users'));
        $this->set('_serialize', ['projectParticipant']);
    }
}
