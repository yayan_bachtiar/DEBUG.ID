<?php
namespace App\Controller;

use App\Controller\AppController;

/**
 * Priority Controller
 *
 * @property \App\Model\Table\PriorityTable $Priority
 */
class PriorityController extends AppController
{
    
    /**
     * Index method
     *
     * @return void
     */
    public function initialize() {
        parent::initialize();
    }
    public function index() {
        $this->set('priority', $this->paginate($this->Priority));
        $this->set('_serialize', ['priority']);
    }
    
    /**
     * View method
     *
     * @param string|null $id Priority id.
     * @return void
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function view($id = null) {
        $priority = $this->Priority->get($id, ['contain' => []]);
        $this->set('priority', $priority);
        $this->set('_serialize', ['priority']);
    }
    
    /**
     * Add method
     *
     * @return void Redirects on successful add, renders view otherwise.
     */
    public function add() {
        $priority = $this->Priority->newEntity();
        if ($this->request->is('post')) {
            $priority = $this->Priority->patchEntity($priority, $this->request->data);
            if ($this->Priority->save($priority)) {
                $this->Flash->success(__('The priority has been saved.'));
                return $this->redirect(['action' => 'index']);
            } 
            else {
                $this->Flash->error(__('The priority could not be saved. Please, try again.'));
            }
        }
        $this->set(compact('priority'));
        $this->set('_serialize', ['priority']);
    }
    
    /**
     * Edit method
     *
     * @param string|null $id Priority id.
     * @return void Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null) {
        $priority = $this->Priority->get($id, ['contain' => []]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $priority = $this->Priority->patchEntity($priority, $this->request->data);
            if ($this->Priority->save($priority)) {
                $this->Flash->success(__('The priority has been saved.'));
                return $this->redirect(['action' => 'index']);
            } 
            else {
                $this->Flash->error(__('The priority could not be saved. Please, try again.'));
            }
        }
        $this->set(compact('priority'));
        $this->set('_serialize', ['priority']);
    }
    
    /**
     * Delete method
     *
     * @param string|null $id Priority id.
     * @return void Redirects to index.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function delete($id = null) {
        $this->request->allowMethod(['post', 'delete']);
        $priority = $this->Priority->get($id);
        if ($this->Priority->delete($priority)) {
            $this->Flash->success(__('The priority has been deleted.'));
        } 
        else {
            $this->Flash->error(__('The priority could not be deleted. Please, try again.'));
        }
        return $this->redirect(['action' => 'index']);
    }
}
