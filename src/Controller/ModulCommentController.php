<?php
namespace App\Controller;

use App\Controller\AppController;

/**
 * ModulComment Controller
 *
 * @property \App\Model\Table\ModulCommentTable $ModulComment
 */
class ModulCommentController extends AppController
{
    
    /**
     * Index method
     *
     * @return void
     */
    public function initialize() {
        parent::initialize();
    }
    public function index() {
        $this->paginate = ['contain' => ['Modules']];
        $this->set('modulComment', $this->paginate($this->ModulComment));
        $this->set('_serialize', ['modulComment']);
    }
    
    /**
     * View method
     *
     * @param string|null $id Modul Comment id.
     * @return void
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function view($id = null) {
        $modulComment = $this->ModulComment->get($id, ['contain' => ['Modules']]);
        $this->set('modulComment', $modulComment);
        $this->set('_serialize', ['modulComment']);
    }
    
    public function get_modul_comments() {
        if ($this->request->is('ajax')) {
            $module_id = $_GET['module_id'];
            $comments = $this->ModulComment->find()->where(['module_id' => $module_id])->contain(['Users']);
            $numbers = $comments->count();
            $this->set(compact('comments', 'module_id'));
        }
    }
    
    /**
     * Add method
     *
     * @return void Redirects on successful add, renders view otherwise.
     */
    public function add($module_id) {
        
        // $this->autoRender = false;
        $modulComment = $this->ModulComment->newEntity();
        if ($this->request->is('post')) {
            $modulComment->module_id = $module_id;
            $modulComment->created = date('Y-m-d H:i:s', time());
            $modulComment->created_by = $this->Auth->user('id');
            $modulComment = $this->ModulComment->patchEntity($modulComment, $this->request->data);
            if ($this->ModulComment->save($modulComment)) {
                $comments = $this->ModulComment->find()->where(['module_id' => $module_id])->contain(['Users']);
                $this->set(compact('comments', 'module_id'));
                $this->render('get_modul_comments');
            } 
            else {
                
                // echo ($this->ModulComment->save($modul_comment));
                echo 'The modul comment could not be saved. Please, try again.';
            }
        }
        
        // $modules = $this->ModulComment->Module->find('list', ['limit' => 200]);
        // $this->set(compact('modulComment', 'modules'));
        // $this->set('_serialize', ['modulComment']);
        
        
    }
    
    /**
     * Edit method
     *
     * @param string|null $id Modul Comment id.
     * @return void Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null) {
        $modulComment = $this->ModulComment->get($id, ['contain' => []]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $modulComment = $this->ModulComment->patchEntity($modulComment, $this->request->data);
            if ($this->ModulComment->save($modulComment)) {
                $this->Flash->success('The modul comment has been saved.');
                return $this->redirect(['action' => 'index']);
            } 
            else {
                $this->Flash->error('The modul comment could not be saved. Please, try again.');
            }
        }
        $modules = $this->ModulComment->Modules->find('list', ['limit' => 200]);
        $this->set(compact('modulComment', 'modules'));
        $this->set('_serialize', ['modulComment']);
    }
    
    /**
     * Delete method
     *
     * @param string|null $id Modul Comment id.
     * @return void Redirects to index.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function delete($id = null) {
        $this->request->allowMethod(['post', 'delete']);
        $modulComment = $this->ModulComment->get($id);
        if ($this->ModulComment->delete($modulComment)) {
            $this->Flash->success('The modul comment has been deleted.');
        } 
        else {
            $this->Flash->error('The modul comment could not be deleted. Please, try again.');
        }
        return $this->redirect(['action' => 'index']);
    }
}
