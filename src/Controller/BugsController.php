<?php
namespace App\Controller;

use App\Controller\AppController;
use Cake\Datasource\ConnectionManager;
/**
 * Bugs Controller
 *
 * @property \App\Model\Table\BugsTable $Bugs
 */
class BugsController extends AppController
{
    
    /**
     * Index method
     *
     * @return void
     */
    
    public function initialize() {
        parent::initialize();
        $this->loadComponent('Paginator');
    }
    public function index() {
        $this->paginate = ['contain' => ['Module']];
        $this->set('bugs', $this->paginate($this->Bugs));
        $this->set('_serialize', ['bugs']);
    }
    
    /**
     * View method
     *
     * @param string|null $id Bug id.
     * @return void
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function view($id = null) {
        $bug = $this->Bugs->get($id, ['contain' => ['Module', 'Users', 'Priority', 'Status','BugsParticipant'=>['Users']]]);
        $title = "View Bugs " . $bug->bugs_name;
        $color = 'white_bg';
        $fa = "fa fa-question";
        if ($bug->status->id == 1) {
            $color = 'bg-blue';
            $fa = "fa fa-thumbs-up";
        }
        if ($bug->status->id == 2) {
            $color = 'bg-orange';
            $fa = "fa fa-thumb-tack";
        }
        
        if ($bug->status->id == 3) {
            $color = 'bg-red';
            $fa = "fa fa-thumbs-down";
        }
        $color2 = "bg-green";
        $fa2 = "fa fa-question";
        if ($bug->priority->name == 'URGENT') {
            $color2 = "bg-red";
            $fa2 = "fa fa-rocket";
        }
        if ($bug->priority->name == 'TOP') {
            $color2 = "bg-orange";
            $fa2 = "fa fa-space-shuttle";
        }
        if ($bug->priority->name == 'HIGH') {
            $color2 = "bg-yellow";
            $fa2 = "fa fa-plane";
        }
        if ($bug->priority->name == 'HIGH') {
            $color2 = "bg-green";
            $fa2 = "fa fa-ambulance";
        }
        if ($bug->priority->name == 'MIDDLE') {
            $color2 = "bg-blue";
            $fa2 = "fa fa-car";
        }
        if ($bug->priority->name == 'LOW') {
            $color2 = "bg-blue-sky";
            $fa2 = "fa fa-taxi";
        }
        if ($bug->priority->name == 'VERY LOW') {
            $color2 = "bg-grey";
            $fa2 = "fa fa-truck";
        }
        
        $this->set(compact('bug', 'title', 'color', 'fa', 'color2', 'fa2'));
        
        $this->set('_serialize', ['bug']);
    }
    
    /**
     * get all
     * @return [type] [description]
     */
    public function all_bugs() {
        $pid = $_GET['pid'];
        $bugs = $this->Bugs->find()->contain(['Module' => ['Project']])->where(['Project.id' => $pid]);
        $this->set(compact('bugs'));
        $this->set('_serialize', ['bugs']);
    }
    
    public function get_bugs() {
        $mid = $_GET['id'];
        $bugs = $this->Bugs->find()->contain(['Status'])->where(['modul_id' => $mid]);
        $this->paginate = ['limit' => 10];
        $bugs = $this->paginate($bugs);
        $this->set(compact('bugs'));
    }
    
    /**
     * [get_module_bug description]
     * @return [type] [description]
     */
    public function get_module_bug() {
        
        // if ($this->request->is('ajax')) {
        $module_id = $_GET['module_id'];
        if ($this->request->is('ajax')) {
            $this->viewBuilder()->layout('ajax');
        }
        $modul_id = $_GET['module_id'];
        $type = $_GET['type'];
        if ($type == 'all') {
            $tasks = $this->Bugs->find('all', ['contain'])->where(['modul_id' => $modul_id]);
        }
        if ($type == 'pass') {
            $tasks = $this->Bugs->find('all', ['contain'])->where(['modul_id' => $modul_id, 'Bugs.status' => 1]);
        }
        if ($type == 'pending') {
            $tasks = $this->Bugs->find('all', ['contain'])->where(['modul_id' => $modul_id, 'Bugs.status' => 2]);
        }
        if ($type == 'failed') {
            $tasks = $this->Bugs->find('all', ['contain'])->where(['modul_id' => $modul_id, 'Bugs.status' => 3]);
        }
        if ($type == 'ready') {
            $tasks = $this->Bugs->find('all', ['contain'])->where(['modul_id' => $modul_id, 'Bugs.status' => 4]);
        }
        
        // $this->viewBuilder()->layout('ajax');
        $bugs = $this->Bugs->find('all')->where(['modul_id' => $module_id, 'is_deleted' => 0]);
        $this->paginate = ['limit' => 5];
        $bugs = $this->paginate($bugs);
        $number = $bugs->count();
        $this->set(compact('bugs', 'number', 'module_id'));
        
        // }
        
    }

    private function save_history($id){
        $this->loadModel('BugHistory');
        $user = $this->Auth->user('id');
        $sql = "INSERT INTO bug_history (`id`, `bugs_name`, `created`, `created_by`, `due_date`, `ekspektasi`, `bug_id`, `is_deleted`, `link`, `modul_id`, `priority`, `result`, `saran`, `skenario`, `start_date`, `status`, `version`)
        SELECT
            null, `bugs_name`, now(), `created_by`, `due_date`, `ekspektasi`, `id`, `is_deleted`, `link`, `modul_id`, `priority`, `result`, `saran`, `skenario`, `start_date`, `status`, `version`
        FROM
            `bugs`
        WHERE id = $id;";
        $conn = ConnectionManager::get('default');
        $stmt = $conn->query($sql);
    }

    public function open($id){
        if(!$this->request->is('ajax')){
            $this->redirect(['controller'=>'pages']);
        }
        $query = $this->Bugs->query();
        $query->update()
            ->set(['status' => 3])
            ->where(['id'=>$id])
            ->execute();
        $this->save_history($id);
        echo "Bug status has been set to open";
        $this->autoRender = false;
    }

    public function close($id){
        if(!$this->request->data('ajax')){
            $this->redirect(['controller'=>'pages']);
        }
        $query = $this->Bugs->query();
        $query->update()
            ->set(['status' => 1])
            ->where(['id'=>$id])
            ->execute();
        $this->save_history($id);
        echo "Bug status has been set to close";
        $this->autoRender = false;
    }
    public function is_ready($id){
        if(!$this->request->data('ajax')){
            $this->redirect(['controller'=>'pages']);
        }
        $query = $this->Bugs->query();
        $query->update()
            ->set(['status' => 4])
            ->where(['id'=>$id])
            ->execute();
        $this->save_history($id);
        echo "Bug status has been set to ready";
        $this->autoRender = false;
    }
    
    public function get_module_bug_stat() {
        if ($this->request->is('ajax')) {
            $modul_id = $_GET['module_id'];
            $this->viewBuilder()->layout('ajax');
            $bug_sum = $this->Bugs->find('all', ['contain'])->where(['modul_id' => $modul_id])->count();
            $bug_sum_pass = $this->Bugs->find('all', ['contain'])->where(['modul_id' => $modul_id, 'Bugs.status' => 1])->count();
            $bug_sum_pending = $this->Bugs->find('all', ['contain'])->where(['modul_id' => $modul_id, 'Bugs.status' => 2])->count();
            $bug_sum_failed = $this->Bugs->find('all', ['contain'])->where(['modul_id' => $modul_id, 'Bugs.status' => 3])->count();
            $bug_sum_ready = $this->Bugs->find('all', ['contain'])->where(['modul_id' => $modul_id, 'Bugs.status' => 4])->count();
            $this->set(compact('bug_sum', 'bug_sum_pass', 'bug_sum_failed', 'bug_sum_pending', 'bug_sum_ready', 'numbers', 'modul_id'));
            if ($bug_sum == null) {
                $bug_sum = 1;
                $bug_sum_pass = 1;
            }
            $percent = round(($bug_sum_pass / $bug_sum * 100), 2);
            $this->set(compact('percent'));
        }
    }
    
    public function my_bugs() {
        $this->set('title', 'Manage My Bugs');
        $user_id = $this->Auth->User('id');
        $this->paginate = ['limit' => 1];
        $bugs = $this->Bugs->BugsParticipant->find('all')->contain(['Bugs'])->where(['Bugs.is_deleted' => 0, 'BugsParticipant.user_id' => $user_id])->group(['Bugs.id']);
        $total = $bugs->count();
        $this->set(compact('total'));
        $bugspassed = $this->Bugs->BugsParticipant->find('all')->contain(['Bugs'])->where(['Bugs.is_deleted' => 0, 'BugsParticipant.user_id' => $user_id, 'Bugs.status' => 1])->group(['Bugs.id']);
        $bpass = $this->paginate($bugspassed);
        $ttbpas = $bugspassed->count();
        if ($total == 0) {
            $total = 1;
            $ttbpas = 1;
        }
        $pass = $ttbpas / $total * 100;
        $this->set(compact('pass'));
    }
    
    public function get_all() {
        $user_id = $this->Auth->User('id');
        if ($this->request->is('ajax')) {
            $this->viewBuilder()->layout('ajax');
        }
        $this->paginate = ['limit' => 5];
        $ball = $this->Bugs->BugsParticipant->find('all')->contain(['Bugs', "Users"])->where(['Bugs.is_deleted' => 0, 'BugsParticipant.user_id' => $user_id])->group(['Bugs.id']);
        $ball = $this->paginate($ball);
        $this->set(compact('ball'));
    }
    public function project_get_bug($id=null) {
        // $user_id = $this->Auth->User('id');
        if ($this->request->is('ajax')) {
            $this->viewBuilder()->layout('ajax');
        }
        $this->paginate = ['limit' => 5];
        $ball = $this->Bugs->find('all')->contain(['Module'=>['Project'], "BugsParticipant"=>['Users']])->where(['Bugs.is_deleted' => 0, 'Module.project_id' => $id])->group(['Bugs.id']);
        $ball = $this->paginate($ball);
        $this->set(compact('ball'));
        // $this->render('get_all');
    }
    
    public function get_unresolved_bugs() {
        if ($this->request->is('ajax')) {
            $this->viewBuilder()->layout('ajax');
        }
        $this->paginate = ['limit' => 5];
        $this->set(compact('ball'));
        $bf = $this->get_bugs_by_status(3);
        $bf = $this->paginate($bf);
        $this->set('bf', $bf);
    }
    public function get_ready() {
        if ($this->request->is('ajax')) {
            $this->viewBuilder()->layout('ajax');
        }
        $this->paginate = ['limit' => 5];
        // $this->set(compact('ready'));
        $bf = $this->get_bugs_by_status(4);
        $bf = $this->paginate($bf);
        $this->set('ready', $bf);
    }
    
    /**
     * [get_bugs_passed description]
     * @return [type] [description]
     */
    public function get_bugs_passed() {
        if ($this->request->is('ajax')) {
            $this->viewBuilder()->layout('ajax');
        }
        $this->paginate = ['limit' => 5];
        $bugspassed = $this->get_bugs_by_status(1);
        $bpass = $this->paginate($bugspassed);
        $this->set('bpass', $bpass);
    }
    
    /**
     * get bugs by status
     * @param  integer $status [number of status]
     * @return [object]         [all bug]
     */
    private function get_bugs_by_status($status = null) {
        $user_id = $this->Auth->User('id');
        $stat = 'Bugs.status = ' . $status;
        // if ($status == null) {
        //     $stat = ' 1=1 ';
        // }
        $user_id = $this->Auth->User('id');
        return $this->Bugs->BugsParticipant->find('all')->contain(['Bugs', "Users"])->where(['Bugs.is_deleted' => 0, 'BugsParticipant.user_id' => $user_id, "Bugs.status" => $status])->group(['Bugs.id']);
    }
    
    /**
     * Add method
     *
     * @return void Redirects on successful add, renders view otherwise.
     */
    public function add($id = null) {
        if (empty($id)) {
            $this->redirect(['action' => 'index']);
        }
        $this->set(compact('project_id'));
        $this->set('title', 'Open New Bug');
        $bug = $this->Bugs->newEntity();
        if ($this->request->is('post')) {
            
            $bug = $this->Bugs->patchEntity($bug, $this->request->data);
            $bug->created_by = $this->Auth->user('id');
            $bug->modul_id = $id;
            $bug->status = $this->request->data['status'];
            $date = explode(' - ', $this->request->data('reservation'));
            $bug->start_date = $date[0];
            $bug->due_date = $date[1];
            $bug->priority = $this->request->data['priority'];
            $bug->expekasi = $this->request->data['ekspektasi'];
            
            //save bug
            $result = $this->Bugs->save($bug);
            if ($result) {
                $users = $this->Bugs->Module->find('all')->contain(['Users'])
                ->select(['Module.id', 'Module.module_excecutor', 
                    'Module.module_name', 'Users.email', 'Users.role', 
                    'Users.username'])->where(['Module.id' => $id]);
                
                // print_r($users);
                // die();
                $query = $this->Bugs->BugsParticipant->query();
                $contain['bug_id'] = $result->id;
                $contain['bugs_name'] = $bug->bugs_name;
                $contain['status'] = $bug->status;
                $contain['scenario'] = $bug->scenario;
                $contain['priority'] = $bug->priority;
                $contain['expektasi'] = $bug->expekasi;
                $contain['due_date'] = $bug->due_date;
                $contain['start_date'] = $bug->start_date;
                $contain['full_name'] = $this->Auth->User('firstname') 
                . " " . $this->Auth->User('lastname');
                
                // $contain['full_name'] = $this->Auth->user('username');
                $contain['created_by'] = $this->Auth->user('id');
                foreach ($users as $user) {
                    $contain['realname'] = $user->user->username;
                    $contain['module_name'] = $user->module_name;
                    $contain['module_id'] = $user->id;
                    $query->insert(['bug_id', 'user_id'])->values(['bug_id' => $result->id, 'user_id' => $user->module_excecutor])->execute();
                    $this->send_mail('default', 'bug_open', 'html', 'abiedoank@gmail.com', $user->user->email, $contain['realname'], 'A bug has been found' . $bug->bugs_name, $contain);
                }
                
                //get project id
                $pid = $this->Bugs->Module->find()->select(['Module.project_id'])->where(['Module.id' => $id])->first();
                $pid = $pid->project_id;
                $username = $this->Auth->user('username');
                
                //get json data
                $related_link = json_encode($bug);
                
                //write log
                $this->write_log($pid, "Bug has beeen opened by $username", "bug_opened", $related_link, $id, $result->id);
                
                //redirect
                $this->redirect(['controller' => 'bugs', 'action' => 'view', $result->id]);
            }
        }
        $priority = $this->Bugs->Priority->find('all');
        $this->set(compact('bug', 'id', 'priority', 'status'));
        $this->set('_serialize', ['bug']);
    }
}
