<?php
namespace App\Controller;

use App\Controller\AppController;
use Cake\Event\Event;
use Cake\Auth\DefaultPasswordHasher;
//use Cake\Network\Email\Email;
/**
 * Users Controller
 *
 * @property \App\Model\Table\UsersTable $Users
 */
class UserController extends AppController
{
    
    /**
     * Index method
     *
     * @return void
     */
    public function initialize() {
        parent::initialize();
        $this->loadComponent('Paginator');
    }
    
    public function beforeFilter(Event $event) {
        parent::beforeFilter($event);
        $this->loadModel('Users');
    }
    
    public function index() {
        $user_id = $this->Auth->user('id');
        $user = $this->Users->get($user_id, ['contain' => ['Role']]);
        $this->set('user', $user);
        $this->set('title', 'User Profile ' . $user->firstname . ' ' . $user->lastname);
        $this->set('_serialize', ['user']);
    }
    
    public function user_list() {
        $this->paginate = ['contain' => ['Role'], 'limit'=>10];
        $user=$this->Users->find()->where(['active'=>1]);
        $user = $this->paginate($user);
        $this->set(compact('user'));
    }

    public function unapproved_user(){
        $this->paginate = ['contain' => ['Role'], 'limit'=>10];
        $user=$this->Users->find()->where(['active'=>0]);
        $user = $this->paginate($user);
        $this->set(compact('user'));
    }

    public function approve($id){
        if(!$this->request->data('ajax')){
            $this->redirect(['controller'=>'pages']);
        }
        $query = $this->Users->query();
        $query->update()
            ->set(['active' => 1])
            ->where(['id'=>$id])
            ->execute();
        echo "user has been activated";
        $this->autoRender = false;
    }    
    public function get_active_menu() {
        
        // $this->autoRender = false;
        $session = $this->request->session();
        $this->viewBuilder()->layout('ajax');
        $data['menu'] = $session->read('menu');
        $data['submenu'] = $session->read('submenu');
        echo json_encode($data);
    }
    
    public function all_user() {
        $this->set('users', $this->paginate($this->Users));
        $this->set('_serialize', ['users']);
    }
    
    public function user_badge() {
        $id = $_GET['id'];
        $user = $this->Users->get($id, ['contain' => ['Role']]);
        $this->set(compact('user'));
    }
    
    // public function login() {
    //     $this->viewBuilder()->layout('login');
    //     if ($this->request->is('post')) {
    //         $user = $this->Auth->identify();
    //         if ($user) {
    //             $this->Auth->setUser($user);
    //             return $this->redirect($this->Auth->redirectUrl());
    //         }
    //         $this->Flash->error(__('Invalid username or password, try again'));
    //     }
    //     $role = $this->Users->Role->find('all');
    //     $this->set(compact('role'));
    // }
        
    // public function logout() {
    //     return $this->redirect($this->Auth->logout());
    // }
    
    public function send_mail_demo() {
        // $config, $template, $format, $from, $to, $title, $subject, $content
        $content = (['next' => 'asdsadsadsadasd', 'prev' => 'adasdasdasdsad', 'but' => 'fooo']);
        $this->send_mail('default', 'registration', 'html', 'abiedoank@gmail.com', 'abiedoank@outlook.com', 'noreply@tello.com', 'Welcome Email', $content);
    }
    
    /**
     * View method
     *
     * @param string|null $id User id.
     * @return void
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function view($id = null) {
        $user = $this->Users->get($id, ['contain' => ['Role']]);
        $this->set('title', 'User Profile ' . $user->firstname . ' ' . $user->lastname);
        $this->set('user', $user);
        $this->set('_serialize', ['user']);
    }
    
    /**
     * Add method
     *
     * @return void Redirects on successful add, renders view otherwise.
     */
    public function add() {
        $user = $this->Users->newEntity();
        if ($this->request->is('post')) {
            $user = $this->Users->patchEntity($user, $this->request->data);
            $user->password = $this->request->data['password'];
            $user->email = $this->request->data['email'];
            $user->role = $this->request->data['role'];
            $user->register_date = date('Y-m-d H:i:s', time());
            $user->created_by = $this->Auth->User('id');
            $result = $this->Users->save($user);
            if ($result) {
                $this->Flash->success('The user has been saved.');
                return $this->redirect(['action' => 'view', $result->id]);
            } 
            else {
                $this->Flash->error('The user could not be saved. Please, try again.');
            }
        }
        $this->set(compact('user'));
        $this->set('_serialize', ['user']);
    }
    
    /**
     * Edit method
     *
     * @param string|null $id User id.
     * @return void Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null) {
        $user = $this->Users->get($id, ['contain' => []]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $user = $this->Users->patchEntity($user, $this->request->data);
            if ($this->Users->save($user)) {
                $this->Flash->success('The user has been saved.');
                return $this->redirect(['action' => 'index']);
            } 
            else {
                $this->Flash->error('The user could not be saved. Please, try again.');
            }
        }
        $this->set(compact('user'));
        $this->set('_serialize', ['user']);
    }
    
    /**
     * Delete method
     *
     * @param string|null $id User id.
     * @return void Redirects to index.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function delete($id = null) {
        $this->request->allowMethod(['post', 'delete']);
        $user = $this->Users->get($id);
        if ($this->Users->delete($user)) {
            $this->Flash->success('The user has been deleted.');
        } 
        else {
            $this->Flash->error('The user could not be deleted. Please, try again.');
        }
        return $this->redirect(['action' => 'index']);
    }
    
    public function get_user_list() {
        return $this->Users->find('all');
    }
    
    public function getSession() {
        $this->viewBuilder()->layout('ajax');
        $data['username'] = $this->Auth->User('username');
        $data['email'] = "http://www.gravatar.com/avatar/" . md5(strtolower(trim($this->Auth->User('email'))));
        echo json_encode($data);
        
        // }
        
        
    }
    
    public function select2user() {
        if ($this->request->is('ajax')) {
            $results = $this->Users->find()->select(['id', 'username'])->where(function ($exp, $q) {
                return $exp->like('username', '%' . $_GET['q'] . '%');
            });
            
            // $answer;
            if ($results->isEmpty()) {
                $answer[] = array("id" => "0", "text" => "No Results Found..");
            } 
            else {
                foreach ($results as $key) {
                    $answer[] = array("id" => $key->id, "text" => $key->username);
                }
            }
            
            // die();
            $this->autoRender = false;
            $this->viewBuilder()->layout('ajax');
            
            // if ($results)
            
            // finally encode the answer to json and send back the result.
            echo json_encode($answer);
        } 
        else {
            $this->redirect(['controller' => 'pages', 'action' => 'index']);
        }
    }
}
