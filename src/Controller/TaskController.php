<?php
namespace App\Controller;

use App\Controller\AppController;

/**
 * Task Controller
 *
 * @property \App\Model\Table\TaskTable $Task
 */
class TaskController extends AppController
{
    
    public $components = ['RequestHandler', 'Paginator'];
    
    /**
     * Index method
     *
     * @return void
     */
    public function initialize() {
        parent::initialize();
    }
    public function index() {
        
        // $this->paginate = ['contain' => ['Module']];
        $this->set('task', $this->paginate($this->Task));
        $this->set('_serialize', ['task']);
    }
    
    /**
     * View method
     *
     * @param string|null $id Task id.
     * @return void
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function view($id = null) {
        $task = $this->Task->get($id, ['contain' => ['Module']]);
        $this->set('task', $task);
        $this->set('_serialize', ['task']);
    }

    public function all_tasks(){
        $pid =  $_GET['pid'];
        $this->set(compact('pid'));
    }

    public function module_tasks(){
        $pid =  $_GET['pid'];
        $this->paginate = ['limit'=>10];
        $task = $this->Task->find()->contain(['Status', 'Module'=>['ModuleParticipant'=>['Users']]])->where(['Module.project_id'=>$pid]);
        $this->set('task', $this->paginate($task));
    }
    
    public function get_module_task() {
        if ($this->request->is('ajax')) {
            $this->viewBuilder()->layout('ajax');
        }
        $modul_id = $_GET['module_id'];
        $type = $_GET['type'];
        if ($type == 'all') {
            $tasks = $this->Task->find('all', ['contain'])->where(['modul_id' => $modul_id]);
        }
        if ($type == 'done') {
            $tasks = $this->Task->find('all', ['contain'])->where(['modul_id' => $modul_id, 'Task.status' => 4]);
        }
        if ($type == 'undone') {
            $tasks = $this->Task->find('all', ['contain'])->where(['modul_id' => $modul_id, 'Task.status' => 0]);
        }
        
        $numbers = $tasks->count();
        $this->paginate = ['limit' => 5];
        $task = $this->paginate($tasks);
        $this->set(compact('tasks', 'numbers', 'modul_id'));
        
    }

    public function get_module_task_stat() {
        if ($this->request->is('ajax')) {
            $modul_id = $_GET['module_id'];
            $this->viewBuilder()->layout('ajax');
            $task_sum = $this->Task->find('all', ['contain'])->where(['modul_id' => $modul_id])->count();
            $task_sum_done = $this->Task->find('all', ['contain'])->where(['modul_id' => $modul_id, 'Task.status' => 4])->count();
            $task_sum_undone = $this->Task->find('all', ['contain'])->where(['modul_id' => $modul_id, 'Task.status' => 0])->count();
            $this->set(compact('task_sum', 'task_sum_done', 'task_sum_undone', 'numbers'));
            if ($task_sum == null) {
                $task_sum = 1;
                $task_sum_done = 1;
            }
            $percent = round(($task_sum_done / $task_sum * 100), 2);
            $this->set(compact('percent', 'modul_id'));
        }
    }

    /**
     * [get_task description]
     * @return [type] [description]
     */
    public function get_task(){
        if($this->request->is('ajax')){
            $id = $_GET['id'];
            $this->paginate = ['limit'=>10];
            $tasks = $this->Task->find()->where(['Task.modul_id'=>$id])->order(['Task.status ASC']);
            $this->set('tasks', $this->paginate($tasks));
        }
    }

    public function ready(){
        if($this->request->is('ajax')){
            $id = $_GET['id'];
            $this->autoRender = false;
            $this->set_status($id, 4);
        }
    }

    public function unready(){
        if($this->request->is('ajax')){
            $id = $_GET['id'];
            $this->autoRender = false;
            $this->set_status($id, 7);
        }
    }

    private function set_status($id, $status){
        $task =  $this->Task->query();
            $task->update()
            ->set(['status'=>$status])
            ->where(['id'=>$id])->execute();
    }

    private function get_pid($id){
        $pid = $this->Task->Module->find()->select(['Module.project_id'])->where(['Module.id' => $id])->first();
        return $pid;
    }
    
    /**
     * Add method
     *
     * @return void Redirects on successful add, renders view otherwise.
     */
    public function add($id = null) {
        if (empty($id)) {
            $this->redirect(['controller' => 'pages']);
        }
        $task = $this->Task->newEntity();
        $task->modul_id = $id;
        $task->status = 0;
        $task->created_by = $this->Auth->User('id');
        if ($this->request->is('post')) {
            $task->task_name = htmlspecialchars($this->request->data['task_name'], ENT_QUOTES);
            $task->description = $this->request->data['description'];
            $task = $this->Task->patchEntity($task, $this->request->data);
            $result = $this->Task->save($task);
            if ($result) {
                $participant = $this->Task->Module->find('all')->contain(['Users'])->select(['Module.module_excecutor', 'Users.email', 'Users.role', 'Users.username'])->where(['Module.id' => $id]);
                // $pasrticipant = $this->Task->Module->ModuleParticipant->find()->contain(['Users'])->select(['Users.username', 'Users.email'])->where(['ModuleParticipant.modul_id' => $id, ['Users.role' => 1]]);
                $contain['id'] = $result->id;
                $contain['name'] = $task->task_name;
                $contain['desc'] = $task->description;
                $contain['mod_id'] = $id;
                $contain['full_name'] = $this->Auth->User('firstname') . " " . $this->Auth->User('lastname');
                if(!empty($participant)){
                    foreach ($participant as $user) {

                        $contain['realname'] = $user->user->username;
                        $this->send_mail('default', 
                            'task_assignment', 
                            'html', 
                            'abiedoank@gmail.com',
                             $user->user->email, 
                             $user->user->username, 
                             'You have Assigned on ' . $task->task_name . ' Module', $contain);
                    }                    
                }
                $pid = $this->Task->Module->find()->select(['Module.project_id'])->where(['Module.id' => $id])->first();
                //get dependency content
                $username = $this->Auth->user('username');
                $related_link = json_encode($contain);
                //write log
                $this->write_log($pid->project_id, "$username has been open an Task", "task_open", $related_link, $id, null);
                // $this->redirect(['controller' => 'module', 'action' => 'view', $id]);
            } 
            else {
                $this->Flash->error('The task could not be saved. Please, try again.');
            }
        }

        $this->set('title', 'Add task');
        $this->set(compact('id'));
        $this->set('_serialize', ['task']);
    }
    
    /**
     * Edit method
     *
     * @param string|null $id Task id.
     * @return void Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null) {
        $task = $this->Task->get($id, ['contain' => []]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $task = $this->Task->patchEntity($task, $this->request->data);
            if ($this->Task->save($task)) {
                $this->Flash->success('The task has been saved.');
                return $this->redirect(['action' => 'index']);
            } 
            else {
                $this->Flash->error('The task could not be saved. Please, try again.');
            }
        }
        $moduls = $this->Task->Moduls->find('list', ['limit' => 200]);
        $this->set(compact('task', 'module'));
        $this->set('_serialize', ['task']);
    }
    
    /**
     * Delete method
     *
     * @param string|null $id Task id.
     * @return void Redirects to index.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function delete($id = null) {
        $this->request->allowMethod(['post', 'delete']);
        $task = $this->Task->get($id);
        if ($this->Task->delete($task)) {
            $this->Flash->success('The task has been deleted.');
        } 
        else {
            $this->Flash->error('The task could not be deleted. Please, try again.');
        }
        return $this->redirect(['action' => 'index']);
    }
    
    public function my_task() {
        $user_id = $this->Auth->User('id');
        $title = 'My task';
        if ($this->request->is('ajax')) {
            
            // $status = $_GET['status'];
            $id = $this->encryptor('decrypt', $id);
            $this->paginate = ['limit' => 5, 'contain' => ['ModuleParticipant' => ['Users']], 'conditions' => ['Module.project_id' => $id, 'Module.status' => $status]];
            $module = $this->paginate($this->Module);
            
            //$module = $this->Paginator->paginate($this->Module, ['limit' => 5, 'conditions' => ['Module.project_id' => $id, 'Module.status' => $status], 'join' => ['']]);
            $this->set(compact('module', 'status'));
        }
        $this->set(compact('title'));
    }
    
}
