<?php
namespace App\Controller;

use App\Controller\AppController;

/**
 * ModuleAttachment Controller
 *
 * @property \App\Model\Table\ModuleAttachmentTable $ModuleAttachment
 */
class ModuleAttachmentController extends AppController
{
    
    /**
     * Index method
     *
     * @return void
     */
    public function initialize() {
        parent::initialize();
    }
    public function index() {
        $this->paginate = ['contain' => ['Module']];
        $this->set('moduleAttachment', $this->paginate($this->ModuleAttachment));
        $this->set('_serialize', ['moduleAttachment']);
    }
    
    public function get_attachment() {
        $module_id = $_GET['id'];
        $module_attach = $this->ModuleAttachment->find('all', ['contain'])->where(['modul_id' => $module_id]);
        $number = $module_attach->count();
        $this->set(compact('module_attach', 'number', 'module_id'));
    }
    
    /**
     * View method
     *
     * @param string|null $id Module Attachment id.
     * @return void
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function view($id = null) {
        $moduleAttachment = $this->ModuleAttachment->get($id, ['contain' => ['Moduls']]);
        $this->set('moduleAttachment', $moduleAttachment);
        $this->set('_serialize', ['moduleAttachment']);
    }
    
    /**
     * Add method
     *
     * @return void Redirects on successful add, renders view otherwise.
     */
    public function add() {
        $id = $_GET['mod'];
        $moduleAttachment = $this->ModuleAttachment->newEntity();
        
        $moduleAttachment = $this->ModuleAttachment->patchEntity($moduleAttachment, $this->request->data);
        $ds = DIRECTORY_SEPARATOR;
        
        $upload_path = 'images/moduleAttachment/' . $id;
        $storeFolder = WWW_ROOT . $upload_path;
        if (!file_exists($storeFolder)) {
            mkdir($storeFolder, 0777, true);
        }
        if (!empty($_FILES)) {
            $actual_name = pathinfo($_FILES['file']['name'], PATHINFO_FILENAME);
            $original_name = $actual_name;
            $extension = pathinfo($_FILES['file']['name'], PATHINFO_EXTENSION);
            
            $tempFile = $_FILES['file']['tmp_name'];
            $targetPath = $storeFolder . $ds;
            $name = $_FILES['file']['name'];
            $targetFile = $targetPath . $_FILES['file']['name'];
            $i = 1;
            while (file_exists($targetFile)) {
                $actual_name = (string)$original_name . $i;
                $name = $actual_name . "." . $extension;
                $targetFile = $targetPath . $name;
                $i++;
            }
            if (move_uploaded_file($_FILES['file']['tmp_name'], $targetFile)) {
                $id = $this->encryptor('decrypt', $id);
                $moduleAttachment->path = $upload_path;
                $moduleAttachment->ext = $extension;
                $moduleAttachment->filename = $_FILES['file']['name'];
                $moduleAttachment->modul_id = $id;
                $moduleAttachment->upload_by = $this->Auth->User('id');
                $moduleAttachment->upload_date = date('Y-m-d H:i:s', time());
                
                if ($this->ModuleAttachment->save($moduleAttachment)) {
                    echo 'saved';
                    $this->Flash->success('The project attachment has been saved.');
                } 
                else {
                    echo 'unsaved';
                    var_dump($moduleAttachment);
                    echo $id;
                }
                echo "File is valid, and was successfully uploaded.\n";
            } 
            else {
                echo "Possible file upload attack!\n";
            }
        }
        $id = $this->encryptor('decrypt', $id);
        $title = 'Add Module Attachment';
        $this->set(compact('id', 'title'));
        $this->set('_serialize', ['id']);
        
        // $moduleAttachment = $this->ModuleAttachment->newEntity();
        // if ($this->request->is('post')) {
        //     $moduleAttachment = $this->ModuleAttachment->patchEntity($moduleAttachment, $this->request->data);
        //     if ($this->ModuleAttachment->save($moduleAttachment)) {
        //         $this->Flash->success('The module attachment has been saved.');
        //         return $this->redirect(['action' => 'index']);
        //     }
        //     else {
        //         $this->Flash->error('The module attachment could not be saved. Please, try again.');
        //     }
        // }
        // $moduls = $this->ModuleAttachment->Module->find('list', ['limit' => 200]);
        // $this->set(compact('moduleAttachment', 'moduls'));
        // $this->set('_serialize', ['moduleAttachment']);
        
        
    }
    
    /**
     * Edit method
     *
     * @param string|null $id Module Attachment id.
     * @return void Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null) {
        $moduleAttachment = $this->ModuleAttachment->get($id, ['contain' => []]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $moduleAttachment = $this->ModuleAttachment->patchEntity($moduleAttachment, $this->request->data);
            if ($this->ModuleAttachment->save($moduleAttachment)) {
                $this->Flash->success('The module attachment has been saved.');
                return $this->redirect(['action' => 'index']);
            } 
            else {
                $this->Flash->error('The module attachment could not be saved. Please, try again.');
            }
        }
        $moduls = $this->ModuleAttachment->Module->find('list', ['limit' => 200]);
        $this->set(compact('moduleAttachment', 'modul'));
        $this->set('_serialize', ['moduleAttachment']);
    }
    
    /**
     * Delete method
     *
     * @param string|null $id Module Attachment id.
     * @return void Redirects to index.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function delete() {
        $id = $_GET['id'];
        $this->viewBuilder()->layout('ajax');
        $this->autoRender = false;
        
        // $this->request->allowMethod(['post', 'delete']);
        $moduleAttachment = $this->ModuleAttachment->get($id);
        if ($this->ModuleAttachment->delete($moduleAttachment)) {
            echo "berhasil";
        }
    }
}
