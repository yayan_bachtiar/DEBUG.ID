<?php
namespace App\Controller;

use App\Controller\AppController;

/**
 * ModuleParticipant Controller
 *
 * @property \App\Model\Table\ModuleParticipantTable $ModuleParticipant
 */
class ModuleParticipantController extends AppController
{
    
    /**
     * Index method
     *
     * @return void
     */
    public function initialize() {
        parent::initialize();
    }
    public function index() {
        $this->paginate = ['contain' => ['Moduls', 'Users']];
        $this->set('moduleParticipant', $this->paginate($this->ModuleParticipant));
        $this->set('_serialize', ['moduleParticipant']);
    }
    
    /**
     * View method
     *
     * @param string|null $id Module Participant id.
     * @return void
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function view($id = null) {
        $moduleParticipant = $this->ModuleParticipant->get($id, ['contain' => ['Module', 'Users']]);
        $this->set('moduleParticipant', $moduleParticipant);
        $this->set('_serialize', ['moduleParticipant']);
    }
    
    public function get_participant() {
        if ($this->request->is('ajax')) {
            $modul_id = $_GET['modul_id'];
            $moduleParticipant = $this->ModuleParticipant->find('all', ['contain' => ['Users']])->where(['modul_id' => $modul_id]);
            $number = $moduleParticipant->count();
            $this->set(compact('modul_id', 'number', 'moduleParticipant'));
            
            $moduleParticipant = $this->ModuleParticipant->newEntity();
            $this->viewBuilder()->layout('ajax');
            if (!empty($this->request->data['user_id'])) {
                $query = $this->ModuleParticipant->query();
                $id = $this->request->data['user_id'];
                $query->insert(['modul_id', 'user_id'])->values(['modul_id' => $modul_id, 'user_id' => $id])->execute();
                
                // send email
                $users = $this->ModuleParticipant->Users->get($id);
                $module = $this->ModuleParticipant->Module->get($modul_id, ['contain' => 'Users', 'Project']);
                $email = $users->email;
                $firstname = $users->firstname;
                $lastname = $users->lastname;
                $contain['realname'] = $firstname . ' ' . $lastname;
                $contain['mod_id'] = $module->id;
                $contain['mod_name'] = $module->module_name;
                $contain['mod_desc'] = $module->module_desc;
                $contain['project_id'] = $module->project_id;
                $contain['start_date'] = $module->start_date;
                $contain['end_date'] = $module->end_date;
                $contain['full_name'] = $this->Auth->User('firstname') . " " . $this->Auth->User('lastname');
                
                $this->send_mail('default', 'module_assignment', 'html', 'abiedoank@gmail.com', $email, $contain['realname'], 'You have Assigned on ' . $module->module_name . ' Module', $contain);
                
                $this->render('get_participant');
            }
        }
    }
    
    /**
     * Add method
     *
     * @return void Redirects on successful add, renders view otherwise.
     */
    public function add() {
        $modul_id = $_GET['mod'];
        $project_id = $_GET['pid'];
        $this->set(compact('modul_id', 'project_id'));
        $modul_id = $this->encryptor('decrypt', $modul_id);
        $moduleParticipant = $this->ModuleParticipant->newEntity();
        if ($this->request->is('post')) {
            $query = $this->ModuleParticipant->query();
            $method = $this->request->data['method'];
            $id = $this->request->data['id'];
            if ($method == 'add') {
                $query->insert(['modul_id', 'user_id'])->values(['modul_id' => $modul_id, 'user_id' => $id])->execute();
            } 
            else {
                $query->delete()->where(['user_id' => $id, 'modul_id' => $modul_id])->execute();
            }
        }
        $users = $this->ModuleParticipant->find()->select(['user_id'])->where(['modul_id' => $modul_id]);
        
        //            ->toArray();
        $user[] = array();
        $i = 0;
        foreach ($users as $user_id) {
            $user[$i] = $user_id->user_id;
            $i++;
        }
        $project = $this->ModuleParticipant->Module->Project->find('list', ['limit' => 200]);
        $users = $this->ModuleParticipant->Users->find('all', ['limit' => 200]);
        $this->set(compact('moduleParticipant', 'module', 'users', 'user', 'project'));
        $this->set('_serialize', ['moduleParticipant']);
    }
    
    /**
     * Edit method
     *
     * @param string|null $id Module Participant id.
     * @return void Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null) {
        $moduleParticipant = $this->ModuleParticipant->get($id, ['contain' => []]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $moduleParticipant = $this->ModuleParticipant->patchEntity($moduleParticipant, $this->request->data);
            if ($this->ModuleParticipant->save($moduleParticipant)) {
                $this->Flash->success('The module participant has been saved.');
                return $this->redirect(['action' => 'index']);
            } 
            else {
                $this->Flash->error('The module participant could not be saved. Please, try again.');
            }
        }
        $moduls = $this->ModuleParticipant->Moduls->find('list', ['limit' => 200]);
        $users = $this->ModuleParticipant->Users->find('list', ['limit' => 200]);
        $this->set(compact('moduleParticipant', 'moduls', 'users'));
        $this->set('_serialize', ['moduleParticipant']);
    }
    
    /**
     * Delete method
     *
     * @param string|null $id Module Participant id.
     * @return void Redirects to index.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function delete($id_module) {
        $ModuleParticipant = $this->ModuleParticipant->newEntity();
        if ($this->request->is('ajax')) {
            $query = $this->ModuleParticipant->query();
            $id = $_GET['id'];
            $query->delete()->where(['user_id' => $id, 'modul_id' => $id_module])->execute();
            $this->autoRender = false;
        }
    }
}
