<?php
namespace App\Controller;

use App\Controller\AppController;

/**
 * BugsComment Controller
 *
 * @property \App\Model\Table\BugsCommentTable $BugsComment
 */
class BugsCommentController extends AppController
{
    
    /**
     * Index method
     *
     * @return void
     */
    
    public function initialize() {
        parent::initialize();
    }
    public function index() {
        $this->paginate = ['contain' => ['Bugs']];
        $this->set('bugsComment', $this->paginate($this->BugsComment));
        $this->set('_serialize', ['bugsComment']);
    }
    
    /**
     * View method
     *
     * @param string|null $id Bugs Comment id.
     * @return void
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function get_bug_comment() {
        if ($this->request->is('ajax')) {
            $bugs_id = $_GET['id'];
            $comments = $this->get_comment($bugs_id);
            
            // 'all', ['contain'=>['Users']], ['conditions'=>['bugs_id'=>$bugs_id]]);
            $this->set(compact('comments', 'bugs_id'));
        }
    }
    
    private function get_comment($bug_id) {
        return $this->BugsComment->find()->where(['bugs_id' => $bug_id])->contain(['Users'])->order(['BugsComment.created'=>'DESC']);
    }
    
    public function cmt_count() {
        if ($this->request->is('ajax')) {
            $this->autoRender = false;
            $id = $_GET['id'];
            echo $this->get_comment($id)->count();
        }
    }
    
    public function view($id = null) {
        $bugsComment = $this->BugsComment->get($id, ['contain' => ['Bugs']]);
        $this->set('bugsComment', $bugsComment);
        $this->set('_serialize', ['bugsComment']);
    }
    
    /**
     * Add method
     *
     * @return void Redirects on successful add, renders view otherwise.
     */
    public function add($bugs_id) {
        $bugsComment = $this->BugsComment->newEntity();
        if ($this->request->is('post')) {
            $bugsComment->bugs_id = $bugs_id;
            $bugsComment->created = date('Y-m-d H:i:s', time());
            $bugsComment->created_by = $this->Auth->user('id');
            $bugsComment = $this->BugsComment->patchEntity($bugsComment, $this->request->data);
            if ($this->BugsComment->save($bugsComment)) {
                $comments = $this->BugsComment->find()->where(['bugs_id' => $bugs_id])->contain(['Users']);
                $this->set(compact('comments', 'bugs_id'));
                $this->render('get_bug_comment');
                
                // $this->Flash->success(__('The bugs comment has been saved.'));
                // return $this->redirect(['action' => 'index']);
                
                
            } 
            else {
                $this->Flash->error(__('The bugs comment could not be saved. Please, try again.'));
            }
        }
        
        // $bugs = $this->BugsComment->Bugs->find('list', ['limit' => 200]);
        // $this->set(compact('bugsComment', 'bugs'));
        // $this->set('_serialize', ['bugsComment']);
        
        
    }
    
    /**
     * Edit method
     *
     * @param string|null $id Bugs Comment id.
     * @return void Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null) {
        $bugsComment = $this->BugsComment->get($id, ['contain' => []]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $bugsComment = $this->BugsComment->patchEntity($bugsComment, $this->request->data);
            if ($this->BugsComment->save($bugsComment)) {
                $this->Flash->success(__('The bugs comment has been saved.'));
                return $this->redirect(['action' => 'index']);
            } 
            else {
                $this->Flash->error(__('The bugs comment could not be saved. Please, try again.'));
            }
        }
        $bugs = $this->BugsComment->Bugs->find('list', ['limit' => 200]);
        $this->set(compact('bugsComment', 'bugs'));
        $this->set('_serialize', ['bugsComment']);
    }
    
    /**
     * Delete method
     *
     * @param string|null $id Bugs Comment id.
     * @return void Redirects to index.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function delete($id = null) {
        $this->request->allowMethod(['post', 'delete']);
        $bugsComment = $this->BugsComment->get($id);
        if ($this->BugsComment->delete($bugsComment)) {
            $this->Flash->success(__('The bugs comment has been deleted.'));
        } 
        else {
            $this->Flash->error(__('The bugs comment could not be deleted. Please, try again.'));
        }
        return $this->redirect(['action' => 'index']);
    }
}
