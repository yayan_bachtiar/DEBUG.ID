<?php
namespace App\Controller;

use App\Controller\AppController;

/**
 * MenuModul Controller
 *
 * @property \App\Model\Table\MenuModulTable $MenuModul
 */
class MenuModulController extends AppController
{
    
    /**
     * Index method
     *
     * @return void
     */
    public function initialize() {
        parent::initialize();
    }
    public function index() {
        $this->paginate = ['contain' => ['Menu']];
        $this->set('menuModul', $this->paginate($this->MenuModul));
        $this->set('_serialize', ['menuModul']);
    }
    
    /**
     * View method
     *
     * @param string|null $id Menu Modul id.
     * @return void
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function view($id = null) {
        $menuModul = $this->MenuModul->get($id, ['contain' => ['Menu']]);
        $this->set('menuModul', $menuModul);
        $this->set('_serialize', ['menuModul']);
    }
    
    /**
     * Add method
     *
     * @return void Redirects on successful add, renders view otherwise.
     */
    public function add() {
        $menuModul = $this->MenuModul->newEntity();
        if ($this->request->is('post')) {
            $menuModul = $this->MenuModul->patchEntity($menuModul, $this->request->data);
            if ($this->MenuModul->save($menuModul)) {
                $this->Flash->success(__('The menu modul has been saved.'));
                return $this->redirect(['action' => 'index']);
            } 
            else {
                $this->Flash->error(__('The menu modul could not be saved. Please, try again.'));
            }
        }
        $menus = $this->MenuModul->Menu->find('list', ['limit' => 200]);
        $this->set(compact('menuModul', 'menus'));
        $this->set('_serialize', ['menuModul']);
    }
    
    /**
     * Edit method
     *
     * @param string|null $id Menu Modul id.
     * @return void Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null) {
        $menuModul = $this->MenuModul->get($id, ['contain' => []]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $menuModul = $this->MenuModul->patchEntity($menuModul, $this->request->data);
            if ($this->MenuModul->save($menuModul)) {
                $this->Flash->success(__('The menu modul has been saved.'));
                return $this->redirect(['action' => 'index']);
            } 
            else {
                $this->Flash->error(__('The menu modul could not be saved. Please, try again.'));
            }
        }
        $menus = $this->MenuModul->Menu->find('list', ['limit' => 200]);
        $this->set(compact('menuModul', 'menus'));
        $this->set('_serialize', ['menuModul']);
    }
    
    /**
     * Delete method
     *
     * @param string|null $id Menu Modul id.
     * @return void Redirects to index.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function delete($id = null) {
        $this->request->allowMethod(['post', 'delete']);
        $menuModul = $this->MenuModul->get($id);
        if ($this->MenuModul->delete($menuModul)) {
            $this->Flash->success(__('The menu modul has been deleted.'));
        } 
        else {
            $this->Flash->error(__('The menu modul could not be deleted. Please, try again.'));
        }
        return $this->redirect(['action' => 'index']);
    }
}
