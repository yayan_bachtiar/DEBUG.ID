<?php
namespace App\Controller;

use App\Controller\AppController;

/**
 * BugsAttachment Controller
 *
 * @property \App\Model\Table\BugsAttachmentTable $BugsAttachment
 */
class BugsAttachmentController extends AppController
{
    
    /**
     * Index method
     *
     * @return void
     */
    
    public function initialize() {
        parent::initialize();
    }
    public function index() {
        $this->paginate = ['contain' => ['Bugs']];
        $this->set('bugsAttachment', $this->paginate($this->BugsAttachment));
        $this->set('_serialize', ['bugsAttachment']);
    }
    
    public function get_bug_files() {
        if ($this->request->is('ajax')) {
            $bug_id = $_GET['id'];
            $this->viewBuilder()->layout('ajax');
            $bug_attach = $this->BugsAttachment->find('all', ['contain'])->where(['bugs_id' => $bug_id]);
            $number = $bug_attach->count();
            $this->set(compact('bug_attach', 'number', 'bug_id'));
        }
    }
    
    /**
     * View method
     *
     * @param string|null $id Bugs Attachment id.
     * @return void
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function view($id = null) {
        $bugsAttachment = $this->BugsAttachment->get($id, ['contain' => ['Bugs']]);
        $this->set('bugsAttachment', $bugsAttachment);
        $this->set('_serialize', ['bugsAttachment']);
    }
    
    /**
     * Add method
     *
     * @return void Redirects on successful add, renders view otherwise.
     */
    public function add($id = null) {
        
        // $id = $_GET['id'];
        $bugsAttachment = $this->BugsAttachment->newEntity();
        
        $bugsAttachment = $this->BugsAttachment->patchEntity($bugsAttachment, $this->request->data);
        $ds = DIRECTORY_SEPARATOR;
        
        $upload_path = 'images/bugsAttachment/' . $id;
        $storeFolder = WWW_ROOT . $upload_path;
        if (!file_exists($storeFolder)) {
            mkdir($storeFolder, 0777, true);
        }
        if (!empty($_FILES)) {
            $actual_name = pathinfo($_FILES['file']['name'], PATHINFO_FILENAME);
            $original_name = $actual_name;
            $extension = pathinfo($_FILES['file']['name'], PATHINFO_EXTENSION);
            
            $tempFile = $_FILES['file']['tmp_name'];
            $targetPath = $storeFolder . $ds;
            $name = $_FILES['file']['name'];
            $targetFile = $targetPath . $_FILES['file']['name'];
            $i = 1;
            while (file_exists($targetFile)) {
                $actual_name = (string)$original_name . $i;
                $name = $actual_name . "." . $extension;
                $targetFile = $targetPath . $name;
                $i++;
            }
            if (move_uploaded_file($_FILES['file']['tmp_name'], $targetFile)) {
                
                // $id = $this->encryptor('decrypt', $id);
                $bugsAttachment->path = $upload_path;
                $bugsAttachment->ext = $extension;
                $bugsAttachment->filename = $name;
                $bugsAttachment->bugs_id = $id;
                $bugsAttachment->upload_by = $this->Auth->User('id');
                $bugsAttachment->upload_date = date('Y-m-d H:i:s', time());
                
                if ($this->BugsAttachment->save($bugsAttachment)) {
                    
                    // echo 'saved';
                    $pid = $this->BugsAttachment->Bugs->find()->select(['Module.project_id', 'Module.id'])->contain(['Module'])->where(['Bugs.id' => $id])->first();
                    
                    // print($pid->Module->project_id);
                    // die();
                    $po_id = $pid->Module->project_id;
                    $mid = $pid->Module->id;
                    $u_id = $this->Auth->User('username');
                    $related_link = json_encode($bugsAttachment);
                    $this->write_log($pid, "$u_id has upload a file", "upload_file", $related_link, $mid, $bugsAttachment->bugs_id);
                } 
                else {
                    echo 'unsaved';
                    var_dump($bugsAttachment);
                    echo $id;
                }
                echo "File is valid, and was successfully uploaded.\n";
                echo $id;
            } 
            else {
                echo "Possible file upload attack!\n";
            }
        }
        
        // $id = $this->encryptor('decrypt', $id);
        $title = 'Add Bugs Attachment';
        $this->set(compact('id', 'title'));
        
        // $this->set('_serialize', ['id']);
        
        $bugsAttachment = $this->BugsAttachment->newEntity();
        if ($this->request->is('post')) {
            $bugsAttachment = $this->BugsAttachment->patchEntity($bugsAttachment, $this->request->data);
            if ($this->BugsAttachment->save($bugsAttachment)) {
                $this->Flash->success(__('The bugs attachment has been saved.'));
                return $this->redirect(['action' => 'index']);
            } 
            else {
                $this->Flash->error(__('The bugs attachment could not be saved. Please, try again.'));
            }
        }
        $this->set(compact('bugsAttachment'));
        $this->set('_serialize', ['bugsAttachment']);
    }
    
    /**
     * Edit method
     *
     * @param string|null $id Bugs Attachment id.
     * @return void Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null) {
        $bugsAttachment = $this->BugsAttachment->get($id, ['contain' => []]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $bugsAttachment = $this->BugsAttachment->patchEntity($bugsAttachment, $this->request->data);
            if ($this->BugsAttachment->save($bugsAttachment)) {
                $this->Flash->success(__('The bugs attachment has been saved.'));
                return $this->redirect(['action' => 'index']);
            } 
            else {
                $this->Flash->error(__('The bugs attachment could not be saved. Please, try again.'));
            }
        }
        $bugs = $this->BugsAttachment->Bugs->find('list', ['limit' => 200]);
        $this->set(compact('bugsAttachment', 'bugs'));
        $this->set('_serialize', ['bugsAttachment']);
    }
    
    /**
     * Delete method
     *
     * @param string|null $id Bugs Attachment id.
     * @return void Redirects to index.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function delete($id = null) {
        $id = $_GET['id'];
        $this->viewBuilder()->layout('ajax');
        $this->autoRender = false;
        
        // $this->request->allowMethod(['post', 'delete']);
        $bugsAttachment = $this->BugsAttachment->get($id);
        
        // $moduleAttachment = $this->ModuleAttachment->get($id);
        if ($this->BugsAttachment->delete($bugsAttachment)) {
            echo "berhasil";
        }
    }
}