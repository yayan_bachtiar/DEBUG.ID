<?php
namespace App\Controller;

use App\Controller\AppController;

/**
 * BugsParticipant Controller
 *
 * @property \App\Model\Table\BugsParticipantTable $BugsParticipant
 */
class BugsParticipantController extends AppController
{
    
    /**
     * Index method
     *
     * @return void
     */
    public function initialize() {
        parent::initialize();
    }
    public function index() {
        $this->paginate = ['contain' => ['Bugs', 'Users']];
        $this->set('bugsParticipant', $this->paginate($this->BugsParticipant));
        $this->set('_serialize', ['bugsParticipant']);
    }
    
    /**
     * View method
     *
     * @param string|null $id Bugs Participant id.
     * @return void
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function view($id = null) {
        $bugsParticipant = $this->BugsParticipant->get($id, ['contain' => ['Bugs', 'Users']]);
        $this->set('bugsParticipant', $bugsParticipant);
        $this->set('_serialize', ['bugsParticipant']);
    }
    
    /**
     * Add method
     *
     * @return void Redirects on successful add, renders view otherwise.
     */
    public function add() {
        $bugsParticipant = $this->BugsParticipant->newEntity();
        if ($this->request->is('post')) {
            $bugsParticipant = $this->BugsParticipant->patchEntity($bugsParticipant, $this->request->data);
            if ($this->BugsParticipant->save($bugsParticipant)) {
                $this->Flash->success(__('The bugs participant has been saved.'));
                return $this->redirect(['action' => 'index']);
            } 
            else {
                $this->Flash->error(__('The bugs participant could not be saved. Please, try again.'));
            }
        }
        $bugs = $this->BugsParticipant->Bugs->find('list', ['limit' => 200]);
        $users = $this->BugsParticipant->Users->find('list', ['limit' => 200]);
        $this->set(compact('bugsParticipant', 'bugs', 'users'));
        $this->set('_serialize', ['bugsParticipant']);
    }
    
    /**
     * Edit method
     *
     * @param string|null $id Bugs Participant id.
     * @return void Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null) {
        $bugsParticipant = $this->BugsParticipant->get($id, ['contain' => []]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $bugsParticipant = $this->BugsParticipant->patchEntity($bugsParticipant, $this->request->data);
            if ($this->BugsParticipant->save($bugsParticipant)) {
                $this->Flash->success(__('The bugs participant has been saved.'));
                return $this->redirect(['action' => 'index']);
            } 
            else {
                $this->Flash->error(__('The bugs participant could not be saved. Please, try again.'));
            }
        }
        $bugs = $this->BugsParticipant->Bugs->find('list', ['limit' => 200]);
        $users = $this->BugsParticipant->Users->find('list', ['limit' => 200]);
        $this->set(compact('bugsParticipant', 'bugs', 'users'));
        $this->set('_serialize', ['bugsParticipant']);
    }
    
    /**
     * Delete method
     *
     * @param string|null $id Bugs Participant id.
     * @return void Redirects to index.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function delete($id = null) {
        $this->request->allowMethod(['post', 'delete']);
        $bugsParticipant = $this->BugsParticipant->get($id);
        if ($this->BugsParticipant->delete($bugsParticipant)) {
            $this->Flash->success(__('The bugs participant has been deleted.'));
        } 
        else {
            $this->Flash->error(__('The bugs participant could not be deleted. Please, try again.'));
        }
        return $this->redirect(['action' => 'index']);
    }
}
