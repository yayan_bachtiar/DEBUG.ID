<?php
namespace App\Controller;

use App\Controller\AppController;

/**
 * Role Controller
 *
 * @property \App\Model\Table\RoleTable $Role
 */
class RoleController extends AppController
{
    
    /**
     * Index method
     *
     * @return void
     */
    public function initialize() {
        parent::initialize();
    }
    public function index() {
        $this->set('role', $this->paginate($this->Role));
        $this->set('_serialize', ['role']);
    }
    
    /**
     * View method
     *
     * @param string|null $id Role id.
     * @return void
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function view($id = null) {
        $role = $this->Role->get($id, ['contain' => []]);
        $this->set('role', $role);
        $this->set('_serialize', ['role']);
    }
    
    /**
     * Add method
     *
     * @return void Redirects on successful add, renders view otherwise.
     */
    public function add() {
        $role = $this->Role->newEntity();
        if ($this->request->is('post')) {
            $role = $this->Role->patchEntity($role, $this->request->data);
            if ($this->Role->save($role)) {
                $this->Flash->success('The role has been saved.');
                return $this->redirect(['action' => 'index']);
            } 
            else {
                $this->Flash->error('The role could not be saved. Please, try again.');
            }
        }
        $this->set(compact('role'));
        $this->set('_serialize', ['role']);
    }
    
    /**
     * Edit method
     *
     * @param string|null $id Role id.
     * @return void Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null) {
        $role = $this->Role->get($id, ['contain' => []]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $role = $this->Role->patchEntity($role, $this->request->data);
            if ($this->Role->save($role)) {
                $this->Flash->success('The role has been saved.');
                return $this->redirect(['action' => 'index']);
            } 
            else {
                $this->Flash->error('The role could not be saved. Please, try again.');
            }
        }
        $this->set(compact('role'));
        $this->set('_serialize', ['role']);
    }
    
    /**
     * Delete method
     *
     * @param string|null $id Role id.
     * @return void Redirects to index.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function delete($id = null) {
        $this->request->allowMethod(['post', 'delete']);
        $role = $this->Role->get($id);
        if ($this->Role->delete($role)) {
            $this->Flash->success('The role has been deleted.');
        } 
        else {
            $this->Flash->error('The role could not be deleted. Please, try again.');
        }
        return $this->redirect(['action' => 'index']);
    }
}
