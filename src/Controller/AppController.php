<?php

/**
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link      http://cakephp.org CakePHP(tm) Project
 * @since     0.2.9
 * @license   http://www.opensource.org/licenses/mit-license.php MIT License
 */
namespace App\Controller;

use Cake\Controller\Controller;
use Cake\Event\Event;
use Cake\Mailer\Email;

/**
 * Application Controller
 *
 * Add your application-wide methods in the class below, your controllers
 * will inherit them.
 *
 * @link http://book.cakephp.org/3.0/en/controllers.html#the-app-controller
 */
class AppController extends Controller
{
    
    /**
     * Initialization hook method.
     *
     * Use this method to add common initialization code like loading components.
     *
     * @return void
     */
    
    public $helpers = ['General'];
    public function initialize() {
        // $this->loadComponent('Bakkerij.Notifier');
        $this->loadComponent('RequestHandler');
        $this->loadComponent('Flash');
        
        // $this->loadComponent('Auth');
        $this->loadComponent('CakeDC/Users.UsersAuth');
        $this->set('email', $this->Auth->User('email'));
    }
    
    public function beforeRender(Event $event) {
        if (!array_key_exists('_serialize', $this->viewVars) && in_array($this->response->type(), ['application/json', 'application/xml'])) {
            $this->set('_serialize', true);
        }
    }
    
    public function encryptor($action, $string) {
        $output = false;
        
        $encrypt_method = "AES-256-CBC";
        
        //pls set your unique hashing key
        $secret_key = 'xsisthebest';
        $secret_iv = 'byyayanbachtiar';
        
        // hash
        $key = hash('sha256', $secret_key);
        
        // iv - encrypt method AES-256-CBC expects 16 bytes - else you will get a warning
        $iv = substr(hash('sha256', $secret_iv), 0, 16);
        
        //do the encyption given text/string/number
        if ($action == 'encrypt') {
            $output = openssl_encrypt($string, $encrypt_method, $key, 0, $iv);
            $output = base64_encode($output);
        } 
        else if ($action == 'decrypt') {
            
            //decrypt the given text/string/number
            $output = openssl_decrypt(base64_decode($string), $encrypt_method, $key, 0, $iv);
        }
        
        return $output;
    }
    
    public function send_mail($config = null, $template, $format, $from, $to, $title, $sasa, $content = NULL) {
        
        $email = new Email();
        $email->transport('gmail');
        $email->viewVars(['content' => $content]);
        try {
            $res = $email->from([$from])->template($template)->emailFormat($format)->to([$to => $title])->subject($sasa)->send();
        }
        catch(Exception $e) {
            
            echo 'Exception : ', $e->getMessage(), "\n";
            die();
        }
    }
    
    public function write_log($project_id, $message, $log_type, $related_link, $mod_id = null, $bug_id = null) {
        $this->loadModel('ProjectLog');
        $query = $this->ProjectLog->query();
        $user_id = $this->Auth->User('id');
        $browser = $_SERVER['HTTP_USER_AGENT'];
        $ip_address = $_SERVER['REMOTE_ADDR'];
        if (!empty($_SERVER['HTTP_CLIENT_IP'])) {
            $ip_address = $_SERVER['HTTP_CLIENT_IP'];
        }
        if (!empty($_SERVER['HTTP_X_FORWARDED_FOR'])) {
            $ip_address = $_SERVER['HTTP_X_FORWARDED_FOR'];
        }
        $time = date('Y-m-d H:i:s', time());
        $query->insert(['project_id', 'message', 'created_by', 'created', 'log_type', 'related_link', 'module_id', 'bug_id', 'browser', 'ip_address'])->values(['project_id' => $project_id, 'message' => $message, 'created_by' => $user_id, 'created' => $time, 'log_type' => $log_type, 'related_link' => $related_link, 'module_id' => $mod_id, 'bug_id' => $bug_id, 'browser' => $browser, 'ip_address' => $ip_address])->execute();
    }
}
