<?php
namespace App\Controller;

use App\Controller\AppController;

/**
 * ProjectAttachment Controller
 *
 * @property \App\Model\Table\ProjectAttachmentTable $ProjectAttachment
 */
class ProjectAttachmentController extends AppController
{
    
    /**
     * Index method
     *
     * @return void
     */
    public function initialize() {
        parent::initialize();
    }
    public function index() {
        $this->paginate = ['contain' => ['Project']];
        $this->set('title', 'List of Project Attachment');
        $this->set('projectAttachment', $this->paginate($this->ProjectAttachment));
        $this->set('_serialize', ['projectAttachment']);
    }
    
    /**
     * View method
     *
     * @param string|null $id Project Attachment id.
     * @return void
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function view($id = null) {
        $projectAttachment = $this->ProjectAttachment->get($id, ['contain' => ['Project']]);
        $this->set('projectAttachment', $projectAttachment);
        $this->set('_serialize', ['projectAttachment']);
    }
    
    public function get_attachment() {
        if ($this->request->is('ajax')) {
            $project_id = $_GET['project_id'];
            $projectAttachment = $this->ProjectAttachment->find()->where(['project_id' => $project_id]);
            
            // $count = $this->ProjectAttachment->find('all');
            $number = $projectAttachment->count();
            $this->set('projectAttachment', $projectAttachment);
            $this->set(compact('project_id', 'number'));
            $this->viewBuilder()->layout('ajax');
        }
        
        // echo "script not allowed";
        
    }
    
    /**
     * Add method
     *
     * @return void Redirects on successful add, renders view otherwise.
     */
    public function add() {
        $id = $_GET['id'];
        $projectAttachment = $this->ProjectAttachment->newEntity();
        
        $projectAttachment = $this->ProjectAttachment->patchEntity($projectAttachment, $this->request->data);
        $ds = DIRECTORY_SEPARATOR;
        
        $upload_path = 'images/project/' . $id;
        $storeFolder = WWW_ROOT . $upload_path;
        if (!file_exists($storeFolder)) {
            mkdir($storeFolder, 0777, true);
        }
        if (!empty($_FILES)) {
            $actual_name = pathinfo($_FILES['file']['name'], PATHINFO_FILENAME);
            $original_name = $actual_name;
            $extension = pathinfo($_FILES['file']['name'], PATHINFO_EXTENSION);
            
            $tempFile = $_FILES['file']['tmp_name'];
            $targetPath = $storeFolder . $ds;
            $name = $_FILES['file']['name'];
            $targetFile = $targetPath . $_FILES['file']['name'];
            $i = 1;
            while (file_exists($targetFile)) {
                $actual_name = (string)$original_name . $i;
                $name = $actual_name . "." . $extension;
                $targetFile = $targetPath . $name;
                $i++;
            }
            if (move_uploaded_file($_FILES['file']['tmp_name'], $targetFile)) {
                $projectAttachment->path = $upload_path;
                $projectAttachment->ext = $extension;
                $projectAttachment->filename = $_FILES['file']['name'];
                $projectAttachment->project_id = $id;
                $projectAttachment->upload_by = $this->Auth->User('id');
                $projectAttachment->upload_date = date('Y-m-d H:i:s', time());
                
                if ($this->ProjectAttachment->save($projectAttachment)) {
                    echo 'saved';
                    $this->Flash->success('The project attachment has been saved.');
                    
                    //                return $this->redirect(['action' => 'index']);
                    $u_id = $this->Auth->User('username');
                    $related_link = json_encode($projectAttachment);
                    $this->write_log($id,
                     "$u_id has upload a file",
                      "upload_file",
                      $related_link,
                       null, 
                       null);
                    
                } 
                else {
                    echo 'unsaved';
                    var_dump($projectAttachment);
                    echo $id;
                    
                    //                $this->Flash->error('The project attachment could not be saved. Please, try again.');
                    
                    
                }
                echo "File is valid, and was successfully uploaded.\n";
            } 
            else {
                echo "Possible file upload attack!\n";
            }
        }
        
        //        $module = $this->ProjectAttachment->Module->find('list', ['limit' => 200]);
        $this->set(compact('projectAttachment'));
        $this->set('_serialize', ['projectAttachment']);
    }
    
    /**
     * Edit method
     *
     * @param string|null $id Project Attachment id.
     * @return void Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null) {
        $projectAttachment = $this->ProjectAttachment->get($id, ['contain' => []]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $projectAttachment = $this->ProjectAttachment->patchEntity($projectAttachment, $this->request->data);
            if ($this->ProjectAttachment->save($projectAttachment)) {
                $this->Flash->success('The project attachment has been saved.');
                return $this->redirect(['action' => 'index']);
            } 
            else {
                $this->Flash->error('The project attachment could not be saved. Please, try again.');
            }
        }
        
        //        $module = $this->ProjectAttachment->Module->find('list', ['limit' => 200]);
        $this->set(compact('projectAttachment'));
        $this->set('_serialize', ['projectAttachment']);
    }
    
    /**
     * Delete method
     *
     * @param string|null $id Project Attachment id.
     * @return void Redirects to index.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function delete() {
        $this->viewBuilder()->layout('ajax');
        $this->autoRender = false;
        
        // $this->template = false;
        $id = $_GET['id'];
        $this->request->allowMethod(['post', 'get']);
        $projectAttachment = $this->ProjectAttachment->get($id);
        if ($this->ProjectAttachment->delete($projectAttachment)) {
            echo "berhasil";
        }
    }
}
