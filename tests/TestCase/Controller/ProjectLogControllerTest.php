<?php
namespace App\Test\TestCase\Controller;

use App\Controller\ProjectLogController;
use Cake\TestSuite\IntegrationTestCase;

/**
 * App\Controller\ProjectLogController Test Case
 */
class ProjectLogControllerTest extends IntegrationTestCase
{

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.project_log',
        'app.projects',
        'app.modules',
        'app.bugs',
        'app.module',
        'app.project',
        'app.project_participant',
        'app.users',
        'app.bugs_participant',
        'app.module_participant',
        'app.role',
        'app.project_attachment',
        'app.modul_comment',
        'app.task',
        'app.module_attachment',
        'app.status',
        'app.priority'
    ];

    /**
     * Test index method
     *
     * @return void
     */
    public function testIndex()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test view method
     *
     * @return void
     */
    public function testView()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test add method
     *
     * @return void
     */
    public function testAdd()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test edit method
     *
     * @return void
     */
    public function testEdit()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test delete method
     *
     * @return void
     */
    public function testDelete()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
