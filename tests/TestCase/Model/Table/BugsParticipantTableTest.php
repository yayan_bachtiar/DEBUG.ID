<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\BugsParticipantTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\BugsParticipantTable Test Case
 */
class BugsParticipantTableTest extends TestCase
{

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.bugs_participant',
        'app.bugs',
        'app.module',
        'app.project',
        'app.project_participant',
        'app.users',
        'app.module_participant',
        'app.role',
        'app.project_attachment',
        'app.modul_comment',
        'app.task',
        'app.module_attachment',
        'app.status',
        'app.priority'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::exists('BugsParticipant') ? [] : ['className' => 'App\Model\Table\BugsParticipantTable'];
        $this->BugsParticipant = TableRegistry::get('BugsParticipant', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->BugsParticipant);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test buildRules method
     *
     * @return void
     */
    public function testBuildRules()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
