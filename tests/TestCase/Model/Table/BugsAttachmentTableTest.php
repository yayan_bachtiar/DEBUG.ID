<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\BugsAttachmentTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\BugsAttachmentTable Test Case
 */
class BugsAttachmentTableTest extends TestCase
{

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.bugs_attachment',
        'app.bugs',
        'app.module',
        'app.project',
        'app.project_participant',
        'app.users',
        'app.module_participant',
        'app.role',
        'app.project_attachment',
        'app.modul_comment',
        'app.modules',
        'app.task',
        'app.module_attachment'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::exists('BugsAttachment') ? [] : ['className' => 'App\Model\Table\BugsAttachmentTable'];
        $this->BugsAttachment = TableRegistry::get('BugsAttachment', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->BugsAttachment);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test buildRules method
     *
     * @return void
     */
    public function testBuildRules()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
