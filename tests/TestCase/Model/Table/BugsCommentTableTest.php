<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\BugsCommentTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\BugsCommentTable Test Case
 */
class BugsCommentTableTest extends TestCase
{

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.bugs_comment',
        'app.bugs',
        'app.module',
        'app.project',
        'app.project_participant',
        'app.users',
        'app.module_participant',
        'app.role',
        'app.project_attachment',
        'app.modul_comment',
        'app.modules',
        'app.task',
        'app.module_attachment',
        'app.status',
        'app.priority'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::exists('BugsComment') ? [] : ['className' => 'App\Model\Table\BugsCommentTable'];
        $this->BugsComment = TableRegistry::get('BugsComment', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->BugsComment);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test buildRules method
     *
     * @return void
     */
    public function testBuildRules()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
