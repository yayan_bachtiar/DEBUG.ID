<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\BugHistoryTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\BugHistoryTable Test Case
 */
class BugHistoryTableTest extends TestCase
{

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.bug_history',
        'app.moduls',
        'app.bugs',
        'app.module',
        'app.project',
        'app.project_participant',
        'app.users',
        'app.bugs_participant',
        'app.module_participant',
        'app.role',
        'app.project_attachment',
        'app.modul_comment',
        'app.task',
        'app.status',
        'app.module_attachment',
        'app.priority'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::exists('BugHistory') ? [] : ['className' => 'App\Model\Table\BugHistoryTable'];
        $this->BugHistory = TableRegistry::get('BugHistory', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->BugHistory);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test buildRules method
     *
     * @return void
     */
    public function testBuildRules()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
