<?php
/**
 * Copyright 2010 - 2015, Cake Development Corporation (http://cakedc.com)
 *
 * Licensed under The MIT License
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright Copyright 2010 - 2015, Cake Development Corporation (http://cakedc.com)
 * @license MIT License (http://www.opensource.org/licenses/mit-license.php)
 */
use Cake\Core\Configure;
?>
<div id="login" class="animate form">
    <section class="login_content">
    <?= $this->Flash->render('flash') ?>
    <?= $this->Form->create() ?>

        <h1>Register</h1>
        <div>
        <?= $this->Form->input('username', ['required' => true,  "class"=>"form-control",  "placeholder"=>"Username", 'label'=>false]) ?>
        </div>
        <div>
        <?= $this->Form->input('email', ['required' => true,  "class"=>"form-control",  "placeholder"=>"Your Email", 'label'=>false]) ?>
        </div>
        <div>
        <?= $this->Form->input('password', ['required' => true, "class"=>"form-control", "placeholder"=>"Password", 'label'=>false]) ?>
        </div>
        <div>
        <?= $this->Form->input('password_confirm', ['required' => true, "class"=>"form-control", "placeholder"=>"Password Confirmation", 'label'=>false, 'type'=>'password']) ?>
        </div>
        <div>
        <?= $this->Form->input('first_name', ['required' => true, "class"=>"form-control", "placeholder"=>"First Name", 'label'=>false]) ?>
        </div>
        <div>
        <?= $this->Form->input('last_name', ['required' => true, "class"=>"form-control", "placeholder"=>"Last Name", 'label'=>false]) ?>
        </div>
        <div>
        <?php echo $this->Form->input('tos', ['type' => 'checkbox', 'label' => __d('Users', 'Accept TOS conditions?'), 'required' => true]);
        echo $this->User->addReCaptcha();?>
        </div>
        <div>
            <?= $this->Form->button(__d('Users', 'Register'), ['class'=>"btn btn-default submit"]); ?>
            <!-- <button class="btn btn-default submit" type="submit">Log in</button> -->
        </div>
        <div class="clearfix"></div>
        <div class="separator">
        <p>
            <?php
            echo $this->Html->link(__d('users', 'Login'), ['action' => 'login']);
            if (Configure::check('Users.Email.required')) {
                echo ' | ';
                echo $this->Html->link(__d('users', 'Reset Password'), ['action' => 'requestResetPassword']);
            }
            ?>
        </p>
            <div>
                <h1><i class="fa fa-paw" style="font-size: 26px;"></i> Gentelella Alela!</h1>
                <p>©2015 All Rights Reserved. Gentelella Alela! is a Bootstrap 3 template. Privacy and Terms</p>
            </div>
        </div>
        <?= $this->Form->end();?>
    </section>
    <!-- content -->
</div>