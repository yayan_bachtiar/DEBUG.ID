<?php
/**
* Copyright 2010 - 2015, Cake Development Corporation (http://cakedc.com)
*
* Licensed under The MIT License
* Redistributions of files must retain the above copyright notice.
*
* @copyright Copyright 2010 - 2015, Cake Development Corporation (http://cakedc.com)
* @license MIT License (http://www.opensource.org/licenses/mit-license.php)
*/
use Cake\Core\Configure;
?>
<div id="login" class="animate form">
    <section class="login_content">
        <?= $this->Flash->render('auth') ?>
        <?= $this->Form->create('User') ?>
        <h1>Request Password</h1>
        <div>
            <?= $this->Form->input('reference', ['required' => true,  "class"=>"form-control",  "placeholder"=>"Email", 'label'=>false]) ?>
        </div>
        <div>
            <div>
                <?= $this->Form->button(__d('Users', 'Submit')); ?>
                <?= $this->Form->end() ?>
            </div>
            <p>
            </p>
            <div class="clearfix"></div>
            <div class="separator">
                <?php
                    echo $this->Html->link(__d('users', 'Login'), ['action' => 'login']);
                    if (Configure::check('Users.Registration.active')) {
                        echo $this->Html->link(__d('users', 'Register'), ['action' => 'register']);
                    }
                ?>
                <div class="clearfix"></div>
                <br />
                <div>
                    <h1><i class="fa fa-paw" style="font-size: 26px;"></i> Gentelella Alela!</h1>
                    <p>©2015 All Rights Reserved. Gentelella Alela! is a Bootstrap 3 template. Privacy and Terms</p>
                </div>
            </div>
        </div>
    </section>
    <!-- content -->
</div>