<?php
/**
 * Copyright 2010 - 2015, Cake Development Corporation (http://cakedc.com)
 *
 * Licensed under The MIT License
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright Copyright 2010 - 2015, Cake Development Corporation (http://cakedc.com)
 * @license MIT License (http://www.opensource.org/licenses/mit-license.php)
 */

use Cake\Core\Configure;
?>

<div id="login" class="animate form">
    <section class="login_content">
    <?= $this->Flash->render('auth') ?>
    <?= $this->Flash->render('flash') ?>
    <?= $this->Form->create() ?>

        <h1>Login Form</h1>
        <div>
        <?= $this->Form->input('username', ['required' => true,  "class"=>"form-control",  "placeholder"=>"Username"]) ?>
        </div>
        <div>
        <?= $this->Form->input('password', ['required' => true, "class"=>"form-control", "placeholder"=>"Password"]) ?>
        </div>
        <?php
        if (Configure::check('Users.RememberMe.active')) {
            echo $this->Form->input(Configure::read('Users.Key.Data.rememberMe'), [
                'type' => 'checkbox',
                'label' => __d('Users', 'Remember me'),
                'checked' => 'checked'
            ]);
        }
        ?>
        <div>
            <?= $this->Form->button(__d('Users', 'Login'), ['class'=>"btn btn-default submit"]); ?>
            <!-- <button class="btn btn-default submit" type="submit">Log in</button> -->
            <a class="reset_pass" onclick="alert('on doing, please contact your admin')">Lost your password?</a>
        </div>
        <p>
            <?php
            if (Configure::check('Users.Registration.active')) {
                echo $this->Html->link(__d('users', 'Register'), ['action' => 'register']);
            }
            if (Configure::check('Users.Email.required')) {
                echo ' | ';
                echo $this->Html->link(__d('users', 'Reset Password'), ['action' => 'requestResetPassword']);
            }
            ?>
        </p>
        <div class="clearfix"></div>
        <div class="separator">
            <p class="change_link">New to site?
            <a href="#toregister" class="to_register" > Create Account </a>
            </p>
            <div class="clearfix"></div>
            <br />
            <div>
                <h1><i class="fa fa-paw" style="font-size: 26px;"></i> Gentelella Alela!</h1>
                <p>©2015 All Rights Reserved. Gentelella Alela! is a Bootstrap 3 template. Privacy and Terms</p>
            </div>
        </div>
        <?= $this->Form->end();?>
    </section>
    <!-- content -->
</div>