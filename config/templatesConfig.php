<?php
$config = [
    'Templates'=>[
        'shortForm' => [
            'formstart' => '<form class="" {{attrs}}>',
            'label' => '<label class="control-label col-md-3 col-sm-3 col-xs-12" {{attrs}}>{{text}}</label>',
            'input' => '<div class="col-md-6 col-sm-6 col-xs-12"><input type="{{type}}" class="form-control col-md-7 col-xs-12" name="{{name}}" {{attrs}}/></div>',
            'textarea'=>'<div class="col-md-6 col-sm-6 col-xs-12"><textarea type="{{type}}" class="form-control col-md-7 col-xs-12 summernote" name="{{name}}" {{attrs}} >{{value}}</textarea></div>',
            'select' => '<div class="col-md-6 col-sm-6 col-xs-12"><select name="{{name}}" class="select2_single form-control" {{attrs}}>{{content}}</select></div>',
            'inputContainer' => '<div class="form-group {{required}}" form-type="{{type}}">{{content}}</div>',
            'checkContainer' => '',],
        'longForm' => [
            'formstart' => '<form class="" {{attrs}}>',
            'label' => '<label class="col-md-2 control-label" {{attrs}}>{{text}}</label>',
            'input' => '<div class="col-md-6"><input type="{{type}}" name="{{name}}" {{attrs}} /></div>',
            'select' => '<div class="col-md-6"><select name="{{name}}"{{attrs}}>{{content}}</select></div>',
            'inputContainer' => '<div class="form-group {{required}}" form-type="{{type}}">{{content}}</div>',
            'checkContainer' => '',],
        'fullForm' => [
            'formstart' => '<form class="" {{attrs}}>',
            'label' => '<label class="col-md-2 control-label" {{attrs}}>{{text}}</label>',
            'input' => '<div class="col-md-10"><input type="{{type}}" name="{{name}}" {{attrs}} /></div>',
            'select' => '<div class="col-md-10"><select name="{{name}}"{{attrs}}>{{content}}</select></div>',
            'inputContainer' => '<div class="form-group {{required}}" form-type="{{type}}">{{content}}</div>',
            'checkContainer' => '',]
    ]
];